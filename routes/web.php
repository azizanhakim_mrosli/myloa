<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/logout', function () {
    return view('logout');
});

Route::any('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::any('/inbox', [App\Http\Controllers\HomeController::class, 'inbox'])->name('inbox');
Route::any('/sent', [App\Http\Controllers\HomeController::class, 'sent'])->name('sent');
Route::any('/drafts', [App\Http\Controllers\HomeController::class, 'drafts'])->name('drafts');
Route::any('/GetDraftPaper', [App\Http\Controllers\HomeController::class, 'GetDraftPaper'])->name('GetDraftPaper');
Route::any('/GetSentPaper', [App\Http\Controllers\HomeController::class, 'GetSentPaper'])->name('GetSentPaper');
Route::any('/mytracker', [App\Http\Controllers\HomeController::class, 'mytracker'])->name('mytracker');
Route::any('/PaperReviewed', [App\Http\Controllers\HomeController::class, 'PaperReviewed'])->name('PaperReviewed');
Route::any('/GetRiskDetails', [App\Http\Controllers\HomeController::class, 'GetRiskDetails'])->name('GetRiskDetails');
Route::any('/RiskReviewed', [App\Http\Controllers\HomeController::class, 'RiskReviewed'])->name('RiskReviewed');

Route::any('/draft-approval-pg1', [App\Http\Controllers\PaperController::class, 'draftapprovalpg1'])->name('draftApprovalPg1');
Route::any('/draft-approval-pg1/{paper_id}', [App\Http\Controllers\PaperController::class, 'draftapprovalpg1'])->name('draftApprovalPg1');
Route::any('/draft-approval-pg2', [App\Http\Controllers\PaperController::class, 'draftapprovalpg2'])->name('draftApprovalPg2');
Route::any('/draft-approval-pg3', [App\Http\Controllers\PaperController::class, 'draftapprovalpg3'])->name('draftApprovalPg3');
Route::any('/self-assessment-pg1', [App\Http\Controllers\PaperController::class, 'selfassessmentpg1'])->name('selfassessmentpg1');
Route::any('/self-assessment-pg2', [App\Http\Controllers\PaperController::class, 'selfassessmentpg2'])->name('selfassessmentpg2');
Route::any('/self-assessment-pg3', [App\Http\Controllers\PaperController::class, 'selfassessmentpg3'])->name('selfassessmentpg3');
Route::any('/self-assessment-pg4', [App\Http\Controllers\PaperController::class, 'selfassessmentpg4'])->name('selfassessmentpg4');
Route::any('/self-assessment-pg5', [App\Http\Controllers\PaperController::class, 'selfassessmentpg5'])->name('selfassessmentpg5');
Route::any('/risk-assessment-pg1', [App\Http\Controllers\PaperController::class, 'riskassessmentpg1'])->name('riskassessmentpg1');
Route::any('/risk-assessment-pg2', [App\Http\Controllers\PaperController::class, 'riskassessmentpg2'])->name('riskassessmentpg2');
Route::any('/risk-assessment-pg3', [App\Http\Controllers\PaperController::class, 'riskassessmentpg3'])->name('riskassessmentpg3');
Route::any('/risk-assessment-pg3/{risk_id}', [App\Http\Controllers\PaperController::class, 'riskassessmentpg3'])->name('riskassessmentpg3');
Route::any('/risk-assessment-pg4/{risk_id}', [App\Http\Controllers\PaperController::class, 'riskassessmentpg4'])->name('riskassessmentpg4');
Route::any('/risk-assessment-pg5', [App\Http\Controllers\PaperController::class, 'riskassessmentpg5'])->name('riskassessmentpg5');
Route::any('/submission-pg1/{paper_id}', [App\Http\Controllers\PaperController::class, 'papersubmissionpg1'])->name('papersubmissionpg1');
Route::any('/summary-pg1/{paper_id}', [App\Http\Controllers\PaperController::class, 'summarypg1'])->name('summarypg1');


Route::any('/CreatePaper', [App\Http\Controllers\PaperController::class, 'CreatePaper'])->name('CreatePaper');
Route::any('/DeleteDraftPaper/{paper_id}', [App\Http\Controllers\PaperController::class, 'DeleteDraftPaper'])->name('DeleteDraftPaper');
Route::any('/PaperAddDratf', [App\Http\Controllers\PaperController::class, 'PaperAddDratf'])->name('PaperAddDratf');
Route::any('/PaperAddObjective', [App\Http\Controllers\PaperController::class, 'PaperAddObjective'])->name('PaperAddObjective');
Route::any('/PaperDeleteObjective', [App\Http\Controllers\PaperController::class, 'PaperDeleteObjective'])->name('PaperDeleteObjective');
Route::any('/PaperAddRecommendation', [App\Http\Controllers\PaperController::class, 'PaperAddRecommendation'])->name('PaperAddRecommendation');
Route::any('/PaperDeleteRecommendation', [App\Http\Controllers\PaperController::class, 'PaperDeleteRecommendation'])->name('PaperDeleteRecommendation');
Route::any('/PaperAddAttachment', [App\Http\Controllers\PaperController::class, 'PaperAddAttachment'])->name('PaperAddAttachment');
Route::any('/PaperDeleteAttachment', [App\Http\Controllers\PaperController::class, 'PaperDeleteAttachment'])->name('PaperDeleteAttachment');
Route::any('/PaperAddContent', [App\Http\Controllers\PaperController::class, 'PaperAddContent'])->name('PaperAddContent');
Route::any('/PaperAddRPT', [App\Http\Controllers\PaperController::class, 'PaperAddRPT'])->name('PaperAddRPT');
Route::any('/PaperAddPaperType/{papertype_id}/{paper_id}', [App\Http\Controllers\PaperController::class, 'PaperAddPaperType'])->name('PaperAddPaperType');
Route::any('/PaperAddProjectthreshold', [App\Http\Controllers\PaperController::class, 'PaperAddProjectthreshold'])->name('PaperAddProjectthreshold');
Route::any('/PaperAddRecipient', [App\Http\Controllers\PaperController::class, 'PaperAddRecipient'])->name('PaperAddRecipient');
Route::any('/PaperDeleteRecipient', [App\Http\Controllers\PaperController::class, 'PaperDeleteRecipient'])->name('PaperDeleteRecipient');
Route::any('/PaperAddExclusion', [App\Http\Controllers\PaperController::class, 'PaperAddExclusion'])->name('PaperAddExclusion');
Route::any('/PaperAddRisk', [App\Http\Controllers\PaperController::class, 'PaperAddRisk'])->name('PaperAddRisk');
Route::any('/PaperDeleteRisk/{risk_id}', [App\Http\Controllers\PaperController::class, 'PaperDeleteRisk'])->name('PaperDeleteRisk');
Route::any('/risk-assessment-pg4/{risk_id}/PaperAddMitigation', [App\Http\Controllers\PaperController::class, 'PaperAddMitigation'])->name('PaperAddMitigation');
Route::any('/risk-assessment-pg4/{risk_id}/PaperRemoveMitigation/{mitigation_id}', [App\Http\Controllers\PaperController::class, 'PaperRemoveMitigation'])->name('PaperRemoveMitigation');
Route::any('/PaperAddReviewer', [App\Http\Controllers\PaperController::class, 'PaperAddReviewer'])->name('PaperAddReviewer');
Route::any('/PaperRemoveReviewer', [App\Http\Controllers\PaperController::class, 'PaperRemoveReviewer'])->name('PaperRemoveReviewer');
Route::any('/PaperAddEndorser', [App\Http\Controllers\PaperController::class, 'PaperAddEndorser'])->name('PaperAddEndorser');
Route::any('/PaperRemoveEndorser', [App\Http\Controllers\PaperController::class, 'PaperRemoveEndorser'])->name('PaperRemoveEndorser');
Route::any('/PaperAddRiskRemarks', [App\Http\Controllers\PaperController::class, 'PaperAddRiskRemarks'])->name('PaperAddRiskRemarks');
Route::any('/PaperAddRiskTreatment', [App\Http\Controllers\PaperController::class, 'PaperAddRiskTreatment'])->name('PaperAddRiskTreatment');
Route::any('/PaperSubmit/{paper_id}', [App\Http\Controllers\PaperController::class, 'PaperSubmit'])->name('PaperSubmit');

Route::any('/RiskAddReviewer', [App\Http\Controllers\RiskController::class, 'RiskAddReviewer'])->name('RiskAddReviewer');
Route::any('/RiskRemoveReviewer', [App\Http\Controllers\RiskController::class, 'RiskRemoveReviewer'])->name('RiskRemoveReviewer');
Route::any('/RiskAddEndorser', [App\Http\Controllers\RiskController::class, 'RiskAddEndorser'])->name('RiskAddEndorser');
Route::any('/RiskRemoveEndorser', [App\Http\Controllers\RiskController::class, 'RiskRemoveEndorser'])->name('RiskRemoveEndorser');
Route::any('/RiskSubmitEndorsement', [App\Http\Controllers\RiskController::class, 'RiskSubmitEndorsement'])->name('RiskSubmitEndorsement');
Route::any('/RiskRecipientAddAttachment', [App\Http\Controllers\RiskController::class, 'RiskRecipientAddAttachment'])->name('RiskRecipientAddAttachment');
Route::any('/RiskRecipientDeleteAttachment', [App\Http\Controllers\RiskController::class, 'RiskRecipientDeleteAttachment'])->name('RiskRecipientDeleteAttachment');


Route::any('/GetProjectThresholds', [App\Http\Controllers\PaperController::class, 'GetProjectThresholds'])->name('PaperAddEndorser');

Route::any('/create-paper', [App\Http\Controllers\PaperController::class, 'createPaper'])->name('createPaper');
Route::any('/create-paper-pg2', [App\Http\Controllers\PaperController::class, 'createPaperPg2'])->name('createPaperPg2');
Route::any('/create-paper-pg3', [App\Http\Controllers\PaperController::class, 'createPaperPg3'])->name('createPaperPg3');
Route::any('/create-paper-pg4', [App\Http\Controllers\PaperController::class, 'createPaperPg4'])->name('createPaperPg4');
Route::any('/create-paper-pg5', [App\Http\Controllers\PaperController::class, 'createPaperPg5'])->name('createPaperPg5');
Route::any('/create-paper-pg6', [App\Http\Controllers\PaperController::class, 'createPaperPg6'])->name('createPaperPg6');





