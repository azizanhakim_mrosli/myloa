<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riskreviewers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('paper_id')->nullable();
            $table->foreignId('reviewer_id')->nullable();
            $table->string('reviewer_role')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riskreviewers');
    }
}
