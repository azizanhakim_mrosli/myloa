<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiskrecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riskrecipients', function (Blueprint $table) {
            $table->id();
            $table->foreignId('paper_id')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->string('riskrecipient_role')->nullable();
            $table->string('riskrecipient_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riskrecipients');
    }
}
