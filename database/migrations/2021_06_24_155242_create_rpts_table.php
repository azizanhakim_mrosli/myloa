<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpts', function (Blueprint $table) {
            $table->id();
            $table->integer('paper_id')->nullable();
            $table->string('rpt_related_pdb')->nullable();
            $table->string('rpt_market_rate')->nullable();
            $table->string('rpt_at_par')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpts');
    }
}
