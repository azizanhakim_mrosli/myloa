<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRisksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('paper_id')->nullable();
            $table->string('risk_title')->nullable();
            $table->string('risk_causes')->nullable();
            $table->string('risk_category')->nullable();
            $table->string('risk_potential_impact')->nullable();
            $table->integer('risk_likelihood_rating')->nullable();
            $table->integer('risk_impact_rating')->nullable();
            $table->integer('risk_current_rating')->nullable();
            $table->string('risk_assumptions_likehood')->nullable();
            $table->string('risk_assumptions_impact')->nullable();
            $table->string('risk_remark')->nullable();
            $table->integer('risk_target_likehood_rating')->nullable();
            $table->integer('risk_target_impact_rating')->nullable();
            $table->integer('risk_target_rating')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('risks');
    }
}
