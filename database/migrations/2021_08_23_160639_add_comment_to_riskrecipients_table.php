<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCommentToRiskrecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('riskrecipients', function (Blueprint $table) {
            $table->string('riskrecipient_comment', 500)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('riskrecipients', function (Blueprint $table) {
            $table->dropColumn('riskrecipient_comment');
            //
        });
    }
}
