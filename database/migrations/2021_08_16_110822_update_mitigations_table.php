<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMitigationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mitigations', function (Blueprint $table) {
            //
            $table->foreignId('mitigation_owner')->nullable()->change();
            $table->renameColumn('mitigation_owner', 'user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mitigations', function (Blueprint $table) {
            //
            $table->string('mitigation_owner')->nullable()->change();
            $table->renameColumn('user_id', 'mitigation_owner');
        });
    }
}
