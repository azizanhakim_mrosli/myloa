<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateProjectthresholdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projectthresholds', function (Blueprint $table) {
            //
            $table->foreignId('papertype_id')->nullable();
            $table->foreignId('papersubtype_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projectthresholds', function (Blueprint $table) {
            $table->dropColumn('papertype_id');
            $table->dropColumn('papersubtype_id');
            //
        });
    }
}
