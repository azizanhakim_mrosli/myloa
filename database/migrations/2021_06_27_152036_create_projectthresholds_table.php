<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectthresholdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projecttresholds', function (Blueprint $table) {
            $table->id();
            $table->string('projecttreshold_name')->nullable();
            $table->string('projecttreshold_approval')->nullable();
            $table->string('projecttreshold_description')->nullable();
            $table->foreignId('project_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projecttresholds');
    }
}
