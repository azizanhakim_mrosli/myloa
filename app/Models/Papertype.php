<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Papertype extends Model
{
    use HasFactory;

    protected $fillable = [
        'papertype_name',
        'papertype_definition',
    ];

    public function papers()
    {
        return $this->hasMany(Paper::class);
    }
}
