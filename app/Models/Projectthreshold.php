<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Projectthreshold extends Model
{
    use HasFactory;
    protected $fillable = [
        'projecttreshold_name',
        'projecttreshold_approval',
        'projecttreshold_description',
    ];
    
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function papers()
    {
        return $this->hasMany(Paper::class);
    }
}
