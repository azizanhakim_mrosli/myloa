<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rpt extends Model
{
    use HasFactory;

    protected $fillable = [
        'paper_id',
        'rpt_related_pdb',
        'rpt_market_rate',
        'rpt_at_par',
    ];

    public function paper()
    {
        return $this->belongsTo(Paper::class);
    }
}
