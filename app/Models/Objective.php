<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Objective extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'objective',
        'paper_id'
    ];

    public function paper()
    {
        return $this->belongsTo(Paper::class);
    }
}
