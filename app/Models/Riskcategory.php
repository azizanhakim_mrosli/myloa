<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Riskcategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'riskcategory_name',
        'riskcategory_definition',
    ];

    public function risks()
    {
        return $this->hasMany(Risk::class);
    }
}
