<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paper extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'paper_ref_number',
        'paper_subject',
        'paper_importance',
        'paper_app_date',
        'paper_status',
        'paper_content',
        'papertype_id',
        'projectthreshold_id',
        'exclusion_id',
        'paper_riskremark',
        'paper_next_steps',
        'paper_risk_status',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function rpt()
    {
        return $this->hasOne(Rpt::class);
    }

    public function papertype()
    {
        return $this->belongsTo(Papertype::class);
    }

    public function projectthreshold()
    {
        return $this->belongsTo(Projectthreshold::class);
    }

    public function exclusion()
    {
        return $this->belongsTo(Exclusion::class);
    }

    public function risks()
    {
        return $this->hasMany(Risk::class);
    }

    public function riskreviewers()
    {
        return $this->hasMany(Riskreviewer::class);
    }

    public function riskendorsers()
    {
        return $this->hasMany(Riskendorser::class);
    }

    public function riskrecipients()
    {
        return $this->hasMany(Riskrecipient::class);
    }

    public function objectives()
    {
        return $this->hasMany(Objective::class);
    }

    public function recommendations()
    {
        return $this->hasMany(Recommendation::class);
    }

    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    public function recipients()
    {
        return $this->hasMany(recipient::class);
    }

    public function riskattachments()
    {
        return $this->hasMany(Riskattachment::class);
    }
    
}
