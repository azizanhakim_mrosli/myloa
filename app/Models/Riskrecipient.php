<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Riskrecipient extends Model
{
    use HasFactory;

    protected $fillable = [
        'paper_id',
        'user_id',
        'riskrecipient_role',
        'riskrecipient_status',
        'riskrecipient_comment',
    ];

    public function paper()
    {
        return $this->belongsTo(Paper::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
