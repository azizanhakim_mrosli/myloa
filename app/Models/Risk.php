<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Risk extends Model
{
    use HasFactory;

    protected $fillable = [
        'paper_id',
        'risk_title',
        'risk_category',
        'risk_causes',
        'risk_potential_impact',
        'risk_likelihood_rating',
        'risk_impact_rating',
        'risk_current_rating',
        'risk_assumptions_likehood',
        'risk_assumptions_impact',
        'risk_remark',
        'risk_target_likehood_rating',
        'risk_target_impact_rating',
        'risk_target_rating',
    ];

    public function paper()
    {
        return $this->belongsTo(Paper::class);
    }

    public function mitigations()
    {
        return $this->hasMany(Mitigation::class);
    }

    public function riskcategory()
    {
        return $this->belongsTo(Riskcategory::class);
    }
}
