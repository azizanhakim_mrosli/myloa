<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mitigation extends Model
{
    use HasFactory;

    protected $fillable = [
        'risk_id',
        'mitigation_detail',
        'mitigation_owner',
    ];

    public function risk()
    {
        return $this->belongsTo(Risk::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
