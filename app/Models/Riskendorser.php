<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Riskendorser extends Model
{
    use HasFactory;

    protected $fillable = [
        'endorser_role',
    ];

    public function paper()
    {
        return $this->belongsTo(Paper::class);
    }

    public function endorser()
    {
        return $this->belongsTo(User::class);
    }
}
