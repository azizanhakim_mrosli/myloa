<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exclusion extends Model
{
    use HasFactory;

    protected $fillable = [
        'exclusion_name',
    ];

    public function papers()
    {
        return $this->hasMany(Paper::class);
    }
}
