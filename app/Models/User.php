<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function papers()
    {
        return $this->hasMany(Paper::class);
    }

    public function receiving()
    {
        return $this->hasMany(Recipient::class);
    }

    public function riskreceiving()
    {
        return $this->hasMany(Riskrecipient::class);
    }

    public function mitigations(){
        return $this->hasMany(Mitigation::class);
    }

    public function riskattachments()
    {
        return $this->hasMany(Riskattachment::class);
    }
}
