<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Paper;
use App\Models\Risk;
use App\Models\Recipient;
use App\Models\Riskrecipient;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        if (Session::exists('paper')) {
            Session::forget('paper');
        }
        
        // $recipient_approver = false;
        $pendings = collect();
        $revieweds = collect();
        $ccs = collect();
        $RADMs = collect();
        $RADMs_reviewed = collect();
        $risk_approveds = collect();
        $RADMs_temp = Auth::user()->riskreceiving;

        $risk_approveds = Auth::user()->papers->where('paper_status', 'Risks Approved');

        // for displaying papers at homepage
        $papers = Auth::user()->papers->where('paper_status', '!=', 'Draft');

        foreach($RADMs_temp as $RADM_temp){
            if($RADM_temp->riskrecipient_role == "Reviewer"){
                if($RADM_temp == Riskrecipient::where('paper_id', $RADM_temp->paper_id)->where('riskrecipient_role', 'Reviewer')->where('riskrecipient_status', 'Pending')->first()){
                    $RADMs->push($RADM_temp);
                }
            }
            elseif($RADM_temp->riskrecipient_role == "Endorser"){
                $RADM_pending_count = Riskrecipient::where('paper_id', $RADM_temp->paper_id)->where('riskrecipient_role', 'Reviewer')->where('riskrecipient_status', 'Pending')->count();
                if($RADM_pending_count == 0 && $RADM_temp == Riskrecipient::where('paper_id', $RADM_temp->paper_id)->where('riskrecipient_role', 'Endorser')->where('riskrecipient_status', 'Pending')->first()){
                    $RADMs->push($RADM_temp);
                }
            }
        }

        $RADMs_reviewed_temp = Auth::user()->riskreceiving->where('riskrecipient_status', 'Reviewed');
        foreach($RADMs_reviewed_temp as $RADM_reviewed_temp){
            $RADMs_reviewed->push($RADM_reviewed_temp);
        }

        info('$RADMs->count() :' . $RADMs->count());
        info('$RADMs :' . $RADMs);


        $pendings_temp = Auth::user()->receiving;
        info("pendings_temp->count() : " . $pendings_temp->count());
        foreach($pendings_temp as $pending_temp){
            info("pending_temp->paper_id : " . $pending_temp->paper_id );
            info("pending_temp->recipient_category : " . $pending_temp->recipient_category );
            $risk_reviewers_count = $pending_temp->paper->riskrecipients->where('riskrecipient_status', 'Pending')->count();
            info('risk_reviewers_count : ' . $risk_reviewers_count);
            if($pending_temp->recipient_category == "Reviewer" && $risk_reviewers_count == 0 &&  $pending_temp->paper->paper_status != "Draft"){
                $reviewer = Recipient::where('paper_id', $pending_temp->paper_id)->where('recipient_category', 'Reviewer')->where('recipient_status', 'Pending')->first();
                info("reviewer : " . $reviewer);
                if($reviewer != null){
                    if($reviewer->user_id == Auth::user()->id){
                        $pendings->push($reviewer);
                    }
                }
            }
            elseif($pending_temp->recipient_category == "Approver" && $pending_temp->paper->paper_status != "Draft"){
                $reviewer = Recipient::where('paper_id', $pending_temp->paper_id)->where('recipient_category', 'Approver')->where('recipient_status', 'Pending')->first();
                $reviewers_count = $pendings_temp->where('paper_id', $pending_temp->paper_id)->where('recipient_category', 'Reviewer')->where('recipient_status', 'Pending')->count();
                info("reviewer : " . $reviewer);
                if($reviewer != null && $reviewers_count == 0){
                    if($reviewer->user_id == Auth::user()->id){
                        $pendings->push($reviewer);
                    }
                }
            }
        }

        $revieweds_temp = Auth::user()->receiving->where('recipient_status', 'Reviewed');
        foreach($revieweds_temp as $reviewed_temp){
            if($reviewed_temp->paper->paper_status != "Draft"){
                $revieweds->push($reviewed_temp);
            }
        }

        $ccs_temp = Auth::user()->receiving->where('recipient_category', 'CC');
        foreach($ccs_temp as $cc_temp){
            if($cc_temp->paper->paper_status != "Draft"){
                $ccs->push($cc_temp);
            }
        }

        info('$pendings' . $pendings);
        info('$revieweds' . $revieweds);

        return view('home', compact('risk_approveds', 'papers', 'pendings', 'revieweds', 'ccs', 'RADMs', 'RADMs_reviewed'));
    }

    public function inbox()
    {
        if (Session::exists('paper')) {
            Session::forget('paper');
        }
        
        // $recipient_approver = false;
        $pendings = collect();
        $revieweds = collect();
        $ccs = collect();
        $RADMs = collect();
        $RADMs_reviewed = collect();
        $risk_approveds = collect();
        $RADMs_temp = Auth::user()->riskreceiving;

        $risk_approveds = Auth::user()->papers->where('paper_status', 'Risks Approved');
        info('risk_approveds : ' . $risk_approveds);
        info('risk_approveds->count() : ' . $risk_approveds->count());

        // for displaying papers at homepage
        $papers = Auth::user()->papers;

        foreach($RADMs_temp as $RADM_temp){
            if($RADM_temp->riskrecipient_role == "Reviewer"){
                if($RADM_temp == Riskrecipient::where('paper_id', $RADM_temp->paper_id)->where('riskrecipient_role', 'Reviewer')->where('riskrecipient_status', 'Pending')->first()){
                    $RADMs->push($RADM_temp);
                }
            }
            elseif($RADM_temp->riskrecipient_role == "Endorser"){
                $RADM_pending_count = Riskrecipient::where('paper_id', $RADM_temp->paper_id)->where('riskrecipient_role', 'Reviewer')->where('riskrecipient_status', 'Pending')->count();
                if($RADM_pending_count == 0 && $RADM_temp == Riskrecipient::where('paper_id', $RADM_temp->paper_id)->where('riskrecipient_role', 'Endorser')->where('riskrecipient_status', 'Pending')->first()){
                    $RADMs->push($RADM_temp);
                }
            }
        }

        $RADMs_reviewed_temp = Auth::user()->riskreceiving->where('riskrecipient_status', 'Reviewed');
        foreach($RADMs_reviewed_temp as $RADM_reviewed_temp){
            $RADMs_reviewed->push($RADM_reviewed_temp);
        }

        info('$RADMs->count() :' . $RADMs->count());
        info('$RADMs :' . $RADMs);


        $pendings_temp = Auth::user()->receiving;
        info("pendings_temp->count() : " . $pendings_temp->count());
        foreach($pendings_temp as $pending_temp){
            info("pending_temp->paper_id : " . $pending_temp->paper_id );
            info("pending_temp->recipient_category : " . $pending_temp->recipient_category );
            $risk_reviewers_count = $pending_temp->paper->riskrecipients->where('riskrecipient_status', 'Pending')->count();
            info('risk_reviewers_count : ' . $risk_reviewers_count);
            if($pending_temp->recipient_category == "Reviewer" && $risk_reviewers_count == 0 &&  $pending_temp->paper->paper_status == "Pending Reviewal"){
                $reviewer = Recipient::where('paper_id', $pending_temp->paper_id)->where('recipient_category', 'Reviewer')->where('recipient_status', 'Pending')->first();
                info("reviewer : " . $reviewer);
                if($reviewer != null){
                    if($reviewer->user_id == Auth::user()->id){
                        $pendings->push($reviewer);
                    }
                }
            }
            elseif($pending_temp->recipient_category == "Approver" && $pending_temp->paper->paper_status == "Pending Approval"){
                $reviewer = Recipient::where('paper_id', $pending_temp->paper_id)->where('recipient_category', 'Approver')->where('recipient_status', 'Pending')->first();
                $reviewers_count = $pendings_temp->where('paper_id', $pending_temp->paper_id)->where('recipient_category', 'Reviewer')->where('recipient_status', 'Pending')->count();
                info("reviewer : " . $reviewer);
                if($reviewer != null && $reviewers_count == 0){
                    if($reviewer->user_id == Auth::user()->id){
                        $pendings->push($reviewer);
                    }
                }
            }
        }

        $revieweds_temp = Auth::user()->receiving->where('recipient_status', 'Reviewed');
        foreach($revieweds_temp as $reviewed_temp){
            if($reviewed_temp->paper->paper_status != "Draft"){
                $revieweds->push($reviewed_temp);
            }
        }

        $ccs_temp = Auth::user()->receiving->where('recipient_category', 'CC');
        foreach($ccs_temp as $cc_temp){
            if($cc_temp->paper->paper_status != "Draft"){
                $ccs->push($cc_temp);
            }
        }

        $approveds = Auth::user()->papers->where('paper_status', 'Approved');

        info('$pendings' . $pendings);
        info('$revieweds' . $revieweds);

        return view('inbox', compact('risk_approveds', 'papers', 'pendings', 'revieweds', 'ccs', 'RADMs', 'RADMs_reviewed', 'approveds'));
    }

    public function sent()
    {
        $papers = Auth::user()->papers->where("paper_status", "!=", "Draft");
        return view('sent', compact('papers'));
    }

    public function drafts()
    {
        $drafts = Auth::user()->papers->where("paper_status", "Draft");
        return view('drafts', compact('drafts'));
    }

    public function mytracker()
    {
        return view('mytracker');
    }
    
    public function GetDraftPaper(Request $request){
        $paper = Paper::find($request->paper_id);
        $rpt = $paper->rpt;
        $objectives = $paper->objectives;
        $attachments = $paper->attachments;
        $recommendations = $paper->recommendations;
        $next_steps = $paper->paper_next_steps;


        $recipients_cc = collect();
        $recipients_cc_temp = $paper->recipients->where("recipient_category", "CC");
        foreach($recipients_cc_temp as $recipient_cc_temp){
            $recipients_cc->push($recipient_cc_temp->user);
        }

        $recipients_reviewer = collect();
        $recipients_reviewer_temp = $paper->recipients->where("recipient_category", "Reviewer");
        foreach($recipients_reviewer_temp as $recipient_reviewer_temp){
            $recipients_reviewer->push($recipient_reviewer_temp->user);
        }
        
        $recipients_approver = collect();
        $recipients_approver_temp = $paper->recipients->where("recipient_category", "Approver");
        foreach($recipients_approver_temp as $recipient_approver_temp){
            $recipients_approver->push($recipient_approver_temp->user);

        }

        info("CC : " . $recipients_cc);
        info("Reviewer : " .$recipients_reviewer);
        info("Approver : " .$recipients_approver);

        return response()->json(['paper' => $paper, 'rpt' => $rpt, 'objectives' => $objectives, 'attachments' => $attachments, 'recommendations' => $recommendations, 'next_steps' => $next_steps]);
    }

    public function GetSentPaper(Request $request){

        $paper = Paper::find($request->paper_id);
        $user = $paper->user;
        $rpt = $paper->rpt;
        $objectives = $paper->objectives;
        $attachments = $paper->attachments;
        $recommendations = $paper->recommendations;
        $next_steps = $paper->paper_next_steps;


        $recipients_cc = collect();
        $recipients_cc_temp = $paper->recipients->where("recipient_category", "CC");
        foreach($recipients_cc_temp as $recipient_cc_temp){
            $recipients_cc->push($recipient_cc_temp->user);
        }

        $recipients_reviewer = collect();
        $recipients_reviewer_temp = $paper->recipients->where("recipient_category", "Reviewer");
        foreach($recipients_reviewer_temp as $recipient_reviewer_temp){
            $recipient_reviewer_temp->user->setAttribute('recipient_status', $recipient_reviewer_temp->recipient_status);
            $recipient_reviewer_temp->user->setAttribute('recipient_id', $recipient_reviewer_temp->id);
            $recipients_reviewer->push($recipient_reviewer_temp->user);
            // $recipients_reviewer->put('recipient_status', $recipient_reviewer_temp->recipient_status);
        }
        
        $recipients_approver = collect();
        $recipients_approver_temp = $paper->recipients->where("recipient_category", "Approver");
        foreach($recipients_approver_temp as $recipient_approver_temp){
            $recipient_approver_temp->user->setAttribute('recipient_status', $recipient_approver_temp->recipient_status);
            $recipient_approver_temp->user->setAttribute('recipient_id', $recipient_approver_temp->id);
            $recipients_approver->push($recipient_approver_temp->user);
            // $recipients_approver->put('recipient_status', $recipient_approver_temp->recipient_status);
        }

        info("CC : " . $recipients_cc);
        info("paper : " . $paper);
        
        info("Reviewer : " .$recipients_reviewer);
        info("Approver : " .$recipients_approver);

        return response()->json(['paper' => $paper, 'user' => $user, 'rpt' => $rpt, 'objectives' => $objectives, 'attachments' => $attachments, 'recommendations' => $recommendations, 'next_steps' => $next_steps, "recipients_cc" => $recipients_cc, "recipients_reviewer" => $recipients_reviewer, "recipients_approver" => $recipients_approver]);
    }

    public function PaperReviewed(Request $request){
        info($request->paper_id);

        $recipient = Recipient::where('paper_id', $request->paper_id)->where('user_id', Auth::user()->id)->first();
        info($recipient);

        $recipient->recipient_status = "Reviewed";
        $recipient->save();

        $paper = Paper::find($request->paper_id);

        $reviewer_pending = $paper->recipients->where('recipient_category', 'Reviewer')->where('recipient_status', 'Pending');
        $approver_pending = $paper->recipients->where('recipient_category', 'Approver')->where('recipient_status', 'Pending');

        if($paper->paper_status == "Pending Reviewal"){
            if($reviewer_pending->count() > 0){
                $paper->paper_status = "Pending Reviewal";
            }elseif($reviewer_pending->count() == 0){
                $paper->paper_status = "Pending Approval";
            }
        }elseif($paper->paper_status == "Pending Approval"){
            if($approver_pending->count() > 0){
                $paper->paper_status = "Pending Approval";
            }elseif($approver_pending->count() == 0){
                $paper->paper_status = "Approved";
            }
        }

        // if($approver_pending->count() == 0){
        //     $paper->paper_status = "Approver Approved";
        // }elseif($reviewer_pending->count() == 0){
        //     $paper->paper_status = "Reviewer Approved";
        // }elseif($reviewer_pending->count() > 0){
        //     $paper->paper_status = "Pending Reviewal";
        // }elseif($approver_pending->count() > 0){
        //     $paper->paper_status = "Pending Approval";
        // }

        $paper->save();

        return response()->json();
    }

    public function GetRiskDetails(Request $request){
        info('request->paper_id : ' . $request->paper_id);
        $paper = Paper::find($request->paper_id);
        $attachments = $paper->attachments;
        $riskattachments = $paper->riskattachments;
        $risk_reviewers_temp = $paper->riskrecipients->where('riskrecipient_role', 'Reviewer');
        $risk_endorsers_temp = $paper->riskrecipients->where('riskrecipient_role', 'Endorser');
        $risk_reviewers = collect();
        $risk_endorsers = collect();
        foreach($risk_reviewers_temp as $risk_reviewer_temp){
            $risk_reviewer_temp->user->setAttribute('riskrecipient_status', $risk_reviewer_temp->riskrecipient_status);
            $risk_reviewers->push($risk_reviewer_temp->user);
        }
        foreach($risk_endorsers_temp as $risk_endorser_temp){
            $risk_endorser_temp->user->setAttribute('riskrecipient_status', $risk_endorser_temp->riskrecipient_status);
            $risk_endorsers->push($risk_endorser_temp->user);
        }
        $risks = $paper->risks;
        foreach($risks as $risk){
            $risk->setAttribute('mitigations', $risk->mitigations);
        }
        info('risks : ' . $risks);
        info('risk_reviewers : ' . $risk_reviewers);
        return response()->json(['paper' => $paper, 'risks' => $risks, 'risk_reviewers' => $risk_reviewers, 'risk_endorsers' => $risk_endorsers, 'attachments' => $attachments, 'riskattachments' => $riskattachments]);
    }

    public function RiskReviewed(Request $request){
        info($request->paper_id);

        $riskrecipient = Riskrecipient::where('paper_id', $request->paper_id)->where('user_id', Auth::user()->id)->first();
        info($riskrecipient);

        $riskrecipient->riskrecipient_status = "Reviewed";
        $riskrecipient->riskrecipient_comment = $request->riskrecipient_comment;
        $riskrecipient->save();

        $paper = Paper::find($request->paper_id);
        if($paper->riskrecipients->where('riskrecipient_status', 'Pending')->count() == 0){
            $paper->paper_status = "Risks Approved";
            $paper->save();
        }

        return response()->json();
    }
}
