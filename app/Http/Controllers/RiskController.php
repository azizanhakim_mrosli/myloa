<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Paper;
use App\Models\Rpt;
use App\Models\Papertype;
use App\Models\Projectthreshold;
use App\Models\Project;
use App\Models\Exclusion;
use App\Models\Mitigation;
use App\Models\Objective;
use App\Models\Recommendation;
use App\Models\Risk;
use App\Models\Riskcategory;
use App\Models\Riskendorser;
use App\Models\user;
use App\Models\Riskreviewer;
use App\Models\Riskrecipient;
use App\Models\Attachment;
use App\Models\Recipient;
use App\Models\Riskattachment;

class RiskController extends Controller
{
    //
    public function RiskAddReviewer(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $user = User::where('email', $request['user_email'])->first();
            $riskrecipient = Riskrecipient::create([
                'riskrecipient_role' => "Reviewer",
                'riskrecipient_status' => "Pending",
            ]);
            $riskrecipient->paper()->associate(session('paper')->id);
            $riskrecipient->user()->associate($user->id);
            $riskrecipient->save();
            Session::forget('paper');
        }
        Session::put('paper', $paper);
        return redirect('/risk-assessment-pg5');
    }

    public function RiskRemoveReviewer(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $riskrecipient = Riskrecipient::find($request->riskrecipient_id);
            $riskrecipient->delete();
        }
        Session::put('paper', $paper);
        return redirect('/risk-assessment-pg5');
    }

    public function RiskAddEndorser(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $user = User::where('email', $request['user_email'])->first();
            $riskrecipient = Riskrecipient::create([
                'riskrecipient_role' => "Endorser",
                'riskrecipient_status' => "Pending",
            ]);
            $riskrecipient->paper()->associate(session('paper')->id);
            $riskrecipient->user()->associate($user->id);
            $riskrecipient->save();
            Session::forget('paper');
        }
        Session::put('paper', $paper);
        return redirect('/risk-assessment-pg5');
    }

    public function RiskRemoveEndorser(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $riskrecipient = Riskrecipient::find($request->riskrecipient_id);
            $riskrecipient->delete();
        }
        Session::put('paper', $paper);
        return redirect('/risk-assessment-pg5');
    }
    
    public function RiskSubmitEndorsement(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $paper->paper_status = "Wait Risk";
            $paper->paper_risk_status = "Pending";
            $paper->paper_riskremark = $request['paper_riskremark'];
            $paper->save();
            return redirect('/home');
        }
    }

    public function RiskRecipientAddAttachment(Request $request){
        info("1 okay");
        info($request->all());
        if($request['attachment_id'] == "" || $request['attachment_id'] == null){
            info("2 okay");
            $document = $request->attachment;
            info($document);
            if($document != null){
                info("2.1 okay");
                $document->getRealPath();
                $document->getClientOriginalName();
                $document->getClientOriginalExtension();
                $document->getSize();
                $document->getMimeType();
                $fileExt = $document->getClientOriginalExtension();
                info($fileExt);
                $fileExt = strToLower($fileExt);
                $fileName = $request->paper_id . "_" . $document->getClientOriginalName();
                $destinationPath = "riskrecipientattachments";
                $document->move($destinationPath, $fileName);
                info("2.2 okay");
            }
            info("3 okay");
            $attachment = Riskattachment::create([
                'riskattachment_name' => $document->getClientOriginalName(),
                'riskattachment_category' => "Default",
            ]);
            info("4 okay");
            $attachment->paper()->associate($request['paper_id']);
            $attachment->user()->associate(Auth::user()->id);
            $attachment->save();
            $mode = "create";
            info("5 okay");
        }else{
            $attachment = Riskattachment::find($request['attachment_id']);
            $attachment->attachment = $request['attachment'];
            $attachment->save();
            $mode = "edit";
        }
        info("6 okay");
        // return response()->json(['projectthresholds' => $projectthresholds, 'count' => $count]);
        return response()->json(['attachment_id' => $attachment->id, 'mode' => $mode]);
        // return response()->json(['attachment_id' => "1", 'mode' => "create"]);
    }

    public function RiskRecipientDeleteAttachment(Request $request){
        $attachment = Riskattachment::find($request['attachment_id']);
        $attachment->delete();
        return response()->json();
    }
}
