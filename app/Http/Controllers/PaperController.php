<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Paper;
use App\Models\Rpt;
use App\Models\Papertype;
use App\Models\Projectthreshold;
use App\Models\Project;
use App\Models\Exclusion;
use App\Models\Mitigation;
use App\Models\Objective;
use App\Models\Recommendation;
use App\Models\Risk;
use App\Models\Riskcategory;
use App\Models\Riskendorser;
use App\Models\user;
use App\Models\Riskreviewer;
use App\Models\Attachment;
use App\Models\Recipient;
use App\Models\Riskrecipient;
use PDF;
use finfo;
use Laravel\Ui\Presets\React;
use Symfony\Component\ErrorHandler\Debug;
use Illuminate\Support\Facades\Log;

class PaperController extends Controller
{
    public function draftapprovalpg1(Request $request)
    {
        if(Session::exists('paper')){
            $paper = Paper::find(session('paper')->id);
        }
        else if($request['paper_id'] == null){
            $paper = null;
        }else{
            $paper = Paper::find($request['paper_id']);
            if(Auth::user() !=  $paper->user){
                return redirect('/inbox');
            }
        }

        return view('create-paper.draft-approval-pg1', compact('paper'));
    }

    // public function draftapprovalpg2(Request $request)
    // {
    //     if (Session::exists('paper')) {
    //         $paper = session('paper');
    //     }else if($request['paper_subject'] != null){
    //         $paper = $this->CreatePaper($request);
    //     }else if(Auth::User() != null){
    //         return redirect('/draft-approval-pg1');
    //     }else{
    //         return redirect('/login');
    //     }
    //     return view('create-paper.draft-approval-pg2', compact('paper'));
    // }

    public function draftapprovalpg2()
    {
        info("asdasd");
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            return view('create-paper.draft-approval-pg2', compact('paper'));
        }else if(Auth::User() != null){
            return redirect('/draft-approval-pg1');
        }else{
            return redirect('/login');
        }
    }

    public function draftapprovalpg3()
    {
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            return view('create-paper.draft-approval-pg3', compact('paper'));
        }else if(Auth::User() != null){
            return redirect('/draft-approval-pg1');
        }else{
            return redirect('/login');
        }
    }

    public function selfassessmentpg1()
    {
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $papertypes = Papertype::all();
            return view('create-paper.self-assessment-pg1', compact('paper', 'papertypes'));
        }else if(Auth::User() != null){
            return redirect('/draft-approval-pg1');
        }else{
            return redirect('/login');
        }
    }

    public function selfassessmentpg2()
    {
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $papertypes = Papertype::all();
            return view('create-paper.self-assessment-pg2', compact('paper', 'papertypes'));
        }else if(Auth::User() != null){
            return redirect('/draft-approval-pg1');
        }else{
            return redirect('/login');
        }
    }

    public function selfassessmentpg3()
    {
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $projects = Project::where('papertype_id', $paper->papertype->id)->get();
            return view('create-paper.self-assessment-pg3', compact('paper', 'projects'));
        }else if(Auth::User() != null){
            return redirect('/draft-approval-pg1');
        }else{
            return redirect('/login');
        }
    }

    public function selfassessmentpg4()
    {
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            return view('create-paper.self-assessment-pg4', compact('paper'));
        }else if(Auth::User() != null){
            return redirect('/draft-approval-pg1');
        }else{
            return redirect('/login');
        }
    }

    public function selfassessmentpg5()
    {
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            return view('create-paper.self-assessment-pg5', compact('paper'));
        }else if(Auth::User() != null){
            return redirect('/draft-approval-pg1');
        }else{
            return redirect('/login');
        }
    }

    public function riskassessmentpg1()
    {
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $exclusions = Exclusion::all();
            return view('create-paper.risk-assessment-pg1', compact('paper', 'exclusions'));
        }else if(Auth::User() != null){
            return redirect('/draft-approval-pg1');
        }else{
            return redirect('/login');
        }
    }

    public function riskassessmentpg2()
    {
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $risks = Risk::where('paper_id', $paper->id)->get();
            return view('create-paper.risk-assessment-pg2', compact('paper', 'risks'));
        }else if(Auth::User() != null){
            return redirect('/draft-approval-pg1');
        }else{
            return redirect('/login');
        }
    }

    public function riskassessmentpg3(Request $request)
    {
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $riskcategories = Riskcategory::all();
            $risk = null;
            if(isset($request->risk_id)){
                $risk = Risk::find($request->risk_id);
            }
            return view('create-paper.risk-assessment-pg3', compact('paper', 'risk', 'riskcategories'));
        }else if(Auth::User() != null){
            return redirect('/draft-approval-pg1');
        }else{
            return redirect('/login');
        }
    }

    public function riskassessmentpg4(Request $request)
    {
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $risk = Risk::find($request->risk_id);
            return view('create-paper.risk-assessment-pg4', compact('paper', 'risk'));
        }else if(Auth::User() != null){
            return redirect('/draft-approval-pg1');
        }else{
            return redirect('/login');
        }
    }

    public function riskassessmentpg5(Request $request)
    {
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $risk_reviewers = $paper->riskrecipients->where('riskrecipient_role', 'Reviewer');
            $risk_endorsers = $paper->riskrecipients->where('riskrecipient_role', 'Endorser');
            return view('create-paper.risk-assessment-pg5', compact('paper', 'risk_reviewers', 'risk_endorsers'));
        }else if(Auth::User() != null){
            return redirect('/draft-approval-pg1');
        }else{
            return redirect('/login');
        }
    }

    public function CreatePaper(Request $request){
        if($request->paper_id == null){
            $paper = Paper::create([
                'paper_status' => "Draft"
            ]);
            $rpt = Rpt::create([
                'rpt_related_pdb' => $request['rpt_related_pdb'],
                'rpt_market_rate' => $request['rpt_market_rate'],
                'rpt_at_par' => $request['rpt_at_par'],
            ]);
            $rpt->paper()->associate($paper->id);
            $rpt->save();
        }else{
            $paper = Paper::find($request->paper_id);
            $rpt = $paper->rpt;
            $rpt['rpt_related_pdb'] = $request['rpt_related_pdb'];
            $rpt['rpt_market_rate'] = $request['rpt_market_rate'];
            $rpt['rpt_at_par'] =$request['rpt_at_par'];
            $rpt->save();
        }

        $paper->user()->associate(Auth::User()->id);
        $paper->save();

        if (Session::exists('paper')) {
            Session::forget('paper');
        }
        Session::put('paper', $paper);
        return redirect('/draft-approval-pg2');
    }

    public function DeleteDraftPaper(Request $request){
        info("delete paper id : " . $request->paper_id);
        $paper = Paper::find($request->paper_id);
        $paper->delete();
        if (Session::exists('paper')) {
            Session::forget('paper');
        }
        return redirect('/drafts');
    }

    public function PaperAddObjective(Request $request){
        if($request['objective_id'] == "" || $request['objective_id'] == null){
            $objective = Objective::create([
                'objective' => $request['objective']
            ]);
            $objective->paper()->associate($request['paper_id']);
            $objective->save();
            $mode = "create";
        }else{
            $objective = Objective::find($request['objective_id']);
            $objective->objective = $request['objective'];
            $objective->save();
            $mode = "edit";
        }
        // return response()->json(['projectthresholds' => $projectthresholds, 'count' => $count]);
        return response()->json(['objective_id' => $objective->id, 'mode' => $mode]);
    }

    public function PaperDeleteObjective(Request $request){
        $objective = Objective::find($request['objective_id']);
        $objective->delete();
        return response()->json();
    }

    public function PaperAddRecommendation(Request $request){
        info($request->all());
        if($request['recommendation_id'] == "" || $request['recommendation_id'] == null){
            $recommendation = Recommendation::create([
                'recommendation' => $request['recommendation']
            ]);
            $recommendation->paper()->associate($request['paper_id']);
            $recommendation->save();
            $mode = "create";
        }else{
            $recommendation = Recommendation::find($request['recommendation_id']);
            $recommendation->recommendation = $request['recommendation'];
            $recommendation->save();
            $mode = "edit";
        }
        // return response()->json(['projectthresholds' => $projectthresholds, 'count' => $count]);
        return response()->json(['recommendation_id' => $recommendation->id, 'mode' => $mode]);
    }

    public function PaperDeleteRecommendation(Request $request){
        $recommendation = Recommendation::find($request['recommendation_id']);
        $recommendation->delete();
        return response()->json();
    }

    public function PaperAddAttachment(Request $request){
        info("1 okay");
        info($request->all());
        if($request['attachment_id'] == "" || $request['attachment_id'] == null){
            info("2 okay");
            $document = $request->attachment;
            info($document);
            if($document != null){
                info("2.1 okay");
                $document->getRealPath();
                $document->getClientOriginalName();
                $document->getClientOriginalExtension();
                $document->getSize();
                $document->getMimeType();
                $fileExt = $document->getClientOriginalExtension();
                info($fileExt);
                $fileExt = strToLower($fileExt);
                $fileName = $request->paper_id . "_" . $document->getClientOriginalName();
                $destinationPath = "attachments";
                $document->move($destinationPath, $fileName);
                info("2.2 okay");
            }
            info("3 okay");
            $attachment = Attachment::create([
                'attachment_name' => $document->getClientOriginalName(),
                'attachment_category' => "Default",
            ]);
            info("4 okay");
            $attachment->paper()->associate($request['paper_id']);
            $attachment->save();
            $mode = "create";
            info("5 okay");
        }else{
            $attachment = Attachment::find($request['attachment_id']);
            $attachment->attachment = $request['attachment'];
            $attachment->save();
            $mode = "edit";
        }
        info("6 okay");
        // return response()->json(['projectthresholds' => $projectthresholds, 'count' => $count]);
        return response()->json(['attachment_id' => $attachment->id, 'mode' => $mode]);
        // return response()->json(['attachment_id' => "1", 'mode' => "create"]);
    }

    public function PaperDeleteAttachment(Request $request){
        $attachment = Attachment::find($request['attachment_id']);
        $attachment->delete();
        return response()->json();
    }
    
    public function PaperAddDratf(Request $request){
        if($request['paper_ref_number'] == "" || $request['paper_ref_number'] == null){
            $paper_ref_number = "N/A";
        }else{
            $paper_ref_number = $request['paper_ref_number'];
        }

        if($request['paper_importance'] == "" || $request['paper_importance'] == null){
            $paper_importance = "Default";
        }else{
            $paper_importance = $request['paper_importance'];
        }

        $paper = Paper::find($request->paper_id);
        $paper->paper_ref_number = $paper_ref_number;
        $paper->paper_subject = $request['paper_subject'];
        $paper->paper_importance = $paper_importance;
        $paper->paper_app_date = $request['paper_app_date'];
        $paper->paper_status = "Draft";
        $paper->paper_content = $request['paper_content'];
        $paper->paper_next_steps = $request['paper_next_steps'];

        $paper->save();

        if (Session::exists('paper')) {
            Session::forget('paper');
        }
        Session::put('paper', $paper);
        return redirect('/self-assessment-pg1');
    }

    public function PaperAddContent(Request $request){

        $paper = Paper::find(session('paper')->id);
        $paper['paper_content'] = $request->paper_content;
        $paper->save();

        if (Session::exists('paper')) {
            Session::forget('paper');
        }

        Session::put('paper', $paper);
        // $this->draftapprovalpg3();
        return redirect('/self-assessment-pg1');
        // return $paper;
    }

    public function PaperAddRPT(Request $request){
        
        $paper = Paper::find(session('paper')->id);
        if($paper->rpt == null){
            $rpt = Rpt::create([
                'rpt_related_pdb' => $request['rpt_related_pdb'],
                'rpt_market_rate' => $request['rpt_market_rate'],
                'rpt_at_par' => $request['rpt_at_par'],
            ]);
            $rpt->paper()->associate($paper->id);
            $rpt->save();
        }else{
            $rpt = $paper->rpt;
            $rpt['rpt_related_pdb'] = $request['rpt_related_pdb'];
            $rpt['rpt_market_rate'] = $request['rpt_market_rate'];
            $rpt['rpt_at_par'] =$request['rpt_at_par'];
            $rpt->save();
        }
        
        if (Session::exists('paper')) {
            Session::forget('paper');
        }

        Session::put('paper', $paper);
        return redirect('/self-assessment-pg2');
    }
    
    public function PaperAddPaperType(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $paper->papertype()->associate($request->papertype_id);
            $paper->save();
            Session::forget('paper');
            Session::put('paper', $paper);
            return redirect('/self-assessment-pg3');
        }else{
            return redirect('/inbox');
        }
    }

    public function PaperAddProjectthreshold(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $paper->projectthreshold()->associate($request->projectthreshold_id);
            $paper->save();
            Session::forget('paper');
            Session::put('paper', $paper);
            return redirect('/self-assessment-pg4');
        }else{
            return redirect('/inbox');
        }
    }

    public function PaperAddRecipient(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $user = User::where('email', $request->recipient_email)->first();
            $recipient = Recipient::create([
                'recipient_category' => $request['recipient_category'],
                'recipient_status' => "Pending",
            ]);
            $recipient->paper()->associate($request->paper_id);
            $recipient->user()->associate($user->id);
            $recipient->save();
            Session::put('paper', $paper);
            return response()->json(['recipient' => $recipient, 'user' => $user, 'mode' => "create"]);
        }else{
            return redirect('/inbox');
        }
    }

    public function PaperDeleteRecipient(Request $request){
        if (Session::exists('paper')) {
            $recipient = Recipient::find($request['recipient_id']);
            $recipient->delete();
            return response()->json();
        }else{
            return redirect('/inbox');
        }
    }

    public function PaperAddExclusion(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            if($request->exclusion_id != ""){
                $paper->exclusion()->associate($request->exclusion_id);
                $paper->save();
                return redirect('/risk-assessment-pg5');
            }
            Session::forget('paper');
        }
        Session::put('paper', $paper);
        if($request->exclusion_id == ""){
            $paper->exclusion_id = null;
            $paper->save();
            return redirect('/risk-assessment-pg2');
        }else{
            return redirect('/inbox');
        }
    }

    public function PaperAddRisk(Request $request){
        if (Session::exists('paper')) {
            info("risk_current_rating : " . $request['risk_current_rating']);
            $paper = Paper::find(session('paper')->id);
            if($request['risk_id'] != null){
                $risk = Risk::find($request['risk_id']);
                $risk->risk_title = $request['risk_title'];
                $risk->risk_category = $request['risk_category'];
                $risk->risk_causes = $request['risk_causes'];
                $risk->risk_potential_impact = $request['risk_potential_impact'];
                $risk->risk_likelihood_rating = $request['risk_likelihood_rating'];
                $risk->risk_impact_rating = $request['risk_impact_rating'];
                $risk->risk_current_rating = $request['risk_current_rating'];
                $risk->risk_assumptions_likehood = $request['risk_assumptions_likehood'];
                $risk->risk_assumptions_impact = $request['risk_assumptions_impact'];
                $risk->risk_remark = $request['risk_remark'];
                $risk->riskcategory()->associate($request['riskcategory_id']);
            }else{
                $risk = Risk::create([
                    'risk_title' => $request['risk_title'],
                    'risk_category' => $request['risk_category'],
                    'risk_causes' => $request['risk_causes'],
                    'risk_potential_impact' => $request['risk_potential_impact'],
                    'risk_likelihood_rating' => $request['risk_likelihood_rating'],
                    'risk_impact_rating' => $request['risk_impact_rating'],
                    'risk_current_rating' => $request['risk_current_rating'],
                    'risk_assumptions_likehood' => $request['risk_assumptions_likehood'],
                    'risk_assumptions_impact' => $request['risk_assumptions_impact'],
                    'risk_remark' => $request['risk_remark'],
                ]);
                $risk->riskcategory()->associate($request['riskcategory_id']);
                $risk->paper()->associate($paper->id);
            }
            $risk->save();
            Session::forget('paper');
        }
        Session::put('paper', $paper);
        return redirect('/risk-assessment-pg4/'.$risk->id);
    }

    public function PaperDeleteRisk(Request $request){
        if (Session::exists('paper')) {
            info("risk_current_rating : " . $request['risk_current_rating']);
            $paper = Paper::find(session('paper')->id);
            $risk = Risk::find($request['risk_id']);
            $risk->delete();
            Session::forget('paper');
        }
        Session::put('paper', $paper);
        return redirect('/risk-assessment-pg2');
    }

    public function PaperAddRiskTreatment(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $risk = Risk::find($request['risk_id']);
            $risk->risk_target_likehood_rating = $request['risk_target_likehood_rating'];
            $risk->risk_target_impact_rating = $request['risk_target_impact_rating'];
            $risk->risk_target_rating = $request['risk_target_rating'];
            $risk->save();
            Session::forget('paper');
        }
        Session::put('paper', $paper);
        return redirect('/risk-assessment-pg2');
    }

    public function PaperAddMitigation(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $mitigation_owner = User::where('email', $request['mitigation_owner'])->first();
            if($request['mitigation_id'] == ""){
                $mitigation = Mitigation::create([
                    'mitigation_detail' => $request['mitigation_detail'],
                ]);
                $mitigation->risk()->associate($request['risk_id']);
                $mitigation->user()->associate($mitigation_owner['id']);
            }else{
                $mitigation = Mitigation::find($request['mitigation_id']);
                $mitigation->mitigation_detail =  $request['mitigation_detail'];
                $mitigation->user()->dissociate();
                $mitigation->user()->associate($mitigation_owner['id']);
            }
            $mitigation->save();
            Session::forget('paper');
        }
        Session::put('paper', $paper);
        echo $mitigation;
        return redirect('/risk-assessment-pg4/'.$request['risk_id']);
    }
    
    public function PaperRemoveMitigation(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $mitigation = Mitigation::find($request['mitigation_id']);
            $mitigation->delete();
            Session::forget('paper');
        }
        Session::put('paper', $paper);
        echo $mitigation;
        return redirect('/risk-assessment-pg4/'.$request['risk_id']);
    }
    
    public function PaperAddReviewer(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $reviewer = User::where('email', $request['reviewer_email'])->first();
            $riskreviewer = Riskreviewer::create([
                'reviewer_role' => $request['reviewer_role'],
            ]);
            $riskreviewer->paper()->associate(session('paper')->id);
            $riskreviewer->reviewer()->associate($reviewer->id);
            $riskreviewer->save();
            Session::forget('paper');
        }
        Session::put('paper', $paper);
        return redirect('/risk-assessment-pg5');
    }

    public function PaperRemoveReviewer(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $riskreviewer = Riskreviewer::find($request->riskreviewer_id);
            $riskreviewer->delete();
        }
        Session::put('paper', $paper);
        return redirect('/risk-assessment-pg5');
    }

    public function PaperAddEndorser(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $endorser = User::where('email', $request['endorser_email'])->first();
            $riskendorser = Riskendorser::create([
                'endorser_role' => $request['endorser_role'],
            ]);
            $riskendorser->paper()->associate(session('paper')->id);
            $riskendorser->endorser()->associate($endorser->id);
            $riskendorser->save();
            Session::forget('paper');
        }
        Session::put('paper', $paper);
        return redirect('/risk-assessment-pg5');
    }

    public function PaperRemoveEndorser(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $riskendorser = Riskendorser::find($request->riskendorser_id);
            $riskendorser->delete();
        }
        Session::put('paper', $paper);
        return redirect('/risk-assessment-pg5');
    }

    public function PaperAddRiskRemarks(Request $request){
        if (Session::exists('paper')) {
            $paper = Paper::find(session('paper')->id);
            $paper->paper_riskremark = $request['paper_riskremark'];
            $paper->save();
            Session::forget('paper');
        }
        Session::put('paper', $paper);
        return redirect('/risk-assessment-pg5');
    }
    
    public function GetProjectThresholds(Request $request){
        $projectthresholds = Projectthreshold::where("project_id", $request->project_id)->get();
        info($projectthresholds);
        $count = $projectthresholds->count();
        return response()->json(['projectthresholds' => $projectthresholds, 'count' => $count]);
    }
    
    public function papersubmissionpg1(Request $request){
        $paper = Paper::find($request->paper_id);
        return view('create-paper.submission-pg1', compact('paper'));
    }
    
    public function summarypg1(Request $request){
        $paper = Paper::find($request->paper_id);
        $pdf = PDF::loadView('create-paper.summary-pdf', compact('paper'))->save( 'papers/' . $paper->id . '.pdf' );
        return view('create-paper.summary-pg1', compact('paper', 'pdf'));
    }

    public function PaperSubmit(Request $request){
        $paper = Paper::find($request->paper_id);
        $paper->paper_status = "Pending Reviewal";
        $paper->save();
        // $pdf = PDF::loadView('create-paper.summary-pdf', compact('paper'));
        // $pdf->download($paper->id . '.pdf');
        // return view('create-paper.summary-pdf', compact('paper', 'pdf'));

        return redirect('/inbox');
    }
}
