@guest
please login
@else
    @extends('layouts.app')
    @section('content')
    {{ Session::flush() }}
    <script>
        window.location = "home";
    </script>
    @endsection
@endguest