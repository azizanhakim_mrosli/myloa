<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">


  {{-- *********************************************************************** --}}
  <link rel="icon" href="{{ asset('images/petronas-logo-white.jpg') }}">

  <!-- CSS Links -->
  {{-- <link rel="stylesheet" type= "text/css" href= "{{ asset('css/app.css') }}"> --}}
  <link rel="stylesheet" type="text/css" href="{{ asset('css/components-v2.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/asset.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/tailwind.css') }}">


  <!-- General JS -->
  <script src="{{ asset('js/components-v2.js') }}"></script>
  <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.3.5/dist/alpine.js" defer=""></script>

</head>

<body class="bg-gray-100" style="height: 100%; border:none solid 2px">
  <div id="app" style="height: 100%;; border:none solid 2px;">
    <!-- Navigation Bar -->
    <div style="border:none solid 2px; height:10%;">
      <nav class="navi" style="border:none solid 1px; height:100%">
        <div class="flex items-center h-full">
          <div class=" mx-auto py-2 px-2 w-full sm:px-6 lg:px-8">
            <div class="relative flex justify-between h-16">
              <div class="absolute inset-y-0 left-0 flex items-center sm:hidden">
                <!-- Mobile menu button -->
                <button type="button" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500" aria-controls="mobile-menu" aria-expanded="false">
                  <span class="sr-only">Open main menu</span>
                  <!--
                  Icon when menu is closed.
                  Heroicon name: outline/menu
                  Menu open: "hidden", Menu closed: "block"
                  -->
                  <svg class="block h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                  </svg>
                  <!--
                  Icon when menu is open.
                  Heroicon name: outline/x
                  Menu open: "block", Menu closed: "hidden"
                  -->
                  <svg class="hidden h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                  </svg>
                </button>
              </div>
              <div class="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                <div class="flex-shrink-0 flex items-center px-4">
                  <!-- If logged in, prevent redirecting to home.html, redirect to respective user role dashboard instead -->
                  @if (Auth::user() != null)
                  <a href="{{ url('home') }}">
                    <img class="block lg:hidden h-12 w-15" src="images/petronas-logo-white.jpg" alt="Petronas">
                    <img class="hidden lg:block h-12 w-15" src="{{ url('images/petronas-logo-white.jpg') }}" alt="Petronas">
                  </a>
                  {{-- <!-- If logged in, display the nav bar for respective roles -->
                  @if (Auth::user()->user_role == "Staff")
                    <div class="hidden sm:ml-6 sm:flex sm:space-x-8 px-8">
                    <a href="{{url('/inbox')}}" class="border-transparent text-white hover:border-gray-300 hover:text-white-700 inline-flex items-center px-1 pt-1 border-b-2 text-m font-medium">
                      Dashboard
                    </a>
                    <a href="staff_loalibrary" class="border-transparent text-white hover:border-gray-300 hover:text-white-700 inline-flex items-center px-1 pt-1 border-b-2 text-m font-medium">
                      LOA Library
                    </a> --}}
                    <!-- If logged in, display the nav bar for respective roles -->
                    @if (Auth::user()->user_role == "Staff")
                      <div class="hidden sm:ml-6 sm:flex sm:space-x-8 px-8">
                        <a href="{{url('/inbox')}}" class="border-transparent text-white hover:border-gray-300 hover:text-white-700 inline-flex items-center px-1 pt-1 border-b-2 text-m font-medium">
                          Dashboard
                        </a>
                        <a href="staff_loalibrary" class="border-transparent text-white hover:border-gray-300 hover:text-white-700 inline-flex items-center px-1 pt-1 border-b-2 text-m font-medium">
                          LOA Library
                        </a>
                      </div>
                    @else
                      <div class="hidden sm:ml-6 sm:flex sm:space-x-8 px-8">
                        <a href="{{url('/mytracker')}}"
                          class="border-transparent text-white hover:border-gray-300 hover:text-white-700 inline-flex items-center mx-5 px-1 pt-1 border-b-2 text-m font-medium">
                          Dashboard
                        </a>
                        <a href="admin_loalibrary"
                          class="border-transparent text-white hover:border-gray-300 hover:text-white-700 inline-flex items-center px-1 pt-1 border-b-2 text-m font-medium">
                          LOA Library
                        </a>
                      </div>
                    @endif
                    @else
                    <a href="/MyLOA/public">
                      <img class="block lg:hidden h-12 w-15" src="images/petronas-logo-white.jpg" alt="Petronas">
                      <img class="hidden lg:block h-12 w-15  " src="images/petronas-logo-white.jpg" alt="Petronas">
                    </a>
                    @endif
                  </div>
                  <div class="absolute inset-y-0 right-0 flex items-center pr-2 sm:ml-6 sm:pr-0">
                    <a href='files/myloa-user-manual.pdf' target="_blank">
                      <button class="bg-white p-1 rounded-full text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-600">
                        <span class="sr-only">View notifications</span>
                        <!-- Heroicon name: user guide icon -->
                        <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                          <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd" />
                        </svg>
                      </button>
                    </a>
                    @if (Auth::user() != null)
                    <!-- Profile dropdown -->
                    <div @click.away="open = false" class="ml-3 relative" x-data="{ open: false }" style="padding-right: 1rem;">
                      <div>
                        <button @click="open = !open" class="max-w-xs bg-white flex items-center text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-600" id="user-menu" aria-haspopup="true" x-bind:aria-expanded="open">
                          <span class="sr-only">Open user menu</span>
                          <img class="h-8 w-8 rounded-full" src="images/profile-picture.png" alt="">
                        </button>
                      </div>
                      <div x-show="open" x-description="Profile dropdown panel, show/hide based on dropdown state." x-transition:enter="transition ease-out duration-100" x-transition:enter-start="transform opacity-0 scale-95" x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100" x-transition:leave-end="transform opacity-0 scale-95" class="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5" role="menu" aria-orientation="vertical" aria-labelledby="user-menu" style="display: none;">
                        <a href="#" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">Your Profile</a>
                        <a href="logout" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">Sign out</a>
                      </div>
                    </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
      </nav>
    </div>
    <!-- vertical green bar left & right -->
    <div class="left pt-0" style="background-color: #38B09D; padding-bottom: 0px; height:90%; width:3%; border:none solid 1px; ">&nbsp</div>
    <div class="right pt-0" style="background-color: #38B09D; padding-bottom: 0px; height:90%; width:3%; border:none solid 1px; ">&nbsp</div>
    <div id="MainDiv" class="overflow-auto" style="height: 90%; ">
      <main class="bg-gray-100">
        @yield('content')
      </main>
    </div>
    {{-- <div class="right-0  pt-0" style="background-color: #38B09D; padding-bottom: 0px; height:80%; width:5%; border:black solid 1px; ">&nbsp</div> --}}

    {{-- <div class="right-0 flex flex-col " style="background-color: #38B09D; padding-bottom: 0px; width:100%; border:black solid 1px; ">&nbsp</div> --}}
  </div>
  {{-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
  {{ config('app.name', 'Laravel') }}
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <!-- Left Side Of Navbar -->
    <ul class="navbar-nav mr-auto">

    </ul>

    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav ml-auto">
      <!-- Authentication Links -->
      @guest
      @if (Route::has('login'))
      <li class="nav-item">
        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
      </li>
      @endif

      @if (Route::has('register'))
      <li class="nav-item">
        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
      </li>
      @endif
      @else
      <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
          {{ Auth::user()->name }}
        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
          </form>
        </div>
      </li>
      @endguest
    </ul>
  </div>
  </div>
  </nav> --}}

  {{-- <main class="py-4"> --}}
  {{-- <main class="" style="height: 100%; background-color:black">
        @yield('content')
    </main> --}}
</body>
{{-- <footer class="footerstyle">
  <!-- Copyright -->
  <div class="text-center p-3" >
      <p>© 2021 Copyright | Digital Division, Petronas Dagangan Berhad (PDB)</p>
  </div>
  <!-- Copyright -->
</footer> --}}

</html>