@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="//cdn.materialdesignicons.com/3.7.95/css/materialdesignicons.min.css">
<link rel="stylesheet" href="https://rsms.me/inter/inter.css">

<!-- Content for dashboard page -->
<div class="grid grid-cols-4 h-full w-full">
  <!-- left navigation -->
  <div class="h-full border" style="background-color: #F5F5F5;">
      <!-- Create report button -->
      <div class="hidden md:block m-4 w-64 p-6 overflow-y-auto bg-gray-100 ">
        <a href="create-paper">
          <button type="button"
            class=" inline-flex items-center justify-center px-1 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-black bg-opacity-75 hover:bg-gray-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-600 w-full">
            &plus; Create Paper
          </button>
        </a>
        <!-- left navigation  -->
        <nav>
          <div class="mt-3">
            <a href="{{url('/inbox')}}"
              class="-mx-3  py-1 px-3 text-sm font-medium flex items-center justify-center hover:bg-gray-300 rounded-lg border-2 ">
              <span>
                <i class="h-6 w-6 fa fa-envelope-o fill-current text-gray-700" aria-hidden="true"></i>
                <span class=" text-gray-900 ">Inbox</span>
              </span>
            </a>
          </div>
          <div class="mt-3">
            <a href="{{ url('/sent') }}"
              class="-mx-3  py-1 px-3 text-sm font-medium flex items-center justify-center bg-gray-300 border-2 border-gray-400 border-opacity-100 rounded-lg">
              <span>
                <i class="h-6 w-6 fa fa-flag-o fill-current text-gray-700" aria-hidden="true"></i>
                <span class=" text-gray-900">Sent</span>
              </span>
            </a>
          </div>
          <div class="mt-3">
            <a href="{{ url('/drafts') }}"
              class="-mx-3  py-1 px-3 text-sm font-medium flex items-center justify-center hover:bg-gray-300 rounded-lg border-2">
              <span>
                <i class="h-6 w-6 fa fa-flag-o fill-current text-gray-700" aria-hidden="true"></i>
                <span class=" text-gray-900">Draft</span>
              </span>
            </a>
          </div>
          <div class="mt-3">
            <a href="{{ url('/mytracker') }}"
              class="-mx-3  py-1 px-3 text-sm font-medium flex items-center justify-center hover:bg-gray-300 rounded-lg border-2">
              <span>
                <i class="h-6 w-6 fa fa-flag-o fill-current text-gray-700" aria-hidden="true"></i>
                <span class=" text-gray-900 ">My Tracker</span>
              </span>
            </a>
          </div>
        </nav>
      </div>
  </div>

  <!-- content at right  -->
  <div class="col-span-3">
      <div class="grid grid-cols-2" style="background-color: #F5F5F5;">
        <div class="col-span-2 px-6 ml-4 mr-4 pb-16 mt-5" style="height:70%" id="style-1">
          <!-- sent  -->
          @foreach ($papers as $paper)
            <a onclick="GetSentPaper('{{$paper->id}}')" class="block cursor-pointer bg-white border">
              <div class="pt-5 px-4 py-2 flex content-center">
                <img class="h-8 w-8 m-2 rounded-full" src="images/profile-picture.png" alt="">
                <span class="w-full text-sm m-2 self-center font-semibold text-gray-900">
                  @php
                  if ($paper->paper_status == "Wait Risk") {
                    $endorserid = DB::table('riskrecipients')->where('paper_id', $paper->id)->where('riskrecipient_role', 'Endorser')->value('user_id');
                    echo(DB::table('users')->where('id', $endorserid)->value('name'));
                  }else {
                    $recipientid = DB::table('recipients')->where('paper_id', $paper->id)->where('recipient_category', 'Approver')->value('user_id');
                    echo(DB::table('users')->where('id', $recipientid)->value('name'));
                  }
                  @endphp
                </span>
                <div id="risk_status" class="w-auto whitespace-nowrap px-2 text-xs text-right self-center justify-self-end leading-5 font-semibold bg-green-100 text-green-800">
                  @php
                  if($paper->paper_status == "Risks Approved"){
                  echo($paper->paper_status);
                  }elseif ($paper->paper_status == "Wait Risk") {
                  echo("Waiting for Risk Assessment Endorsement");
                  }else {
                  echo($paper->paper_status);
                  }
                  @endphp
                </div>
              </div>
              <span class="text-sm font-semibold text-gray-500 px-9 py-2">Paper Subject: {{$paper->paper_subject}}</span>
              <br>
              <br>
            </a>
          @endforeach
        </div>
      </div>

    {{-- modal for view sent --}}
    <div id="ViewSentModal1" class="modal overflow-y-auto" style="border: none; padding-top: 100px; background-color:rgba(56, 56, 56, 0.08)">
      <div class="row justify-content-center" id="ViewSentModal2">
        <div class="col-md-8 row justify-content-center" id="ViewSentModal3" style="border: none;">
          <div class="card" id="editCard2" style="border: none; width: 100%">
            <div class="card-header" style="background-color:#00a57c; color:white; width:100%;">
              <b style="width: 100%;" id="Paper_Subject"></b>
              <button class="float-left"onclick="ViewSentCloseFunction()">x</button>
            </div>
            <div class="card-body justify-content-center">
              <h4 style="padding-bottom: 10px;">Approvers: </h4>
              <table class="min-w-full divide-y" id="Table_Approvers">
                <thead style="background:; border-radius: 25px;">
                  <tr style="">
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                      No
                    </th>
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      Approvers
                    </th>
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      Status
                    </th>
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr id="Table_Approvers_Tr" class="Table_Approvers_Tr">
                    <td id="Table_Approvers_Td_No" class="text-center Table_Approvers_Td_No">
                    </td>
                    <td id="Table_Approvers_Td_Content">
                    </td>
                  </tr>
                </tbody>
              </table>
              <br>
              <h4 style="padding-bottom: 10px;">Reviewers: </h4>
              <table class="min-w-full divide-y" id="Table_Reviewers">
                <thead style="background:; border-radius: 25px;">
                  <tr style="">
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                      No
                    </th>
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      Reviewers
                    </th>
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      Status
                    </th>
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr id="Table_Reviewers_Tr" class="Table_Reviewers_Tr">
                    <td id="Table_Reviewers_Td_No" class="text-center Table_Reviewers_Td_No">
                    </td>
                    <td id="Table_Reviewers_Td_Content">
                    </td>
                  </tr>
                </tbody>
              </table>
              <br>
              <h4 style="padding-bottom: 10px;">CC: </h4>
              <table class="min-w-full divide-y" id="Table_CCs">
                <thead style="background:; border-radius: 25px;">
                  <tr style="">
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                      No
                    </th>
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      CCs
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr id="Table_CCs_Tr" class="Table_CCs_Tr">
                    <td id="Table_CCs_Td_No" class="text-center Table_CCs_Td_No">
                    </td>
                    <td id="Table_CCs_Td_Content">
                    </td>
                  </tr>
                </tbody>
              </table>
              <br>
              <h4 style="padding-bottom: 10px;">Related Party & Transfer Pricing (TP) : </h4>
              <table class="min-w-full divide-y" id="Table_RPTs">
                <tbody>
                  <tr id="Table_RPTs_Tr1" class="Table_RPTs_Tr">
                    <td id="Table_RPTs_Td_No1" class="text-center Table_RPTs_Td_No">
                      1
                    </td>
                    <td id="Table_RPTs_Td_Content11" class="px-8">
                      Is the transacting party related to PDB?	
                    </td>
                    <td id="Table_RPTs_Td_Content12">
                    </td>
                  </tr>
                  <tr id="Table_RPTs_Tr2" class="Table_RPTs_Tr">
                    <td id="Table_RPTs_Td_No2" class="text-center Table_RPTs_Td_No">
                      2
                    </td>
                    <td id="Table_RPTs_Td_Content21" class="px-8">
                      Is the agreed price within prevailing market rate and on normal commersial terms?	
                    </td>
                    <td id="Table_RPTs_Td_Content22">
                    </td>
                  </tr>
                  <tr id="Table_RPTs_Tr3" class="Table_RPTs_Tr">
                    <td id="Table_RPTs_Td_No3" class="text-center Table_RPTs_Td_No">
                      3
                    </td>
                    <td id="Table_RPTs_Td_Content31" class="px-8">
                      Are the material terms at par with the non-related party?	
                    </td>
                    <td id="Table_RPTs_Td_Content32">
                    </td>
                  </tr>
                </tbody>
              </table>
              <br>
              <h4 style="padding-bottom: 10px;">Objectives : </h4>
              <table class="min-w-full divide-y" id="Table_Objectives">
                <thead style="background:; border-radius: 25px;">
                  <tr style="">
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                      No
                    </th>
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      Objectives
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr id="Table_Objectives_Tr" class="Table_Objectives_Tr">
                    <td id="Table_Objectives_Td_No" class="text-center Table_Objectives_Td_No">
                    </td>
                    <td id="Table_Objectives_Td_Content">
                    </td>
                  </tr>
                </tbody>
              </table>
              <br>
              <h4 style="padding-bottom: 10px;">Body : </h4>
              <textarea readonly id="Paper_Content" maxlength="10000"  name="paper_content" rows="10" placeholder="Paper Content Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max"></textarea>
              <br>
              <h4 style="padding-bottom: 10px;">Attachments : </h4>
              <table class="min-w-full divide-y" id="Table_Attachments">
                <thead style="background:; border-radius: 25px;">
                  <tr style="">
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                      No
                    </th>
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      Attachments
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr id="Table_Attachments_Tr" class="Table_Attachments_Tr">
                    <td id="Table_Attachments_Td_No" class="text-center Table_Attachments_Td_No">
                    </td>
                    <td id="Table_Attachments_Td_Content">
                    </td>
                  </tr>
                </tbody>
              </table>
              <br>
              <h4 style="padding-bottom: 10px;">Recommendations : </h4>
              <table class="min-w-full divide-y" id="Table_Recommendations">
                <thead style="background:; border-radius: 25px;">
                  <tr style="">
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                      No
                    </th>
                    <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                      Recommendations
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr id="Table_Recommendations_Tr" class="Table_Recommendations_Tr">
                    <td id="Table_Recommendations_Td_No" class="text-center Table_Recommendations_Td_No">
                    </td>
                    <td id="Table_Recommendations_Td_Content">
                    </td>
                  </tr>
                </tbody>
              </table>
              <br>
              <h4 style="padding-bottom: 10px;">Next Steps : </h4>
              <textarea readonly id="Paper_Next_Steps" maxlength="10000"  name="Paper_Next_Steps" rows="10" placeholder="Paper Next Steps" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max"></textarea>
              <br>
              <div id="AttachmentModal1" class="modal" style="border: none; padding-top:10%; background-color:rgba(56, 56, 56, 0.08)">
                <div class="row justify-content-center" id="AttachmentModal2" >
                  <div class="col-md-8 row justify-content-center" id="AttachmentModal3" style="border: none; text-align:center" >
                    <div class="card" id="editCard2" style="border: none; width: 100%">
                      <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('Attachment') }}</b>
                      </div>
                      <div class="card-body justify-content-center">
                        <p><span id="AttachmentSpanName"></span></p>
                        <div class="flex justify-center pb-3 border-2 border-gray-300 border-dashed rounded-md">
                          <div class="space-y-1 text-center" style="width:100%">
                            <center><span id="AttachmentSpan"></span></center>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {{-- end of div for card body --}}
            </div>
          </div>
        </div>
      </div>
    </div>

      <!-- report content  -->
    <div class=" bg-gray-100 flex-grow ">
      <div id="DraftDiv" class="h-screen bg-gray-200 overflow-y-auto" style="background-color: ; display:none">
        <div style="padding-top: 120px; padding-left: 45px; padding-right: 40px;">
          <!-- Content goes here -->
          {{-- {% for draft_report in draft_report %} --}}
            {{-- <h1 class="demoFont">{{draft_report.subject}}</h1>
            <h4 style="padding-bottom: 10px;">Approvers: {{draft_report.approver_email}}</h4>
            <h4 style="padding-bottom: 10px;">CC: {{draft_report.cc_email}}</h4>
            <div class="myButton"><p>{{draft_report.report_type}}</p></div> <div class="myButton"><p>{{draft_report.security_level}}</p></div>
            <p style="padding-top: 10px; padding-bottom: 50px;">{{draft_report.message}}</p> --}}
            <h1 id="Paper_Subject" class="demoFont"></h1>
            <h4 style="padding-bottom: 10px;">Approvers: </h4>
            <table class="min-w-full divide-y" id="Table_Approvers">
              <thead style="background:; border-radius: 25px;">
                <tr style="">
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                    No
                  </th>
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Approvers
                  </th>
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Status
                  </th>
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr id="Table_Approvers_Tr" class="Table_Approvers_Tr">
                  <td id="Table_Approvers_Td_No" class="text-center Table_Approvers_Td_No">
                  </td>
                  <td id="Table_Approvers_Td_Content">
                  </td>
                </tr>
              </tbody>
            </table>
            <br>
            <h4 style="padding-bottom: 10px;">Reviewers: </h4>
            <table class="min-w-full divide-y" id="Table_Reviewers">
              <thead style="background:; border-radius: 25px;">
                <tr style="">
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                    No
                  </th>
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Reviewers
                  </th>
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Status
                  </th>
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr id="Table_Reviewers_Tr" class="Table_Reviewers_Tr">
                  <td id="Table_Reviewers_Td_No" class="text-center Table_Reviewers_Td_No">
                  </td>
                  <td id="Table_Reviewers_Td_Content">
                  </td>
                </tr>
              </tbody>
            </table>
            <br>
            <h4 style="padding-bottom: 10px;">CC: </h4>
            <table class="min-w-full divide-y" id="Table_CCs">
              <thead style="background:; border-radius: 25px;">
                <tr style="">
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                    No
                  </th>
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    CCs
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr id="Table_CCs_Tr" class="Table_CCs_Tr">
                  <td id="Table_CCs_Td_No" class="text-center Table_CCs_Td_No">
                  </td>
                  <td id="Table_CCs_Td_Content">
                  </td>
                </tr>
              </tbody>
            </table>
            <br>
            <h4 style="padding-bottom: 10px;">Related Party & Transfer Pricing (TP) : </h4>
            <table class="min-w-full divide-y" id="Table_RPTs">
              {{-- <thead style="background:; border-radius: 25px;">
                <tr style="">
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                    No
                  </th>
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    RPTs
                  </th>
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  </th>
                </tr>
              </thead> --}}
              <tbody>
                <tr id="Table_RPTs_Tr1" class="Table_RPTs_Tr">
                  <td id="Table_RPTs_Td_No1" class="text-center Table_RPTs_Td_No">
                    1
                  </td>
                  <td id="Table_RPTs_Td_Content11" class="px-8">
                    Is the transacting party related to PDB?	
                  </td>
                  <td id="Table_RPTs_Td_Content12">
                  </td>
                </tr>
                <tr id="Table_RPTs_Tr2" class="Table_RPTs_Tr">
                  <td id="Table_RPTs_Td_No2" class="text-center Table_RPTs_Td_No">
                    2
                  </td>
                  <td id="Table_RPTs_Td_Content21" class="px-8">
                    Is the agreed price within prevailing market rate and on normal commersial terms?	
                  </td>
                  <td id="Table_RPTs_Td_Content22">
                  </td>
                </tr>
                <tr id="Table_RPTs_Tr3" class="Table_RPTs_Tr">
                  <td id="Table_RPTs_Td_No3" class="text-center Table_RPTs_Td_No">
                    3
                  </td>
                  <td id="Table_RPTs_Td_Content31" class="px-8">
                    Are the material terms at par with the non-related party?	
                  </td>
                  <td id="Table_RPTs_Td_Content32">
                  </td>
                </tr>
              </tbody>
            </table>
            <br>
            <h4 style="padding-bottom: 10px;">Objectives : </h4>
            <table class="min-w-full divide-y" id="Table_Objectives">
              <thead style="background:; border-radius: 25px;">
                <tr style="">
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                    No
                  </th>
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Objectives
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr id="Table_Objectives_Tr" class="Table_Objectives_Tr">
                  <td id="Table_Objectives_Td_No" class="text-center Table_Objectives_Td_No">
                  </td>
                  <td id="Table_Objectives_Td_Content">
                  </td>
                </tr>
              </tbody>
            </table>
            <br>
            <h4 style="padding-bottom: 10px;">Body : </h4>
            <textarea readonly id="Paper_Content" maxlength="10000"  name="paper_content" rows="10" placeholder="Paper Content Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max"></textarea>
            <br>
            <h4 style="padding-bottom: 10px;">Attachments : </h4>
            <table class="min-w-full divide-y" id="Table_Attachments">
              <thead style="background:; border-radius: 25px;">
                <tr style="">
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                    No
                  </th>
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Attachments
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr id="Table_Attachments_Tr" class="Table_Attachments_Tr">
                  <td id="Table_Attachments_Td_No" class="text-center Table_Attachments_Td_No">
                  </td>
                  <td id="Table_Attachments_Td_Content">
                  </td>
                </tr>
              </tbody>
            </table>
            <br>
            <h4 style="padding-bottom: 10px;">Recommendations : </h4>
            <table class="min-w-full divide-y" id="Table_Recommendations">
              <thead style="background:; border-radius: 25px;">
                <tr style="">
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                    No
                  </th>
                  <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Recommendations
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr id="Table_Recommendations_Tr" class="Table_Recommendations_Tr">
                  <td id="Table_Recommendations_Td_No" class="text-center Table_Recommendations_Td_No">
                  </td>
                  <td id="Table_Recommendations_Td_Content">
                  </td>
                </tr>
              </tbody>
            </table>
            <br>
            <h4 style="padding-bottom: 10px;">Next Steps : </h4>
            <textarea readonly id="Paper_Next_Steps" maxlength="10000"  name="Paper_Next_Steps" rows="10" placeholder="Paper Next Steps" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max"></textarea>
            <br>
            <div id="AttachmentModal1" class="modal" style="border: none; padding-top:10%; background-color:rgba(56, 56, 56, 0.08)">
              <div class="row justify-content-center" id="AttachmentModal2" >
                <div class="col-md-8 row justify-content-center" id="AttachmentModal3" style="border: none; text-align:center" >
                  <div class="card" id="editCard2" style="border: none; width: 100%">
                    <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('Attachment') }}</b>
                    </div>
                    <div class="card-body justify-content-center">
                      <p><span id="AttachmentSpanName"></span></p>
                      <div class="flex justify-center pb-3 border-2 border-gray-300 border-dashed rounded-md">
                        <div class="space-y-1 text-center" style="width:100%">
                          <center><span id="AttachmentSpan"></span></center>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {{-- <div class="myButton"><p></p></div> <div class="myButton"><p></p></div> --}}
            {{-- <div class="flex justify-end pt-5 pb-5">
              <a id="ButtonEditDraft" href="" ><button class="btn btn-primary">Edit</button></a>&nbsp; &nbsp;
              <a id="ButtonDeleteDraft" href="" ><button class="btn btn-primary">Delete</button></a>
            </div> --}}
          {{-- {% endfor %} --}}
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

<script>

  function ViewSentCloseFunction() {
    document.getElementById("ViewSentModal1").style.display = "none";
  };
  
  function GetSentPaper(paper_id){
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/GetSentPaper")}}',
      dataType: 'JSON',
      async:true, //ensure process finish then exit the function
      data: {
        paper_id : paper_id
      },
      success: function (data) {
        document.getElementById("ViewSentModal1").style.display = "block";
        $('#Table_Approvers .Table_Approvers_Tr').remove();
        for(var i in data.recipients_approver){
          var recipient_approver = data.recipients_approver[i];
          $('#Table_Approvers')
            .append($('<tr>', { class   : "Table_Approvers_Tr"})
                .append($('<td>', { class   : "text-center Table_Approvers_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Approvers_Td_Content px-8", 
                                    text    : recipient_approver.email}))
                .append($('<td>', { class   : "Table_Approvers_Td_Content px-8", 
                                    text    : recipient_approver.recipient_status}))
            );
        }
        $('#Table_Reviewers .Table_Reviewers_Tr').remove();
        for(var i in data.recipients_reviewer){
          var recipient_reviewer = data.recipients_reviewer[i];
          $('#Table_Reviewers')
            .append($('<tr>', { class   : "Table_Reviewers_Tr",
                                id      : "Table_Reviewers_Tr_"+recipient_reviewer.recipient_id})
                .append($('<td>', { class   : "text-center Table_Reviewers_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Reviewers_Td_Content px-8", 
                                    text    : recipient_reviewer.email}))
                .append($('<td>', { class   : "Table_Reviewers_Td_Content px-8", 
                                    text    : recipient_reviewer.recipient_status}))
                .append($('<td>', { class   : "Table_Reviewers_Td_Content px-8"})
                  // .append($('<button>', { type    : "button",
                  //                         onclick : "ReviewerApprovePaper('"+data.objective_id+"')",
                  //                         text    : " Delete ",
                  //                         class   : "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
                  //                         style   : "width:100px; margin-right: 0px"
                  //                         }))
                                          )
            );
          // if(recipient_reviewer.)
        }
        $('#Table_CCs .Table_CCs_Tr').remove();
        for(var i in data.recipients_cc){
          var recipient_cc = data.recipients_cc[i];
          $('#Table_CCs')
            .append($('<tr>', { class   : "Table_CCs_Tr"})
                .append($('<td>', { class   : "text-center Table_CCs_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_CCs_Td_Content px-8", 
                                    text    : recipient_cc.email}))
            );
        }
        document.getElementById("Paper_Subject").innerText = data.paper.paper_subject;
        document.getElementById("Table_RPTs_Td_Content12").innerText = data.rpt.rpt_related_pdb;
        document.getElementById("Table_RPTs_Td_Content22").innerText = data.rpt.rpt_market_rate;
        document.getElementById("Table_RPTs_Td_Content32").innerText = data.rpt.rpt_at_par;
        $('#Table_Objectives .Table_Objectives_Tr').remove();
        for(var i in data.objectives){
          var objective = data.objectives[i];
          $('#Table_Objectives')
            .append($('<tr>', { class   : "Table_Objectives_Tr"})
                .append($('<td>', { class   : "text-center Table_Objectives_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Objectives_Td_Content px-8", 
                                    text    : objective.objective}))
            );
        }
        document.getElementById("Paper_Content").innerText = data.paper.paper_content;
        $('#Table_Attachments .Table_Attachments_Tr').remove();
        for(var i in data.attachments){
          var attachment = data.attachments[i];
          $('#Table_Attachments')
            .append($('<tr>', { class   : "Table_Attachments_Tr"})
                .append($('<td>', { class   : "text-center Table_Attachments_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Attachments_Td_Content px-8", 
                                    text    : attachment.attachment_name,
                                    onclick : "AttachmentModalShow('" + attachment.attachment_name + "','" + data.paper.id + "')"}))
            );
        }
        $('#Table_Recommendations .Table_Recommendations_Tr').remove();
        for(var i in data.recommendations){
          var recommendation = data.recommendations[i];
          $('#Table_Recommendations')
            .append($('<tr>', { class   : "Table_Recommendations_Tr"})
                .append($('<td>', { class   : "text-center Table_Recommendations_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Recommendations_Td_Content px-8", 
                                    text    : recommendation.recommendation}))
            );
        }
        document.getElementById("Paper_Next_Steps").innerText = data.paper.paper_next_steps;
        // document.getElementById("ButtonEditDraft").setAttribute("href", "draft-approval-pg1/" + data.paper.id);
        // document.getElementById("ButtonDeleteDraft").setAttribute("href", "DeleteDraftPaper/" + data.paper.id);
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to add objective!");
      },
    });
  }
</script>
