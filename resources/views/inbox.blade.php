
<style>
  .bg-blue {
    --tw-bg-opacity: 1;
    background-color: blue;
  }
  </style>

@extends('layouts.app')

@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
{{-- <link rel="stylesheet" href="//cdn.materialdesignicons.com/3.7.95/css/materialdesignicons.min.css">
<link rel="stylesheet" href="https://rsms.me/inter/inter.css"> --}}

<!-- Content for dashboard page -->
<div class="grid grid-cols-4 h-full w-full">
  <div class="h-full border" style="background-color: #F5F5F5;">
    <!-- Create report button -->
    <div class="hidden md:block m-4 w-64 p-6 overflow-y-auto bg-gray-100">
      <a href="draft-approval-pg1">
          <button type="button"
            class=" inline-flex items-center justify-center px-1 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-black bg-opacity-75 hover:bg-gray-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-600 w-full">
            &plus; Create Paper
          </button>
      </a>
      <!-- left navigation  -->
      <nav>
        <div class="mt-3">
          <a href="{{url('/inbox')}}"
            class="-mx-3  py-1 px-3 text-sm font-medium flex items-center justify-center bg-gray-300 border-2 border-gray-400 border-opacity-100 rounded-lg ">
            <span>
              <i class="h-6 w-6 fa fa-envelope-o fill-current text-gray-700" aria-hidden="true"></i>
              <span class=" text-gray-900 ">Inbox</span>
            </span>
          </a>
        </div>
        <div class="mt-3">
          <a href="{{ url('/sent') }}"
            class="-mx-3  py-1 px-3 text-sm font-medium flex items-center justify-center hover:bg-gray-300 rounded-lg border-2">
            <span>
              <i class="h-6 w-6 fa fa-flag-o fill-current text-gray-700" aria-hidden="true"></i>
              <span class=" text-gray-900">Sent</span>
            </span>
          </a>
        </div>
        <div class="mt-3">
          <a href="{{ url('/drafts') }}"
            class="-mx-3  py-1 px-3 text-sm font-medium flex items-center justify-center hover:bg-gray-300 rounded-lg border-2">
            <span>
              <i class="h-6 w-6 fa fa-flag-o fill-current text-gray-700" aria-hidden="true"></i>
              <span class=" text-gray-900">Draft</span>
            </span>
          </a>
        </div>
        <div class="mt-3">
          <a href="{{ url('/mytracker') }}"
            class="-mx-3  py-1 px-3 text-sm font-medium flex items-center justify-center hover:bg-gray-300 rounded-lg border-2">
            <span>
              <i class="h-6 w-6 fa fa-flag-o fill-current text-gray-700" aria-hidden="true"></i>
              <span class=" text-gray-900 ">My Tracker</span>
            </span>
          </a>
        </div>
      </nav>
    </div>
  </div>

  <div class="col-span-3">
    <div class="grid grid-cols-2" style="background-color: #F5F5F5;">
      <!-- search bar  -->
      <form class="col-start-2 col-end-3 p-6 mt-6 ml-4 mr-4 flex space-x-4" action="search_inbox" method="post">
          @csrf
          <label for="search" class="sr-only">Search</label>
          <div class="relative rounded-md shadow-sm">
            <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
              <!-- Heroicon name: mail -->
              <svg class="h-5 w-5 text-gray-400" x-description="Heroicon name: solid/search"
                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd"
                  d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                  clip-rule="evenodd"></path>
              </svg>
            </div>
            <input type="text" name="search" id="search"
              class="focus:ring-blue-600 focus:border-blue-600 block pl-12 sm:text-sm border-gray-300 rounded-md"
              placeholder="">
          </div>
          <input type="submit" value="Search" class="inline-flex justify-center px-3.5 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-600">
        </form>
        
        <!-- inbox  -->
        <div class="col-span-2 px-6 ml-4 mr-4 pb-16" style="height:70%" id="style-1">
          <div>
            <button onclick="ShowPaper()" class="btn btn-primary">Paper</button>
            <button onclick="ShowRisk()" class="btn btn-primary">Risk</button>
            {{-- <button class="btn btn-primary">Paper Submission</button> --}}
          </div>
          <br>
          <div id="PaperDiv" style="display: block">
            @if ($pendings->count() > 0 || $risk_approveds->count() > 0)
              <h2 class="py-3">Need Action</h2>
              @foreach ($risk_approveds as $risk_approved)
                <a onclick="GetInboxPaper('{{$risk_approved->id}}', 'Risks Approved')" id="PaperDiv-{{$risk_approved->id}}" class="block bg-white py-3 border">
                  <div class="pt-5 px-4 py-2 flex  justify-between">
                    <span class="text-sm font-semibold text-gray-900">{{$risk_approved->user->name}}</span>
                  </div>
                  <span class="text-sm font-semibold text-gray-900 px-4 py-2">{{$risk_approved->paper_subject}}</span>
                </a>
              @endforeach
              @foreach ($pendings as $pending)
                <a onclick="GetInboxPaper('{{$pending->paper->id}}', 'Pending')" id="PaperDiv-{{$pending->paper->id}}" class="block bg-white py-3 border">
                  <div class="pt-5 px-4 py-2 flex  justify-between">
                    <span class="text-sm font-semibold text-gray-900">{{$pending->paper->user->name}}</span>
                  </div>
                  <span class="text-sm font-semibold text-gray-900 px-4 py-2">{{$pending->paper->paper_subject}}</span>
                </a>
              @endforeach
            @endif
            @if ($revieweds->count() > 0)
              <h2 class="py-3">Reviewed</h2>
              @foreach ($revieweds as $reviewed)
                <a onclick="GetInboxPaper('{{$reviewed->paper->id}}', 'Reviewed')" id="PaperDiv-{{$reviewed->paper->id}}" class="block bg-white py-3 border">
                  <div class="pt-5 px-4 py-2 flex  justify-between">
                    <span class="text-sm font-semibold text-gray-900">{{$reviewed->paper->user->name}}</span>
                  </div>
                  <span class="text-sm font-semibold text-gray-900 px-4 py-2">{{$reviewed->paper->paper_subject}}</span>
                </a>
              @endforeach
            @endif
            @if ($ccs->count() > 0)
              <h2 class="py-3">CC</h2>
              @foreach ($ccs as $cc)
                <a onclick="GetInboxPaper('{{$cc->paper->id}}', 'Reviewed')" id="PaperDiv-{{$cc->paper->id}}" class="block bg-white py-3 border">
                  <div class="pt-5 px-4 py-2 flex  justify-between">
                    <span class="text-sm font-semibold text-gray-900">{{$cc->paper->user->name}}</span>
                  </div>
                  <span class="text-sm font-semibold text-gray-900 px-4 py-2">{{$cc->paper->paper_subject}}</span>
                </a>
              @endforeach
            @endif
            @if ($approveds->count() > 0)
              <h2 class="py-3">Approved</h2>
              @foreach ($approveds as $approved)
                <a onclick="GetInboxPaper('{{$approved->id}}', 'Approved')" id="PaperDiv-{{$approved->id}}" class="block bg-white py-3 border">
                  <div class="pt-5 px-4 py-2 flex  justify-between">
                    <span class="text-sm font-semibold text-gray-900">{{$approved->user->name}}</span>
                  </div>
                  <span class="text-sm font-semibold text-gray-900 px-4 py-2">{{$approved->paper_subject}}</span>
                </a>
              @endforeach
            @endif
          </div>
          <div id="RiskDiv" style="display: none">
            @if ($RADMs->count() > 0)
              <h2 class="py-3">Need Action</h2>
              @foreach ($RADMs as $RADM)
                <a onclick="GetRiskDetails('{{$RADM->paper->id}}', 'Pending')" id="RiskDiv-{{$RADM->paper->id}}" class="block bg-white py-3 border">
                  <div class="pt-5 px-4 py-2 flex  justify-between">
                    <span class="text-sm font-semibold text-gray-900">{{$RADM->paper->user->name}}</span>
                  </div>
                  <span class="text-sm font-semibold text-gray-900 px-4 py-2">{{$RADM->paper->paper_subject}}</span>
                </a>
              @endforeach
            @endif
            @if ($RADMs_reviewed->count() > 0)
              <h2 class="py-3">Reviewed</h2>
              @foreach ($RADMs_reviewed as $RADM_reviewed)
                <a onclick="GetRiskDetails('{{$RADM_reviewed->paper->id}}', 'Reviewed')" id="RiskDiv-{{$RADM_reviewed->paper->id}}" class="block bg-white RiskDiveClass py-3 border">
                  <div class="pt-5 px-4 py-2 flex  justify-between">
                    <span class="text-sm font-semibold text-gray-900">{{$RADM_reviewed->paper->user->name}}</span>
                  </div>
                  <span class="text-sm font-semibold text-gray-900 px-4 py-2">{{$RADM_reviewed->paper->paper_subject}}</span>
                </a>
              @endforeach
            @endif
          </div>
        </div>
    </div>

    <!-- report content  -->
    <div class="bg-gray-100 flex-grow">
      <div id="InboxDetailDiv" class="h-screen bg-gray-200 " style="background-color: ; display:none">
        <div style="padding-left: 45px; padding-right: 40px;">
          <h1 id="Paper_Subject" class="demoFont"></h1>
          <h4 style="padding-bottom: 10px;">Approvers: </h4>
          <table class="min-w-full divide-y" id="Table_Approvers">
            <thead style="background:; border-radius: 25px;">
              <tr style="">
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                  No
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Approvers
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Status
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              <tr id="Table_Approvers_Tr" class="Table_Approvers_Tr">
                <td id="Table_Approvers_Td_No" class="text-center Table_Approvers_Td_No">
                </td>
                <td id="Table_Approvers_Td_Content">
                </td>
              </tr>
            </tbody>
          </table>
          <br>
          <h4 style="padding-bottom: 10px;">Reviewers: </h4>
          <table class="min-w-full divide-y" id="Table_Reviewers">
            <thead style="background:; border-radius: 25px;">
              <tr style="">
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                  No
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Reviewers
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Status
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              <tr id="Table_Reviewers_Tr" class="Table_Reviewers_Tr">
                <td id="Table_Reviewers_Td_No" class="text-center Table_Reviewers_Td_No">
                </td>
                <td id="Table_Reviewers_Td_Content">
                </td>
              </tr>
            </tbody>
          </table>
          <br>
          <h4 style="padding-bottom: 10px;">CC: </h4>
          <table class="min-w-full divide-y" id="Table_CCs">
            <thead style="background:; border-radius: 25px;">
              <tr style="">
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                  No
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  CCs
                </th>
              </tr>
            </thead>
            <tbody>
              <tr id="Table_CCs_Tr" class="Table_CCs_Tr">
                <td id="Table_CCs_Td_No" class="text-center Table_CCs_Td_No">
                </td>
                <td id="Table_CCs_Td_Content">
                </td>
              </tr>
            </tbody>
          </table>
          <br>
          <h4 style="padding-bottom: 10px;">Related Party & Transfer Pricing (TP) : </h4>
          <table class="min-w-full divide-y" id="Table_RPTs">
            {{-- <thead style="background:; border-radius: 25px;">
              <tr style="">
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                  No
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  RPTs
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                </th>
              </tr>
            </thead> --}}
            <tbody>
              <tr id="Table_RPTs_Tr1" class="Table_RPTs_Tr">
                <td id="Table_RPTs_Td_No1" class="text-center Table_RPTs_Td_No">
                  1
                </td>
                <td id="Table_RPTs_Td_Content11" class="px-8">
                  Is the transacting party related to PDB?	
                </td>
                <td id="Table_RPTs_Td_Content12">
                </td>
              </tr>
              <tr id="Table_RPTs_Tr2" class="Table_RPTs_Tr">
                <td id="Table_RPTs_Td_No2" class="text-center Table_RPTs_Td_No">
                  2
                </td>
                <td id="Table_RPTs_Td_Content21" class="px-8">
                  Is the agreed price within prevailing market rate and on normal commersial terms?	
                </td>
                <td id="Table_RPTs_Td_Content22">
                </td>
              </tr>
              <tr id="Table_RPTs_Tr3" class="Table_RPTs_Tr">
                <td id="Table_RPTs_Td_No3" class="text-center Table_RPTs_Td_No">
                  3
                </td>
                <td id="Table_RPTs_Td_Content31" class="px-8">
                  Are the material terms at par with the non-related party?	
                </td>
                <td id="Table_RPTs_Td_Content32">
                </td>
              </tr>
            </tbody>
          </table>
          <br>
          <h4 style="padding-bottom: 10px;">Objectives : </h4>
          <table class="min-w-full divide-y" id="Table_Objectives">
            <thead style="background:; border-radius: 25px;">
              <tr style="">
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                  No
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Objectives
                </th>
              </tr>
            </thead>
            <tbody>
              <tr id="Table_Objectives_Tr" class="Table_Objectives_Tr">
                <td id="Table_Objectives_Td_No" class="text-center Table_Objectives_Td_No">
                </td>
                <td id="Table_Objectives_Td_Content">
                </td>
              </tr>
            </tbody>
          </table>
          <br>
          <h4 style="padding-bottom: 10px;">Body : </h4>
          <textarea readonly id="Paper_Content" maxlength="10000"  name="paper_content" rows="10" placeholder="Paper Content Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max"></textarea>
          <br>
          <h4 style="padding-bottom: 10px;">Attachments : </h4>
          <table class="min-w-full divide-y" id="Table_Attachments">
            <thead style="background:; border-radius: 25px;">
              <tr style="">
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                  No
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Attachments
                </th>
              </tr>
            </thead>
            <tbody>
              <tr id="Table_Attachments_Tr" class="Table_Attachments_Tr">
                <td id="Table_Attachments_Td_No" class="text-center Table_Attachments_Td_No">
                </td>
                <td id="Table_Attachments_Td_Content">
                </td>
              </tr>
            </tbody>
          </table>
          <br>
          <h4 style="padding-bottom: 10px;">Recommendations : </h4>
          <table class="min-w-full divide-y" id="Table_Recommendations">
            <thead style="background:; border-radius: 25px;">
              <tr style="">
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                  No
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Recommendations
                </th>
              </tr>
            </thead>
            <tbody>
              <tr id="Table_Recommendations_Tr" class="Table_Recommendations_Tr">
                <td id="Table_Recommendations_Td_No" class="text-center Table_Recommendations_Td_No">
                </td>
                <td id="Table_Recommendations_Td_Content">
                </td>
              </tr>
            </tbody>
          </table>
          <br>
          <h4 style="padding-bottom: 10px;">Next Steps : </h4>
          <textarea readonly id="Paper_Next_Steps" maxlength="10000"  name="Paper_Next_Steps" rows="10" placeholder="Paper Next Steps" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max"></textarea>
          <br>
          <div id="DivButtonRespond" class="flex justify-end pt-5 pb-5">
          </div>
          
        </div>
      </div>
      <div id="InboxDivRespond" class="h-screen bg-gray-200 " style="background-color: ; display:none">
        <div style="padding-left: 45px; padding-right: 40px;">
          <h1 id="Respond_Paper_Subject" class="demoFont"></h1>
          <h4 id="Respond_Paper_User" style="padding-bottom: 10px;"></h4>
          <br>
          <textarea id="paper_respond_comment" maxlength="10000"  name="paper_respond_comment" rows="10" placeholder="Leave Comments..." class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max"></textarea>
          <br>
          <h4 style="padding-bottom: 10px;">Attachments : </h4>
          <table class="min-w-full divide-y" id="Table_Attachments">
            <thead style="background:; border-radius: 25px;">
              <tr style="">
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                  No
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Attachments
                </th>
              </tr>
            </thead>
            <tbody>
              <tr id="Table_Attachments_Tr" class="Table_Attachments_Tr">
                <td id="Table_Attachments_Td_No" class="text-center Table_Attachments_Td_No">
                </td>
                <td id="Table_Attachments_Td_Content">
                </td>
              </tr>
            </tbody>
          </table>
          <br>
          <div class="flex justify-end pt-5 pb-5">
            <button id="ButtonReviewed" class="btn btn-primary">Reviewed</button>&nbsp; &nbsp;
            <button id="ButtonReturnForAmendment" onclick="ButtonReturnFromAmendmentClicked()" class="btn btn-primary">Return For Amendment</button>
          </div>
          {{-- <div id="AttachmentModal1" class="modal" style="border: none; padding-top:10%; background-color:rgba(56, 56, 56, 0.08)">
            <div class="row justify-content-center" id="AttachmentModal2" >
              <div class="col-md-8 row justify-content-center" id="AttachmentModal3" style="border: none; text-align:center" >
                <div class="card" id="editCard2" style="border: none; width: 100%">
                  <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('Attachment') }}</b>
                  </div>
                  <div class="card-body justify-content-center">
                    <p><span id="AttachmentSpanName"></span></p>
                    <div class="flex justify-center pb-3 border-2 border-gray-300 border-dashed rounded-md">
                      <div class="space-y-1 text-center" style="width:100%">
                        <center><span id="AttachmentSpan"></span></center>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> --}}
        </div>
      </div>
      <div id="RiskDetailDiv" class="h-screen bg-gray-200 " style="background-color: ; display:none">
        <div style="padding-left: 45px; padding-right: 40px;">
          <h1 id="Risk_Paper_Subject" class="demoFont"></h1>
          <h4 id="Risk_Paper_User" style="padding-bottom: 10px;"></h4>
          <table class="min-w-full divide-y" id="Table_Risk_Endorser">
            <thead style="background:; border-radius: 25px;">
              <tr style="">
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                  No
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Edorsers
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Status
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              <tr id="Table_Risk_Endorser_Tr" class="Table_Risk_Endorser_Tr">
                <td id="Table_Risk_Endorser_Td_No" class="text-center Table_Risk_Endorser_Td_No">
                </td>
                <td id="Table_Risk_Endorser_Td_Content">
                </td>
              </tr>
            </tbody>
          </table>
          <br>
          <table class="min-w-full divide-y" id="Table_Risk_Reviewer">
            <thead style="background:; border-radius: 25px;">
              <tr style="">
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                  No
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Reviewers
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Status
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              <tr id="Table_Risk_Reviewer_Tr" class="Table_Risk_Reviewer_Tr">
                <td id="Table_Risk_Reviewer_Td_No" class="text-center Table_Risk_Reviewer_Td_No">
                </td>
                <td id="Table_Risk_Reviewer_Td_Content">
                </td>
              </tr>
            </tbody>
          </table>
          <br>
          <h4 style="padding-bottom: 10px;">Risks And Mitigations : </h4>
          <table class="min-w-full divide-y" id="Table_Risk_Mitigation">
            <thead style="background:; border-radius: 25px;">
              <tr style="">
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                  No
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Risk
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Mitigations
                </th>
              </tr>
            </thead>
            <tbody>
              <tr id="Table_Risk_Mitigation_Tr" class="Table_Risk_Mitigation_Tr">
                <td id="Table_Risk_Mitigation_Td_No" class="text-center Table_Risk_Mitigation_Td_No">
                </td>
                <td id="Table_Risk_Mitigation_Td_Content">
                </td>
              </tr>
            </tbody>
          </table>
          <br>
          <table class="min-w-full divide-y" id="Table_Risk_Attachments">
            <thead style="background:; border-radius: 25px;">
              <tr style="">
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                  No
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Attachments
                </th>
              </tr>
            </thead>
            <tbody>
              <tr id="Table_Risk_Attachments_Tr" class="Table_Attachments_Tr">
                <td id="Table_Risk_Attachments_Td_No" class="text-centerTable_Risk_Attachments_Td_No">
                </td>
                <td id="Table_Risk_Attachments_Td_Content">
                </td>
              </tr>
            </tbody>
          </table>
          <br>
          {{-- <div class="flex justify-end pt-5 pb-5">
            <button id="ButtonRiskRespond" class="btn btn-primary">Respond</button>&nbsp; &nbsp;
          </div> --}}
          <div id="DivRiskButtonRespond" class="flex justify-end pt-5 pb-5">
          </div>

          {{-- Attachment Modal --}}
          {{-- <div id="AttachmentModal1" class="modal" style="border: none; padding-top:10%; background-color:rgba(56, 56, 56, 0.08)">
            <div class="row justify-content-center" id="AttachmentModal2" >
              <div class="col-md-8 row justify-content-center" id="AttachmentModal3" style="border: none; text-align:center" >
                <div class="card" id="editCard2" style="border: none; width: 100%">
                  <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('Attachment') }}</b>
                  </div>
                  <div class="card-body justify-content-center">
                    <p><span id="AttachmentSpanName"></span></p>
                    <div class="flex justify-center pb-3 border-2 border-gray-300 border-dashed rounded-md">
                      <div class="space-y-1 text-center" style="width:100%">
                        <center><span id="AttachmentSpan"></span></center>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> --}}
          {{-- Attachment Modal --}}

        </div>
      </div>
      <div id="DivRiskRespond" class="h-screen bg-gray-200 " style="background-color: ; display:none">
        <div style="padding-left: 45px; padding-right: 40px;">
          <h1 id="Respond_Paper_Subject" class="demoFont"></h1>
          <h4 id="Respond_Paper_User" style="padding-bottom: 10px;"></h4>
          <br>
          <textarea id="risk_respond_comment" maxlength="10000"  name="risk_respond_comment" rows="10" placeholder="Leave Comments..." class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max"></textarea>
          <br>
          <h4 style="padding-bottom: 10px;">Attachments : </h4>
          <table class="min-w-full divide-y" id="Table_Risk_Respond_Attachment">
            <thead style="background:; border-radius: 25px;">
              <tr style="">
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                  No
                </th>
                <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Attachments
                </th>
              </tr>
            </thead>
            <tbody>
              <tr id="Table_Risk_Respond_Attachment_Tr" class="Table_Risk_Respond_Attachment_Tr">
                <td id="Table_Risk_Respond_Attachment_Td_No" class="text-center Table_Risk_Respond_Attachment_Td_No">
                </td>
                <td id="Table_Risk_Respond_Attachment_Td_Content">
                </td>
              </tr>
            </tbody>
          </table>
          <div class="width-full flex justify-end">
            <button type="button" onclick="AddAttachmentOnClick(null, null)" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
              Add Attachment
            </button>
          </div>
          <br>
          <div class="flex justify-end pt-5 pb-5">
            <button id="ButtonRiskReviewed" class="btn btn-primary">Reviewed</button>&nbsp; &nbsp;
            {{-- <button id="ButtonReturnForAmendment" onclick="ButtonReturnFromAmendmentClicked()" class="btn btn-primary">Return For Amendment</button> --}}
          </div>
          {{-- <div id="AttachmentModal1" class="modal" style="border: none; padding-top:10%; background-color:rgba(56, 56, 56, 0.08)">
            <div class="row justify-content-center" id="AttachmentModal2" >
              <div class="col-md-8 row justify-content-center" id="AttachmentModal3" style="border: none; text-align:center" >
                <div class="card" id="editCard2" style="border: none; width: 100%">
                  <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('Attachment') }}</b>
                  </div>
                  <div class="card-body justify-content-center">
                    <p><span id="AttachmentSpanName"></span></p>
                    <div class="flex justify-center pb-3 border-2 border-gray-300 border-dashed rounded-md">
                      <div class="space-y-1 text-center" style="width:100%">
                        <center><span id="AttachmentSpan"></span></center>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> --}}
        </div>
      </div>
    </div>
    <div id="AttachmentModal1" class="modal" style="border: none; padding-top:10%; background-color:rgba(56, 56, 56, 0.08)">
      <div class="row justify-content-center" id="AttachmentModal2" >
        <div class="col-md-8 row justify-content-center" id="AttachmentModal3" style="border: none; text-align:center" >
          <div class="card" id="editCard2" style="border: none; width: 100%">
            <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('Attachment') }}</b>
            </div>
            <div class="card-body justify-content-center">
              <p><span id="AttachmentSpanName"></span></p>
              <div class="flex justify-center pb-3 border-2 border-gray-300 border-dashed rounded-md">
                <div class="space-y-1 text-center" style="width:100%">
                  <center><span id="AttachmentSpan"></span></center>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="RiskAttachmentModal1" class="modal" style="border: none; padding-top:10%; background-color:rgba(56, 56, 56, 0.08)">
      <div class="row justify-content-center" id="RiskAttachmentModal2" >
        <div class="col-md-8 row justify-content-center" id="RiskAttachmentModal3" style="border: none; text-align:center" >
          <div class="card" id="editCard2" style="border: none; width: 100%">
            <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('Attachment') }}</b>
            </div>
            <div class="card-body justify-content-center">
              <p><span id="RiskAttachmentSpanName"></span></p>
              <div class="flex justify-center pb-3 border-2 border-gray-300 border-dashed rounded-md">
                <div class="space-y-1 text-center" style="width:100%">
                  <center><span id="RiskAttachmentSpan"></span></center>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="AddAttachmentModal1" class="modal" style="border: none; padding-top:20%; background-color:rgba(56, 56, 56, 0.08)">
      <div class="row justify-content-center" id="AddAttachmentModal2">
        <div class="col-md-8 row justify-content-center" id="AddAttachmentModal3" style="border: none; text-align:center">
          <div class="card" id="editCard2" style="border: none; width: 50%; height : 100%">
            <div class="card-header" style="background-color:#00a57c; color:white; width:100%;"><b>{{ __('New Attachment') }}</b>
            </div>
            <form method="POST" enctype="multipart/form-data" id="laravel-ajax-file-upload" action="javascript:void(0)">
              @csrf
              <input type="hidden" name="paper_id" id="attachment_paper_id" value="">
              <input type="hidden" name="attachment_id" id="attachment_id" value="">
              <div class="card-body justify-content-center">
                <p>Attachment</p>
                <div class="flex justify-center pb-3 border-2 border-gray-300 border-dashed rounded-md">
                  <div class="space-y-1 text-center">
                    <svg class="mx-auto h-5 w-12 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                      <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>
                    <div class="flex text-sm text-gray-600">
                      <input required id="attachment" name="attachment" type="file" multiple="false">
                    </div>
                  </div>
                </div>
                {{-- <textarea style="width: 100%;" id="attachment" name="attachment" oninput="attachment_oninput()" rows="10" cols="100" maxlength="1000" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"></textarea>
                <span id="attachment_count" class="pl-3">0</span>/1000 characters --}}
                <br>
                <br>
                <div class="justify-content-center" style="text-align: center;">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  {{-- <button onclick="SaveAttachment()" id="PaperAddAttachmentSubmitButton" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    {{ __('Add to Attachment list') }}
                  </button> --}}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

@endsection

<script type="text/javascript">
  var prev_paper_id = null;
  // var glob_paper_id = null;

  function GetInboxPaper(paper_id, recipient_status){
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/GetSentPaper")}}',
      dataType: 'JSON',
      async:true, //ensure process finish then exit the function
      data: {
        paper_id : paper_id
      },
      success: function (data) {

        if(prev_paper_id == null){
          prev_paper_id = paper_id;
        }else{
          document.getElementById("PaperDiv-"+prev_paper_id).classList.add("bg-white");
          document.getElementById("PaperDiv-"+prev_paper_id).classList.remove("bg-gray-300"); 
          prev_paper_id = paper_id;
        }

        document.getElementById("PaperDiv-"+paper_id).classList.remove("bg-white");
        document.getElementById("PaperDiv-"+paper_id).classList.add("bg-gray-300"); 
        
        document.getElementById("InboxDetailDiv").style.display = "block";
        document.getElementById("InboxDivRespond").style.display = "none";
        document.getElementById("RiskDetailDiv").style.display = "none";
        document.getElementById("DivRiskRespond").style.display = "none";

        $('#Table_Approvers .Table_Approvers_Tr').remove();
        for(var i in data.recipients_approver){
          var recipient_approver = data.recipients_approver[i];
          $('#Table_Approvers')
            .append($('<tr>', { class   : "Table_Approvers_Tr"})
                .append($('<td>', { class   : "text-center Table_Approvers_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Approvers_Td_Content px-8", 
                                    text    : recipient_approver.email}))
                .append($('<td>', { class   : "Table_Approvers_Td_Content px-8", 
                                    text    : recipient_approver.recipient_status}))
            );
        }
        $('#Table_Reviewers .Table_Reviewers_Tr').remove();
        for(var i in data.recipients_reviewer){
          var recipient_reviewer = data.recipients_reviewer[i];
          $('#Table_Reviewers')
            .append($('<tr>', { class   : "Table_Reviewers_Tr",
                                id      : "Table_Reviewers_Tr_"+recipient_reviewer.recipient_id})
                .append($('<td>', { class   : "text-center Table_Reviewers_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Reviewers_Td_Content px-8", 
                                    text    : recipient_reviewer.email}))
                .append($('<td>', { class   : "Table_Reviewers_Td_Content px-8", 
                                    text    : recipient_reviewer.recipient_status}))
                .append($('<td>', { class   : "Table_Reviewers_Td_Content px-8"})
                                          )
            );
        }
        $('#Table_CCs .Table_CCs_Tr').remove();
        for(var i in data.recipients_cc){
          var recipient_cc = data.recipients_cc[i];
          $('#Table_CCs')
            .append($('<tr>', { class   : "Table_CCs_Tr"})
                .append($('<td>', { class   : "text-center Table_CCs_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_CCs_Td_Content px-8", 
                                    text    : recipient_cc.email}))
            );
        }
        document.getElementById("Paper_Subject").innerText = data.paper.paper_subject;
        document.getElementById("Table_RPTs_Td_Content12").innerText = data.rpt.rpt_related_pdb;
        document.getElementById("Table_RPTs_Td_Content22").innerText = data.rpt.rpt_market_rate;
        document.getElementById("Table_RPTs_Td_Content32").innerText = data.rpt.rpt_at_par;
        $('#Table_Objectives .Table_Objectives_Tr').remove();
        for(var i in data.objectives){
          var objective = data.objectives[i];
          $('#Table_Objectives')
            .append($('<tr>', { class   : "Table_Objectives_Tr"})
                .append($('<td>', { class   : "text-center Table_Objectives_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Objectives_Td_Content px-8", 
                                    text    : objective.objective}))
            );
        }
        document.getElementById("Paper_Content").innerText = data.paper.paper_content;
        $('#Table_Attachments .Table_Attachments_Tr').remove();
        for(var i in data.attachments){
          var attachment = data.attachments[i];
          $('#Table_Attachments')
            .append($('<tr>', { class   : "Table_Attachments_Tr"})
                .append($('<td>', { class   : "text-center Table_Attachments_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Attachments_Td_Content px-8", 
                                    text    : attachment.attachment_name,
                                    onclick : "AttachmentModalShow('" + attachment.attachment_name + "','" + data.paper.id + "')"}))
            );
        }
        $('#Table_Recommendations .Table_Recommendations_Tr').remove();
        for(var i in data.recommendations){
          var recommendation = data.recommendations[i];
          $('#Table_Recommendations')
            .append($('<tr>', { class   : "Table_Recommendations_Tr"})
                .append($('<td>', { class   : "text-center Table_Recommendations_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Recommendations_Td_Content px-8", 
                                    text    : recommendation.recommendation}))
            );
        }
        document.getElementById("Paper_Next_Steps").innerText = data.paper.paper_next_steps;
        // $('#DivButtonRespond').getElementByTagName('button').remove();

        if(document.getElementById('ButtonRespond') != null){
          document.getElementById('ButtonRespond').remove();
        }
        if(recipient_status == "Pending"){
          $('#DivButtonRespond')
            .append($('<button>',{  id      : "ButtonRespond",
                                    class   : "btn btn-primary",
                                    text    : "Respond",
                                    onclick : "RespondButtonOnclick(" + data.paper.id + ")"})
            );
        }else if(recipient_status == "Risks Approved"){
          $('#DivButtonRespond')
            .append($('<button>',{  id      : "ButtonRespond",
                                    class   : "btn btn-primary",
                                    text    : "Proceed",
                                    onclick : "window.location='{{url('/submission-pg1/')}}/" + data.paper.id + "'" })
                                    // onclick : "window.location='{{url('/PaperSubmission/" + data.paper.id + "')}}'"})
                                    
                                    // onclick="window.location='{{url('/risk-assessment-pg2')}}'"
                                    // onclick : "{{url('/PaperSubmission/" + data.paper.id + "')}}"})
                                    // onclick : "PaperSubmissionButonOnclick(" + data.paper.id + ")"})
            );
        }
        // document.getElementById("ButtonEditDraft").setAttribute("href", "draft-approval-pg1/" + data.paper.id);
        // document.getElementById("ButtonDeleteDraft").setAttribute("href", "DeleteDraftPaper/" + data.paper.id);
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to add objective!");
      },
    });
  }

  function RespondButtonOnclick(paper_id){
    document.getElementById("InboxDetailDiv").style.display = "none";
    document.getElementById("InboxDivRespond").style.display = "block";
    document.getElementById("RiskDetailDiv").style.display = "none";
    document.getElementById("DivRiskRespond").style.display = "none";

    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/GetSentPaper")}}',
      dataType: 'JSON',
      async:true, //ensure process finish then exit the function
      data: {
        paper_id : paper_id
      },
      success: function (data) {

        document.getElementById("MainDiv").scrollTop = 0;
        document.getElementById("Respond_Paper_Subject").innerText = data.paper.paper_subject;
        document.getElementById("Respond_Paper_User").innerText = data.user.name;
        // document.getElementById("ButtonReviewed").onclick = ;
        document.getElementById("ButtonReviewed").setAttribute('onclick', "ButtonReviewedClicked('" + data.paper.id + "')");

        // document.getElementById("PaperRespondComment");
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to add objective!");
      },
    });
  }

  function ButtonReviewedClicked(paper_id){
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/PaperReviewed")}}',
      dataType: 'JSON',
      async:true, //ensure process finish then exit the function
      data: {
        paper_id : paper_id
      },
      success: function (data) {
        alert("Successfuly reviewed the paper!")
        location.reload();
        // document.getElementById("MainDiv").scrollTop = 0;
        // document.getElementById("Respond_Paper_Subject").innerText = data.paper.paper_subject;
        // document.getElementById("Respond_Paper_User").innerText = data.user.name;

        // document.getElementById("PaperRespondComment");
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to add objective!");
      },
    });
  }

  function ShowPaper(){
    document.getElementById('RiskDiv').style.display = "none";
    document.getElementById('PaperDiv').style.display = "block";
    document.getElementById("DivRiskRespond").style.display = "none";
    document.getElementById("InboxDetailDiv").style.display = "none";
    document.getElementById("RiskDetailDiv").style.display = "none";

    if(prev_paper_id != null){
      document.getElementById("PaperDiv-"+prev_paper_id).classList.add("bg-white");
      document.getElementById("PaperDiv-"+prev_paper_id).classList.remove("bg-gray-300"); 
    }

    if(prev_risk_paper_id != null){
      document.getElementById("RiskDiv-"+prev_risk_paper_id).classList.add("bg-white");
      document.getElementById("RiskDiv-"+prev_risk_paper_id).classList.remove("bg-gray-300"); 
    }
  }

  function ShowRisk(){
    document.getElementById('RiskDiv').style.display = "block";
    document.getElementById('PaperDiv').style.display = "none";
    document.getElementById("DivRiskRespond").style.display = "none";
    document.getElementById("InboxDetailDiv").style.display = "none";
    document.getElementById("RiskDetailDiv").style.display = "none";

  }

  var prev_risk_paper_id = null;

  function GetRiskDetails(paper_id, risk_recipient_status){
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/GetRiskDetails")}}',
      dataType: 'JSON',
      async:true, //ensure process finish then exit the function
      data: {
        paper_id : paper_id
      },
      success: function (data) {

        if(prev_risk_paper_id == null){
          prev_risk_paper_id = paper_id;
        }else{
          document.getElementById("RiskDiv-"+prev_risk_paper_id).classList.add("bg-white");
          document.getElementById("RiskDiv-"+prev_risk_paper_id).classList.remove("bg-gray-300"); 
          prev_risk_paper_id = paper_id;
        }

        document.getElementById("RiskDiv-"+paper_id).classList.remove("bg-white");
        document.getElementById("RiskDiv-"+paper_id).classList.add("bg-gray-300"); 
        
        document.getElementById("RiskDetailDiv").style.display = "block";
        document.getElementById("DivRiskRespond").style.display = "none";
        document.getElementById("InboxDetailDiv").style.display = "none";
        document.getElementById("InboxDivRespond").style.display = "none";

        $('#Table_Risk_Endorser .Table_Risk_Endorser_Tr').remove();
        for(var i in data.risk_endorsers){
          var risk_endorser = data.risk_endorsers[i];
          $('#Table_Risk_Endorser')
            .append($('<tr>', { class   : "Table_Risk_Endorser_Tr"})
                .append($('<td>', { class   : "text-center Table_Risk_Endorser_Tr",
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Risk_Endorser_Td_Content px-8", 
                                    text    : risk_endorser.email}))
                .append($('<td>', { class   : "Table_Risk_Endorser_Td_Content px-8", 
                                    text    : risk_endorser.riskrecipient_status}))
            );
        }
        // Table_Risk_Endorser
        $('#Table_Risk_Reviewer .Table_Risk_Reviewer_Tr').remove();
        for(var i in data.risk_reviewers){
          var risk_reviewer = data.risk_reviewers[i];
          $('#Table_Risk_Reviewer')
            .append($('<tr>', { class   : "Table_Risk_Reviewer_Tr"})
                .append($('<td>', { class   : "text-center Table_Risk_Reviewer_Tr",
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Risk_Reviewer_Td_Content px-8", 
                                    text    : risk_reviewer.email}))
                .append($('<td>', { class   : "Table_Risk_Reviewer_Td_Content px-8", 
                                    text    : risk_reviewer.riskrecipient_status}))
            );
        }
        $('#Table_Risk_Mitigation .Table_Risk_Mitigation_Tr').remove();
        for(var i in data.risks){
          var risk = data.risks[i];
          $('#Table_Risk_Mitigation')
            .append($('<tr>', { class   : "Table_Risk_Mitigation_Tr"})
                .append($('<td>', { class   : "text-center Table_Risk_Mitigation_Tr",
                                    rowspan : risk.mitigations.length+1,
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Risk_Mitigation_Content px-8", 
                                    rowspan : risk.mitigations.length+1,
                                    text    : risk.risk_title}))
            );
          for(var i in risk.mitigations){
            var mitigation = risk.mitigations[i];
            $('#Table_Risk_Mitigation')
            .append($('<tr>', { class   : "Table_Risk_Mitigation_Tr"})
                .append($('<td>', { class   : "Table_Risk_Mitigation_Content px-8", 
                                    text    : mitigation.mitigation_detail}))
            );
          }
        }
        $('#Table_Risk_Attachments .Table_Risk_Attachments_Tr').remove();
        for(var i in data.attachments){
          var attachment = data.attachments[i];
          $('#Table_Risk_Attachments')
            .append($('<tr>', { class   : "Table_Risk_Attachments_Tr"})
                .append($('<td>', { class   : "text-center Table_Attachments_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Risk_Attachments_Tr_Td_Content px-8", 
                                    text    : attachment.attachment_name,
                                    onclick : "AttachmentModalShow('" + attachment.attachment_name + "','" + data.paper.id + "')"}))
            );
        }
        if(document.getElementById('ButtonRiskRespond') != null){
          document.getElementById('ButtonRiskRespond').remove();
        }
        if(risk_recipient_status == "Pending"){
          $('#DivRiskButtonRespond')
            .append($('<button>',{  id      : "ButtonRiskRespond",
                                    class   : "btn btn-primary",
                                    text    : "Respond",
                                    onclick : "RiskRespondButtonOnclick(" + paper_id + ")"})
            );
        }

        document.getElementById("attachment_paper_id").value = paper_id;
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to fetch risk details!");
      },
    });
  }

  function RiskRespondButtonOnclick(paper_id){
    document.getElementById("RiskDetailDiv").style.display = "none";
    document.getElementById("DivRiskRespond").style.display = "block";
    document.getElementById("InboxDetailDiv").style.display = "none";
    document.getElementById("InboxDivRespond").style.display = "none";

    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/GetRiskDetails")}}',
      dataType: 'JSON',
      async:true, //ensure process finish then exit the function
      data: {
        paper_id : paper_id
      },
      success: function (data) {
        document.getElementById("MainDiv").scrollTop = 0;
        // document.getElementById("Respond_Paper_Subject").innerText = data.paper.paper_subject;
        // document.getElementById("Respond_Paper_User").innerText = data.user.name;
        document.getElementById("ButtonRiskReviewed").setAttribute('onclick', "ButtonRiskReviewedClicked('" + paper_id + "')");
        $('#Table_Risk_Respond_Attachment .Table_Risk_Respond_Attachment_Tr').remove();
        for(var i in data.riskattachments){
          var riskattachment = data.riskattachments[i];
          $('#Table_Risk_Respond_Attachment')
            .append($('<tr>', { class   : "Table_Risk_Respond_Attachment_Tr",
                                id      : "risk-attachment-id-" + riskattachment.id,})
                .append($('<td>', { class   : "text-center Table_Attachment_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Risk_Respond_Attachment_Tr_Td_Content px-8", 
                                    text    : riskattachment.riskattachment_name,
                                    onclick : "RiskAttachmentModalShow('" + riskattachment.riskattachment_name + "','" + paper_id + "')"}))
                .append($('<td>', {
                    class: "justify-center align-center text-center"
                  })
                  .append($('<button>', {
                    type: "button",
                    onclick: "DeleteRiskRecipientAttachmentOnClick('" + riskattachment.id + "')",
                    text: " Delete ",
                    class: "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
                    style: "width:100px; margin-right: 0px"
                  })))
            );
        }
        
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to add objective!");
      },
    });
  }

  function ButtonRiskReviewedClicked(paper_id){
    var risk_respond_comment = document.getElementById("risk_respond_comment").value;
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/RiskReviewed")}}',
      dataType: 'JSON',
      async:true, //ensure process finish then exit the function
      data: {
        paper_id : paper_id,
        riskrecipient_comment : risk_respond_comment
      },
      success: function (data) {
        alert("Successfuly reviewed the paper!")
        location.reload();
        // document.getElementById("MainDiv").scrollTop = 0;
        // document.getElementById("Respond_Paper_Subject").innerText = data.paper.paper_subject;
        // document.getElementById("Respond_Paper_User").innerText = data.user.name;

        // document.getElementById("PaperRespondComment");
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to add objective!");
      },
    });
  }

  function AttachmentModalShow(attachment_name, paper_id) {
    var AttachmentModal = document.getElementById("AttachmentModal1");
    var AttachmentModal2 = document.getElementById("AttachmentModal2");
    var AttachmentModal3 = document.getElementById("AttachmentModal3");
    var fileExt = attachment_name.substring(attachment_name.lastIndexOf('.') + 1).toLowerCase();
    if(fileExt == "png" || fileExt == "jpg" || fileExt == "jpeg"){
        document.getElementById("AttachmentSpanName").innerHTML = attachment_name;
        document.getElementById("AttachmentSpan").innerHTML = "<img src='attachments/" + paper_id + "_" + attachment_name + "' style='height:500; align:center'>";
    }else{
        document.getElementById("AttachmentSpanName").innerHTML = attachment_name;
        document.getElementById("AttachmentSpan").innerHTML = "<iframe src='attachments/" + paper_id + "_" + attachment_name + "' style='height:500; width:100%; overflow-x:auto' frameborder='0'></iframe>";
    }
    // document.getElementById('attachment_name').value = attachment_name;
    AttachmentModal.style.display = "block";
    AttachmentModal.style.overflowY = "";
    const body = document.body;
    body.style.overflowY = 'hidden';
  };

  document.addEventListener("click", function(e)
  {
      var AttachmentModal = document.getElementById("AttachmentModal1");
      var AttachmentModal2 = document.getElementById("AttachmentModal2");
      var AttachmentModal3 = document.getElementById("AttachmentModal3");

      if ((e.target==AttachmentModal || e.target==AttachmentModal2 || e.target==AttachmentModal3))
      {
        AttachmentModalCloseFunction();
      }
  });

  function AttachmentModalCloseFunction(){
    var AttachmentModal = document.getElementById("AttachmentModal1");
    var AttachmentModal2 = document.getElementById("AttachmentModal2");
    var AttachmentModal3 = document.getElementById("AttachmentModal3");

    const body = document.body;
    body.style.overflowY = '';
    AttachmentModal.style.display = "none";
  }
  
  //#region Add Attachment
    
    function AddAttachmentCloseFunction() {
      var AddAttachmentModal1 = document.getElementById("AddAttachmentModal1");
      var AddAttachmentModal2 = document.getElementById("AddAttachmentModal2");
      var AddAttachmentModal3 = document.getElementById("AddAttachmentModal3");

      const body = document.body;
      body.style.overflowY = '';
      AddAttachmentModal1.style.display = "none";
    };

    function AddAttachmentOnClick(attachment_id, attachment) {
      var AddAttachmentModal1 = document.getElementById("AddAttachmentModal1");
      var AddAttachmentModal2 = document.getElementById("AddAttachmentModal2");
      var AddAttachmentModal3 = document.getElementById("AddAttachmentModal3");
      AddAttachmentModal1.style.display = "block";
      AddAttachmentModal1.style.overflowY = "";
      const body = document.body;
      body.style.overflowY = 'hidden';

      if (attachment != null) {
        // alert("attachment : " + attachment + " || attachment_id : " + attachment_id);
        document.getElementById("attachment").value = attachment;
        document.getElementById("attachment_id").value = attachment_id;
        // document.getElementById("PaperAddAttachmentSubmitButton").innerText = "Save Changes";
      } else {
        document.getElementById("attachment").value = null;
        document.getElementById("attachment_id").value = null;
        // document.getElementById("PaperAddAttachmentSubmitButton").innerText = "Add to Attachment List";
      }
    };

    document.addEventListener("click", function(e) {
      var AddAttachmentModal1 = document.getElementById("AddAttachmentModal1");
      var AddAttachmentModal2 = document.getElementById("AddAttachmentModal2");
      var AddAttachmentModal3 = document.getElementById("AddAttachmentModal3");
      if ((e.target == AddAttachmentModal1 || e.target == AddAttachmentModal2 || e.target == AddAttachmentModal3)) {
        AddAttachmentCloseFunction();
      }
    });

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $('#laravel-ajax-file-upload').submit(function(e) {
      e.preventDefault();
      var attachment = document.getElementById("attachment");
      var attachment_id = document.getElementById("attachment_id");
      var paper_id = document.getElementById("attachment_paper_id");
      var Table_Risk_Respond_Attachment_tr_no_count = document.getElementById("Table_Risk_Respond_Attachment_tr_no_count");
      var Table_Risk_Respond_Attachment_Tr_Counter = document.getElementsByClassName("Table_Risk_Respond_Attachment_No").length;
      var formData = new FormData(this);
      $.ajax({
        type: 'POST',
        url: "{{url('/RiskRecipientAddAttachment')}}",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: (data) => {
          this.reset();
          if (data.mode == "create") {
            $('#Table_Risk_Respond_Attachment')
              .append($('<tr>', {
                  class: "Table_Risk_Respond_Attachment_Tr",
                  id: "risk-attachment-id-" + data.attachment_id,
                })
                .append($('<td>', {
                  class: "Table_Risk_Respond_Attachment_No",
                  style: "color : black; text-align: center;",
                  text: parseInt(Table_Risk_Respond_Attachment_Tr_Counter) + 1,
                }))
                .append($('<td>', {
                  id: "risk-attachment-id-" + data.attachment_id + "-content",
                  text: formData.get('attachment').name,
                  onclick: "RiskAttachmentModalShow('" + formData.get('attachment').name + "','" + paper_id.value + "')",
                }))
                .append($('<td>', {
                    class: "justify-center align-center text-center"
                  })
                  .append($('<button>', {
                    type: "button",
                    onclick: "DeleteRiskRecipientAttachmentOnClick('" + data.attachment_id + "')",
                    text: " Delete ",
                    class: "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
                    style: "width:100px; margin-right: 0px"
                  })))
              );

            attachment.value = null;
            attachment_id.value = null;
            if (Table_Risk_Respond_Attachment_tr_no_count != null) {
              Table_Risk_Respond_Attachment_tr_no_count.remove();
            }
            alert("Add attachment success!");
          } else {
            document.getElementById("attachment-id-" + data.attachment_id + "-content").innerText = attachment.value;
            alert("Add attachment success!");
          }
          AddAttachmentCloseFunction();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert("Fail to add attachment!");
        },
      });
    });

  function DeleteRiskRecipientAttachmentOnClick(attachment_id) {
    $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
      type: 'post',
      url: '{{url("/RiskRecipientDeleteAttachment")}}',
      dataType: 'JSON',
      async: false, //ensure process finish then exit the function
      data: {
        attachment_id: attachment_id,
      },
      success: function(data) {
        document.getElementById("risk-attachment-id-" + attachment_id).remove();
        if (document.getElementsByClassName("Table_Risk_Attachments_Tr").length < 1) {
          $('#Table_Risk_Attachments')
            .append($('<tr>', {
                class: "Table_Risk_Attachments_tr_no_count",
                id: "Table_Risk_Attachments_tr_no_count",
              })
              .append($('<td>', {
                class: "text-center",
                colspan: "3",
                text: "No Attachments yet",
              }))
            );
      }
      alert("Remove attachment success!");
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      alert("Fail to remove attachment!");
    },
  });

  Table_Attachments_Tr = document.getElementsByClassName("Table_Attachments_No");
  var counter = 0;
  for (var i = 0; i < Table_Attachments_Tr.length; i++) {
    Table_Attachments_Tr[i].innerText = i + 1;
  }
}
  //#endregion

  function RiskAttachmentModalShow(attachment_name, paper_id) {
    var RiskAttachmentModal = document.getElementById("RiskAttachmentModal1");
    var RiskAttachmentModal2 = document.getElementById("RiskAttachmentModal2");
    var RiskAttachmentModal3 = document.getElementById("RiskAttachmentModal3");
    var fileExt = attachment_name.substring(attachment_name.lastIndexOf('.') + 1).toLowerCase();
    if(fileExt == "png" || fileExt == "jpg" || fileExt == "jpeg"){
        document.getElementById("RiskAttachmentSpanName").innerHTML = attachment_name;
        document.getElementById("RiskAttachmentSpan").innerHTML = "<img src='riskrecipientattachments/" + paper_id + "_" + attachment_name + "' style='height:500; align:center'>";
    }else{
        document.getElementById("RiskAttachmentSpanName").innerHTML = attachment_name;
        document.getElementById("RiskAttachmentSpan").innerHTML = "<iframe src='riskrecipientattachments/" + paper_id + "_" + attachment_name + "' style='height:500; width:100%; overflow-x:auto' frameborder='0'></iframe>";
    }
    // document.getElementById('attachment_name').value = attachment_name;
    RiskAttachmentModal.style.display = "block";
    RiskAttachmentModal.style.overflowY = "";
    const body = document.body;
    body.style.overflowY = 'hidden';
  };

  document.addEventListener("click", function(e)
  {
      var RiskAttachmentModal = document.getElementById("RiskAttachmentModal1");
      var RiskAttachmentModal2 = document.getElementById("RiskAttachmentModal2");
      var RiskAttachmentModal3 = document.getElementById("RiskAttachmentModal3");

      if ((e.target==RiskAttachmentModal || e.target==RiskAttachmentModal2 || e.target==RiskAttachmentModal3))
      {
        RiskAttachmentModalCloseFunction();
      }
  });

  function RiskAttachmentModalCloseFunction(){
    var RiskAttachmentModal = document.getElementById("RiskAttachmentModal1");
    var RiskAttachmentModal2 = document.getElementById("RiskAttachmentModal2");
    var RiskAttachmentModal3 = document.getElementById("RiskAttachmentModal3");

    const body = document.body;
    body.style.overflowY = '';
    RiskAttachmentModal.style.display = "none";
  }
</script>

