<style>
  table {
    border-collapse: separate;
    border: solid black 1px;
    border-radius: 6px;
    -moz-border-radius: 6px;
  }
</style>
@extends('layouts.app')
@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" href="static/css/jquery.emailinput.min.css">
<script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script>


<div class="flex justify">
  <div class=" pt-16 m-5 p-5" style="background-color: #F5F5F5; padding-bottom: 0px;">
    <!-- This example requires Tailwind CSS v2.0+ -->
    <nav aria-label="Progress">
      <ol class="overflow-hidden">
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <!-- Current Step -->
          <a href="#" class="relative flex items-start group" aria-current="step">
            <span class="h-9 flex items-center">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-green-500 rounded-full group-hover:bg-green-700">
                <!-- Heroicon name: solid/check -->
                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">DRAFT APPROVAL</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="{{ url('/selfassessment') }}" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
                <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SELF ASSESSMENT</span>
            </span>
          </a>
        </li>
        <!--    <li class="relative pb-10">-->
        <!--      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>-->
        <!--      &lt;!&ndash; Upcoming Step &ndash;&gt;-->
        <!--      <a href="#" class="relative flex items-start group">-->
        <!--        <span class="h-9 flex items-center" aria-hidden="true">-->
        <!--          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">-->
        <!--            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>-->
        <!--          </span>-->
        <!--        </span>-->
        <!--        <span class="ml-4 min-w-0 flex flex-col">-->
        <!--          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">LOA REFERENCING</span>-->
        <!--        </span>-->
        <!--      </a>-->
        <!--    </li>-->

        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="risk_ass_1" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="report_submission" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SUMMARY OF PAPER</span>
            </span>
          </a>
        </li>
        <li class="relative">
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">APPROVAL INITIATED</span>
            </span>
          </a>
        </li>
      </ol>
    </nav>
  </div>
  <div class="pt-6 h-screen " style="background-color: #F5F5F5;">
    <div class="mx-14 mt-10 sm:mt-0">
      <div class="md:grid md:grid-cols-2 md:gap-6">
        <div class="mt-5 md:mt-0 md:col-span-2">
          <form class="form-horizontal" action="self-assessment-pg5" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="shadow overflow-hidden sm:rounded-md">
              <div class="px-4 py-5 bg-white sm:p-6">
                <label class="pb-4 block text-lg font-medium text-gray-700">Self Assessment Summary</label>
                <table class="min-w-full divide-y divide-gray-200" id="myTable">
                  <thead class="bg-gray-50">
                    <tr>
                      <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Item
                      </th>
                      <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        AODM
                      </th>
                      <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Paper Threshold Value
                      </th>
                      <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Approval By
                      </th>
                      <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Reporting To
                      </th>
                      <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Line Of Sight
                      </th>
                    </tr>
                  </thead>

                  <body class="bg-white divide-y divide-gray-200">
                    <tr>
                      <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                        {{$paper->papertype->papertype_name}}
                      </td>
                      <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                        {{$paper->projectthreshold->project->project_type}}
                      </td>
                      <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                        {{$paper->projectthreshold->projecttreshold_name}}
                      </td>
                      <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                        {{$paper->projectthreshold->projecttreshold_approval}}
                      </td>
                      <td class="px-6 py-4 whitespace-nowrap text-centre text-sm font-medium">
                        -
                      </td>
                      <td class="px-6 py-4 whitespace-nowrap text-centre text-sm font-medium">
                        -
                      </td>
                    </tr>
                  </body>
                </table>
              </div>
            </div>
            <div class="flex ">
              <div class="w-full pt-4 grid grid-cols-2 rows-1">
                <div class="flex justify-start">
                  <button id="BackButton" style="width: 200px" type="button" name="BackButton" onclick="window.location='self-assessment-pg3'" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Back
                  </button>
                </div>
                <div class="flex justify-end">
                  <button id="submit" style="width: 200px" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Proceed
                  </button>
                </div>
              </div>
            </div>
            {{-- <div class="flex justify-end pt-4">
    <p>page 6 of 6</p> &nbsp;&nbsp;
    <button id="submit" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
      Proceed
    </button>
  </div> --}}
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  // function GetProjectThresholds(){
  //   var project_id = document.getElementById('project').value;
  //   alert("project_id : " + project_id);
  //   var check1;
  //   $.ajax({
  //       headers: {
  //           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  //       },
  //       type: 'post',
  //       url: '{{url("/GetProjectThresholds")}}',
  //       dataType: 'JSON',
  //       async:false, //ensure process finish then exit the function
  //       data: {
  //           project_id : project_id,
  //       },
  //       success: function (data) {
  //           // if(data.t){
  //           //     check1 = false;
  //           // }else{
  //           //     check1 = true;
  //           // }
  //           alert(data.projectthresholds.length);
  //           $('#tableProjectThresholds .trProjectThresholds').remove();
  //           for(var i in data.projectthresholds){
  //             var projectthreshold = data.projectthresholds[i];
  //             $('#tableProjectThresholds')
  //               .append($('<tr>', { class   : "trProjectThresholds", 
  //                                   onclick : ""})
  //                   .append($('<td>', { class   : "tdNo", 
  //                                       style   : "color : black; text-align: center;", 
  //                                       text    : projectthreshold.projecttreshold_name}))
  //                   .append($('<td>', { style   : "text-align:center;"})
  //                     .append($('<input>', {
  //                                         type    : "radio",
  //                                         name    : "projectthreshold_id",
  //                                         value   : projectthreshold.id}))
  //                   )
  //               );
  //           }
  //       },
  //       error: function (XMLHttpRequest, textStatus, errorThrown) {
  //       },
  //   });
  //   if(!check1){
  //       if( document.getElementById("invalidStudentBookSameDateTime")!=null){
  //           document.getElementById("invalidStudentBookSameDateTime").style.display = "block";
  //       }
  //       if( document.getElementById("BookFormButton")!=null){
  //           document.getElementById("BookFormButton").disabled = true;
  //           document.getElementById("BookFormButton").style.display = 'none';
  //       }
  //   }
  // }
</script>
@endsection