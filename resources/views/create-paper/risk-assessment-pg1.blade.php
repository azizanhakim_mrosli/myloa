<style>
  table {
    border-collapse:separate;
    border:solid black 1px;
    border-radius:6px;
    -moz-border-radius:6px;
}
  </style>
@extends('layouts.app')
@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
{{-- <link rel="stylesheet" href="static/css/jquery.emailinput.min.css"> --}}
{{-- <script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script> --}}


<div class="flex justify">
  <div class=" pt-16 m-5 p-5" style="background-color: #F5F5F5; padding-bottom: 0px;">
   <!-- This example requires Tailwind CSS v2.0+ -->
   <nav aria-label="Progress">
    <ol class="overflow-hidden">
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <!-- Current Step -->
        <a href="#" class="relative flex items-start group" aria-current="step">
          <span class="h-9 flex items-center">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-green-500 rounded-full group-hover:bg-green-700">
              <!-- Heroicon name: solid/check -->
              <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
            </span>
          </span>
            <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">DRAFT APPROVAL</span>
            </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <!-- Current Step -->
        <a href="#" class="relative flex items-start group" aria-current="step">
          <span class="h-9 flex items-center">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-green-500 rounded-full group-hover:bg-green-700">
              <!-- Heroicon name: solid/check -->
              <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
            </span>
          </span>
            <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">SELF ASSESSMENT</span>
            </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <a href="{{ url('/selfassessment') }}" class="relative flex items-start group">
          <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
              <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
          </span>
          </span>
          <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
          </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
      <!-- Upcoming Step -->
        <a href="report_submission" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
            </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
            </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <a href="#" class="relative flex items-start group">
          <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
              <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
            </span>
          </span>
          <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SUMMARY OF PAPER</span>
          </span>
        </a>
      </li>
      <li class="relative">
        <!-- Upcoming Step -->
        <a href="#" class="relative flex items-start group">
          <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
              <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
            </span>
          </span>
          <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">APPROVAL INITIATED</span>
          </span>
        </a>
      </li>
    </ol>
  </nav>
  </div>
  <div class="pt-6 h-screen w-8/12" style="background-color: #F5F5F5;">
    <div class="mx-14 mt-10 sm:mt-0">
      <div class="md:grid md:grid-cols-2 md:gap-6">
        <div class="mt-5 md:mt-0 md:col-span-2">
          <form class="form-horizontal" action="PaperAddExclusion" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="shadow overflow-hidden sm:rounded-md">
              <div class="px-4 py-5 bg-white sm:p-6">
                <label class="pb-4 block text-lg font-medium text-gray-700">As per PETRONAS RADM Exclusion List issued on 29th May 2020, any of the following decision papers are excluded from carrying out
                      Risk Assessment. Please tick the relevant category for your approval paper below. If your paper does not belongs to any of these
                      categories, then please click Proceed.</label>
                <table style="width: 100%">
                @foreach ($exclusions as $exclusion)
                  <tr>
                    <td>{{$exclusion->id}}</td>
                    <td>&nbsp;{{$exclusion->exclusion_name}} &nbsp; &nbsp;</td>
                    <td><input type="radio" name="exclusion_id" value="{{$exclusion->id}}" {{$paper->exclusion != null ? ($paper->exclusion->id == $exclusion->id ? 'checked' : '' ) : '' }}></td>
                  </tr>
                @endforeach
                </table>
                <br>
                <div class="flex justify-center">
                  <button id="submit" style="width: 200px" type="button" onclick="ClearChoiceOnClick()" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Clear Choice
                  </button>
                </div>
              </div>
            </div>
            <div class="flex ">
              <div class="w-full pt-4 grid grid-cols-2 rows-1">
                {{-- <p>page 1 of 6</p> &nbsp;&nbsp; --}}
                <div class="flex justify-start">
                  <button id="BackButton" style="width: 200px" type="button" name="BackButton" onclick="window.location='self-assessment-pg5'" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Back
                  </button>
                </div>
                <div class="flex justify-end">
                  {{-- <button id="submitdraft" style="width: 200px" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                  Save as Draft
                  </button> --}}
                  <button id="submit" style="width: 200px" type="button" onclick="ProceedOnClick()" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Proceed
                  </button>
                </div>
              </div>
            </div>
            {{-- <div class="flex justify-end pt-4">
              <p>page 3 of 6</p> &nbsp;&nbsp;
              <button id="submitdraft" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Save as Draft
              </button>
              <button id="submit" type="button" onclick="ProceedOnClick()" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Proceed
              </button>
            </div> --}}
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="RADMExclusionModal1" class="modal" style="border: none; padding-top: 20%; background-color:rgba(56, 56, 56, 0.08)">
  <div class="row justify-content-center" id="RADMExclusionModal2" >
    <div class="col-md-8 row justify-content-center" id="RADMExclusionModal3" style="border: none; text-align:center" >
      <div class="card" id="editCard2" style="border: none; width: 50%">
        <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('RADM Exclusion') }}</b>
        </div>
          <div class="card-body justify-content-center">
            <p>As per PETRONAS RADM Exclusion List issued on 29th May 2020,<p>
            <p>your paper category does not require a Risk Assessment.</p>
            <p>Proceed to seek endorsement for the RADM exclusion.</p>
            <div class="justify-content-center pt-4" style="text-align: center;">
              <form name="RADMExclusionForm" id="RADMExclusionForm" action="PaperAddExclusion">
                @csrf
                <input type="hidden" id="" name="paper_id" value="{{$paper->id}}">
                <input type="hidden" id="exclusion_id" name="exclusion_id" value="">
              </form>
              <button onclick="RADMExclusionCloseFunction()" id="RADMExclusionSubmitButton" style="width: 100px;" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                {{ __('Cancel') }}
              </button>
              &nbsp;
              &nbsp;
              <button type="submit" form="RADMExclusionForm" id="RADMExclusionSubmitButton" style="width: 100px;" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                  {{ __('Proceed') }}
              </button>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
<div id="RADMExclusionPendingModal1" class="modal" style="border: none; padding-top: 20%; background-color:rgba(56, 56, 56, 0.08)">
  <div class="row justify-content-center" id="RADMExclusionPendingModal2" >
    <div class="col-md-8 row justify-content-center" id="RADMExclusionPendingModal3" style="border: none; text-align:center" >
      <div class="card" id="editCard2" style="border: none; width: 50%">
        <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('RADM Exclusion') }}</b>
        </div>
          <div class="card-body justify-content-center">
            <p>Your paper's exclusion from RADM is pending endorsement<p>
            <p>before you can proceed with submission for approval.</p>
            <div class="justify-content-center pt-4" style="text-align: center;">
              <form name="RADMExclusionPendingForm" id="RADMExclusionPendingForm" action="PaperAddExclusion">
                @csrf
                <input type="hidden" name="paper_id" value="{{$paper->id}}">
              </form>
              <button onclick="RADMExclusionPendingCloseFunction()" id="RADMExclusionPendingSubmitButton" style="width: 100px;" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                {{ __('Cancel') }}
              </button>
              &nbsp;
              &nbsp;
              <button type="submit" form="RADMExclusionPendingForm" id="RADMExclusionPendingSubmitButton" style="width: 100px;" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                  {{ __('Proceed') }}
              </button>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>

<script>
  var RADMExclusionModal1 = document.getElementById("RADMExclusionModal1");
  var RADMExclusionModal2 = document.getElementById("RADMExclusionModal2");
  var RADMExclusionModal3 = document.getElementById("RADMExclusionModal3");

  function RADMExclusionCloseFunction() {
      const body = document.body;
      body.style.overflowY = '';
      RADMExclusionModal1.style.display = "none";
  };

  function RADMExclusionOnClick() {
    RADMExclusionModal1.style.display = "block";
    RADMExclusionModal1.style.overflowY = "";
    const body = document.body;
    body.style.overflowY = 'hidden';
  };

  document.addEventListener("click", function(e)
  {
      if ((e.target==RADMExclusionModal1 || e.target==RADMExclusionModal2 || e.target==RADMExclusionModal3))
      {
        RADMExclusionCloseFunction();
      }
  });

  function ProceedOnClick(){
    var exclusion_id = document.querySelector('input[name="exclusion_id"]:checked');
    if(exclusion_id != null){
      document.getElementById("exclusion_id").value = exclusion_id.value;
      RADMExclusionOnClick();
    }else{
      RADMExclusionPendingOnClick();
    }
  }

  var RADMExclusionPendingModal1 = document.getElementById("RADMExclusionPendingModal1");
  var RADMExclusionPendingModal2 = document.getElementById("RADMExclusionPendingModal2");
  var RADMExclusionPendingModal3 = document.getElementById("RADMExclusionPendingModal3");

  function RADMExclusionPendingCloseFunction() {
      const body = document.body;
      body.style.overflowY = '';
      RADMExclusionPendingModal1.style.display = "none";
  };

  function RADMExclusionPendingOnClick() {
    RADMExclusionPendingModal1.style.display = "block";
    RADMExclusionPendingModal1.style.overflowY = "";
    const body = document.body;
    body.style.overflowY = 'hidden';
  };

  document.addEventListener("click", function(e)
  {
      if ((e.target==RADMExclusionPendingModal1 || e.target==RADMExclusionPendingModal2 || e.target==RADMExclusionPendingModal3))
      {
        RADMExclusionPendingCloseFunction();
      }
  });

  function ClearChoiceOnClick(){
    var exclusion_id = document.querySelector('input[name="exclusion_id"]:checked');
    if(exclusion_id != null){
      exclusion_id.checked = false;
    }
  }
</script>
@endsection
