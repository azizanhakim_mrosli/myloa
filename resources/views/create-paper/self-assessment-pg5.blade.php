<style>
  table {
    border-collapse: separate;
    border-spacing: 0;
    min-width: 350px;
  }

  table tr th,
  table tr td {
    border-right: 1px solid #bbb;
    border-bottom: 1px solid #bbb;
    padding: 5px;
  }

  table tr th:first-child,
  table tr td:first-child {
    border-left: 1px solid #bbb;
  }

  table tr th {
    background: #eee;
    border-top: 1px solid #bbb;
    text-align: left;
  }

  table tr td {
    background: #fff;
  }

  /* top-left border-radius */
  table tr:first-child th:first-child {
    border-top-left-radius: 6px;
  }

  /* top-right border-radius */
  table tr:first-child th:last-child {
    border-top-right-radius: 6px;
  }

  /* bottom-left border-radius */
  table tr:last-child td:first-child {
    border-bottom-left-radius: 6px;
  }

  /* bottom-right border-radius */
  table tr:last-child td:last-child {
    border-bottom-right-radius: 6px;
  }
</style>

@extends('layouts.app')
@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
{{-- <link rel="stylesheet" href="static/css/jquery.emailinput.min.css"> --}}
{{-- <script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script> --}}
{{-- {% with messages = get_flashed_messages(with_categories=true) %}
    {% if messages %}
        {% for category, message in messages %}
        <div class="alert alert-{{ category }}">
{{ message }}
</div>
{% endfor %}
{% endif %}
{% endwith %} --}}

<div class="" style="width:100%;">
  <div class="pt-20 pl-6 left" style="background-color: #F5F5F5; padding-bottom: 0px; width:20%">
    <!-- This example requires Tailwind CSS v2.0+ -->
    <nav aria-label="Progress">
      <ol class="overflow-hidden">
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <!-- Current Step -->
          <a href="#" class="relative flex items-start group" aria-current="step">
            <span class="h-9 flex items-center">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-green-500 rounded-full group-hover:bg-green-700">
                <!-- Heroicon name: solid/check -->
                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">DRAFT APPROVAL</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="{{ url('/selfassessment') }}" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
                <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SELF ASSESSMENT</span>
            </span>
          </a>
        </li>
        <!--    <li class="relative pb-10">-->
        <!--      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>-->
        <!--      &lt;!&ndash; Upcoming Step &ndash;&gt;-->
        <!--      <a href="#" class="relative flex items-start group">-->
        <!--        <span class="h-9 flex items-center" aria-hidden="true">-->
        <!--          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">-->
        <!--            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>-->
        <!--          </span>-->
        <!--        </span>-->
        <!--        <span class="ml-4 min-w-0 flex flex-col">-->
        <!--          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">LOA REFERENCING</span>-->
        <!--        </span>-->
        <!--      </a>-->
        <!--    </li>-->

        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="risk_ass_1" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="report_submission" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SUMMARY OF PAPER</span>
            </span>
          </a>
        </li>
        <li class="relative">
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">APPROVAL INITIATED</span>
            </span>
          </a>
        </li>
      </ol>
    </nav>
  </div>
  <div class="pt-6 flex-grow" style="background-color: #F5F5F5; height:100%; width:100% ">
    <div class="mx-14 mt-10 sm:mt-0 flex-none" style=" height:100%">
      <div class="md:grid md:grid-cols-2 md:gap-6" style="height: 100%">
        <div class="md:mt-0 md:col-span-2 overflow-hidden" style=" height: 100%">
          <form class="form-horizontal" action="{{ url('risk-assessment-pg1') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div style="height: 90%">
              {{-- <div class="shadow overflow-auto sm:rounded-md"> --}}
              <div class="px-4 py-5 shadow overflow-auto sm:rounded-md bg-white h-full">
                <label class="pb-4 block text-lg font-medium text-gray-700">Draft Approval : New Approval Paper</label>
                <div class="grid grid-cols-9 gap-9 ">
                  <div class="col-span-9 mx-20 px-20">
                    <label class="block text-sm font-medium text-gray-700">Send From</label>
                    <b>{{Auth::user()->name}}</b>
                    <p>{{Auth::user()->user_role}}</p>
                    {{-- <input id="company" name="company" type="text" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="" required> --}}
                  </div>
                  {{-- <p>
                      <div class="col-span-6">
                        <label class="block text-sm font-medium text-gray-700">Reference No</label>
                        <input id="title" name="title" type="text" placeholder="Insert Ref. No (Only if applicable)" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                      </div>
                    </p> --}}
                  <div class="col-span-9 mx-20 px-20">
                    <label class="block text-sm font-medium text-gray-700">Subject</label>
                    <input id="objective" name="objective" type="text" value="{{$paper->paper_subject}}" disabled class="ei mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" required>
                  </div>
                  {{-- <div class="col-span-6 ">
                      <label  class="block text-sm font-medium text-gray-700">Body</label>
                      <textarea id="message" name="message" rows="4" placeholder="Message Body Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max" required></textarea>
                    </div> --}}
                  <div class="col-span-9 mx-20 px-20 ">
                    {{-- <label  class="block text-sm font-medium text-gray-700">Importance <span style="color: red">*</span></label> --}}
                    <table style="width:100%; border:rgb(170, 170, 170) solid 1px" class="rounded-md">
                      <tr>
                        <td style="border-left: rgb(170, 170, 170) solid 1px; padding : 3px">
                          <b>Importance</b>
                          {{-- <button class="width-full btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Default</button> --}}
                        </td>
                        <td style="border-left: rgb(170, 170, 170) solid 1px; padding : 3px">
                          <b>Date</b>
                          {{-- <button class="width-full btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">High</button> --}}
                        </td>
                        <td style="border-left: rgb(170, 170, 170) solid 1px; padding : 3px">
                          <b>Reference</b>
                          {{-- <button class="width-full btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">High</button> --}}
                        </td>
                      </tr>
                      <tr style=" text-align:center">
                        <td style="height:75px;border-left: rgb(170, 170, 170) solid 1px">
                          {{$paper->paper_importance}}
                        </td>
                        <td style="border-left: rgb(170, 170, 170) solid 1px">
                          {{$paper->paper_app_date}}
                        </td>
                        <td style="border-left: rgb(170, 170, 170) solid 1px">
                          {{$paper->paper_ref_number}}
                        </td>
                      </tr>
                    </table>
                  </div>
                  <div class="col-span-9 mx-20 px-20 " style="">
                    <label class="block text-sm font-medium text-gray-700">Objective</label>
                    <div class="col-span-6 sm:col-span-3">
                      <table class="min-w-full divide-y" id="Table_Objectives">
                        <thead style="background:; border-radius: 25px;">
                          <tr style="">
                            <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%">
                              No
                            </th>
                            <th scope="col" class="px-8 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider text-center">
                              Objectives
                            </th>
                          </tr>
                        </thead>
                        @if ($paper->objectives->count() == 0)
                        <tr id="Table_Objectives_tr_no_count">
                          <td colspan="3" class="text-center">
                            No objectives yet
                          </td>
                        </tr>
                        @else
                        @php
                        $counter = 1;
                        @endphp
                        @foreach ($paper->objectives as $objective)
                        <tr id="objective-id-{{$objective->id}}" class="Table_Objectives_Tr">
                          <td class=" text-center Table_Objectives_No">
                            {{$counter}}
                          </td>
                          <td id="objective-id-{{$objective->id}}-content">
                            {{$objective->objective}}
                          </td>
                        </tr>
                        @php
                        $counter++;
                        @endphp
                        @endforeach
                        @endif
                      </table>
                    </div>
                  </div>
                  <div class="col-span-9 mx-20 px-20" style="">
                    <label class="block text-sm font-medium text-gray-700">Content of Approval Paper</label>
                    <div class="col-span-6 sm:col-span-3">
                      <textarea readonly id="paper_content" maxlength="10000" oninput="paper_content_oninput()" name="paper_content" rows="10" placeholder="Paper Content Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max" required>{{$paper->paper_content != null ? $paper->paper_content : ''}}</textarea>
                    </div>
                  </div>
                  <div class="col-span-9 mx-20 px-20" style="">
                    <label class="block text-sm font-medium text-gray-700">Attach your file(s) here</label>
                    @if ($paper->rpt->rpt_related_pdb == "Yes")
                    <label class="block text-sm font-medium text-gray-700" style="color: red">Please attach the relevant RPT Assessment (email communication, report, etc)*</label>
                    @endif
                    <div class="col-span-6 sm:col-span-3">
                      <table class="min-w-full divide-y" id="Table_Attachments">
                        <thead style="background:; border-radius: 25px;">
                          <tr style="">
                            <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%">
                              No
                            </th>
                            <th scope="col" class="px-8 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider text-center">
                              Attachments
                            </th>
                            <th scope="col" class="px-8 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider text-center">
                              Action
                            </th>
                          </tr>
                        </thead>
                        @if ($paper->attachments->count() == 0)
                        <tr id="Table_Attachments_tr_no_count">
                          <td colspan="3" class="text-center">
                            No Attachments yet
                          </td>
                        </tr>
                        @else
                        @php
                        $counter = 1;
                        @endphp
                        @foreach ($paper->attachments as $attachment)
                        <tr id="attachment-id-{{$attachment->id}}" class="Table_Attachments_Tr">
                          <td class="text-center Table_Attachments_No">
                            {{$counter}}
                          </td>
                          <td id="attachment-id-{{$attachment->id}}-content" onclick="AttachmentModalShow('{{$attachment->attachment_name}}','{{$paper->id}}')">
                            {{$attachment->attachment_name}}
                          </td>
                          <td class="justify-center align-center text-center">
                            {{-- <button type="button" onclick="AddAttachmentOnClick('{{$attachment->id}}', '{{$attachment->attachment}}')" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
                            Edit
                            </button> --}}
                            <button type="button" style="width: 100px" onclick="DeleteAttachmentOnClick('{{$attachment->id}}')" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
                              Delete
                            </button>
                          </td>
                        </tr>
                        @php
                        $counter++;
                        @endphp
                        @endforeach
                        @endif
                      </table>
                      <div class="width-full flex justify-end">
                        <button type="button" onclick="AddAttachmentOnClick(null, null)" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
                          Add Attachment
                        </button>
                      </div>
                    </div>
                  </div>
                  <div class="col-span-9 mx-20 px-20" style="">
                    <label class="block text-sm font-medium text-gray-700">Recipients</label>
                    <div class="col-span-6 sm:col-span-3">
                      <table class="min-w-full divide-y" id="Table_Recipients">
                        <thead style="background:; border-radius: 25px;">
                          <tr style="">
                            <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%">
                              No
                            </th>
                            <th scope="col" class="px-8 py-3text-xs font-medium text-gray-500 uppercase tracking-wider text-center">
                              Recipients
                            </th>
                            <th scope="col" class="px-8 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider text-center" style="width: 28%">
                              Role
                            </th>
                            <th scope="col" class="px-8 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider text-center" style="width: 28%">
                              Action
                            </th>
                          </tr>
                        </thead>
                        @if ($paper->recipients->count() == 0)
                        <tr id="Table_Recipients_tr_no_count">
                          <td colspan="4" class="text-center">
                            No Recipients yet
                          </td>
                        </tr>
                        @else
                        @php
                        $counter = 1;
                        @endphp
                        @foreach ($paper->recipients as $recipient)
                        <tr id="recipient-id-{{$recipient->id}}" class="Table_Recipients_Tr">
                          <td class=" text-center Table_Recipients_No">
                            {{$counter}}
                          </td>
                          <td id="recipient-id-{{$recipient->id}}-content">
                            {{$recipient->user->name}} ({{$recipient->user->email}})
                          </td>
                          <td id="recipient-id-{{$recipient->id}}-role" class="text-center">
                            {{$recipient->recipient_category}}
                          </td>
                          <td class="justify-center align-center text-center">
                            <button style="width: 100px" type="button" onclick="DeleteRecipientOnClick('{{$recipient->id}}')" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
                              Delete
                            </button>
                          </td>
                        </tr>
                        @php
                        $counter++;
                        @endphp
                        @endforeach
                        @endif
                      </table>
                      <div class="width-full flex justify-end">
                        <button type="button" onclick="AddRecipientOnClick(null, null)" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
                          Add Recipient
                        </button>
                      </div>
                    </div>
                  </div>
                  <div class="col-span-9 mx-20 px-20" style="">
                    <label class="block text-sm font-medium text-gray-700">Recommendation</label>
                    <div class="col-span-6 sm:col-span-3">
                      <table class="min-w-full divide-y" id="Table_Recommendations">
                        <thead style="background:; border-radius: 25px;">
                          <tr style="">
                            <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%">
                              No
                            </th>
                            <th scope="col" class="px-8 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider text-center">
                              Recommendations
                            </th>
                          </tr>
                        </thead>
                        @if ($paper->recommendations->count() == 0)
                        <tr id="Table_Recommendations_tr_no_count">
                          <td colspan="3" class="text-center">
                            No Recommendations yet
                          </td>
                        </tr>
                        @else
                        @php
                        $counter = 1;
                        @endphp
                        @foreach ($paper->recommendations as $recommendation)
                        <tr id="recommendation-id-{{$recommendation->id}}" class="Table_Recommendations_Tr">
                          <td class=" text-center Table_Recommendations_No">
                            {{$counter}}
                          </td>
                          <td id="recommendation-id-{{$recommendation->id}}-content">
                            {{$recommendation->recommendation}}
                          </td>
                        </tr>
                        @php
                        $counter++;
                        @endphp
                        @endforeach
                        @endif
                      </table>
                    </div>
                  </div>
                  <div class="col-span-9 mx-20 px-20" style="">
                    <label class="block text-sm font-medium text-gray-700">Next Step</label>
                    <div class="col-span-6 sm:col-span-3">
                      <textarea readonly id="paper_next_step" name="paper_next_steps" oninput="paper_next_step_oninput()" maxlength="1000" rows="10" placeholder="Next Step Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max" required>{{$paper->paper_content != null ? $paper->paper_content : ''}}</textarea>
                    </div>
                  </div>
                  <input id="input-importance" type="hidden" name="importance" value="" required>
                  <input type="hidden" name="paper_id" value="{{$paper->id}}" required>
                  {{-- <textarea id="message" name="message" rows="4" placeholder="Message Body Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max" required></textarea> --}}
                  {{-- <div class="col-span-6 sm:col-span-3">
                      <label  class="block text-sm font-medium text-gray-700">Attach your file(s) here</label>
                      <div class="max-w-lg flex justify-center px-9 pt-5 pb-3 border-2 border-gray-300 border-dashed rounded-md">
                        <div class="space-y-1 text-center">
                          <svg class="mx-auto h-12 w-12 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                            <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                          </svg>
                          <div class="flex text-sm text-gray-600">
                          <input id="attachment" name="files" type="file" multiple="true">
                          </div>
                        </div>
                      </div>
                    </div> --}}
                </div>
              </div>
              {{-- </div> --}}
            </div>
            <div class="flex ">
              <div class="w-full pt-4 grid grid-cols-2 rows-1">
                {{-- <p>page 1 of 6</p> &nbsp;&nbsp; --}}
                <div class="flex justify-start">
                  <button id="BackButton" style="width: 200px" type="button" name="BackButton" onclick="window.location='self-assessment-pg4'" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Back
                  </button>
                </div>
                <div class="flex justify-end">
                  {{-- <button id="submitdraft" style="width: 200px" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                  Save as Draft
                  </button> --}}
                  <button id="submit" style="width: 200px" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Next
                  </button>
                </div>
              </div>
            </div>
        </div>
        </form>
      </div>
    </div>
  </div>

  <div id="AddRecipientModal1" class="modal" style="border: none; padding-top: 20%; background-color:rgba(56, 56, 56, 0.08)">
    <div class="row justify-content-center" id="AddRecipientModal2">
      <div class="col-md-8 row justify-content-center" id="AddRecipientModal3" style="border: none; text-align:center">
        <div class="card" id="editCard2" style="border: none; width: 50%">
          <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('New Recipient') }}</b>
          </div>
          @csrf
          <input type="hidden" name="paper_id" id="recipient_paper_id" value="{{$paper->id}}">
          <input type="hidden" name="recipient_id" id="recipient_id" value="">
          <div class="card-body justify-content-center">
            <p>Recipient Category</p>
            <select id="recipient_category" name="recipient_category" class="input-medium mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" required>
              <option hidden value="0">Select recipient category</option>
              <option value="Approver">Approver</option>
              <option value="Reviewer">Reviewer</option>
              <option value="CC">CC</option>
            </select>
            <br>
            <p>Recipient Email</p>
            <input id="recipient_email" name="recipient_email" type="text" class="ei mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" required>
            <br>
            <br>
            <div class="justify-content-center" style="text-align: center;">
              <button onclick="SaveRecipient()" id="PaperAddRecipientSubmitButton" class="btn btn-primary ">
                {{ __('Add to Recipient list') }}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="AddRecommendationModal1" class="modal" style="border: none; padding-top: 100px; background-color:rgba(56, 56, 56, 0.08)">
    <div class="row justify-content-center" id="AddRecommendationModal2">
      <div class="col-md-8 row justify-content-center" id="AddRecommendationModal3" style="border: none; text-align:center">
        <div class="card" id="editCard2" style="border: none; width: 100%">
          <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('New Recommendation') }}</b>
          </div>
          @csrf
          <input type="hidden" name="paper_id" id="recommendation_paper_id" value="{{$paper->id}}">
          <input type="hidden" name="recommendation_id" id="recommendation_id" value="">
          <div class="card-body justify-content-center">
            <p>Recommendation</p>
            <textarea style="width: 100%;" id="recommendation" name="recommendation" oninput="recommendation_oninput()" rows="10" cols="100" maxlength="1000" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"></textarea>
            <span id="recommendation_count" class="pl-3">0</span>/1000 characters
            <br>
            <br>
            <div class="justify-content-center" style="text-align: center;">
              <button onclick="SaveRecommendation()" id="PaperAddRecommendationSubmitButton" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                {{ __('Add to Recommendation list') }}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="AddAttachmentModal1" class="modal" style="border: none; padding-top:20%; background-color:rgba(56, 56, 56, 0.08)">
    <div class="row justify-content-center" id="AddAttachmentModal2">
      <div class="col-md-8 row justify-content-center" id="AddAttachmentModal3" style="border: none; text-align:center">
        <div class="card" id="editCard2" style="border: none; width: 50%; height : 100%">
          <div class="card-header" style="background-color:#00a57c; color:white; width:100%;"><b>{{ __('New Attachment') }}</b>
          </div>
          <form method="POST" enctype="multipart/form-data" id="laravel-ajax-file-upload" action="javascript:void(0)">
            @csrf
            <input type="hidden" name="paper_id" id="attachment_paper_id" value="{{$paper->id}}">
            <input type="hidden" name="attachment_id" id="attachment_id" value="">
            <div class="card-body justify-content-center">
              <p>Attachment</p>
              <div class="flex justify-center pb-3 border-2 border-gray-300 border-dashed rounded-md">
                <div class="space-y-1 text-center">
                  <svg class="mx-auto h-5 w-12 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                  </svg>
                  <div class="flex text-sm text-gray-600">
                    <input required id="attachment" name="attachment" type="file" multiple="false" {{$paper != null ? ($paper->rpt->rpt_related_pdb == "Yes" ? "required" : "" ) : ""}}>
                  </div>
                </div>
              </div>
              <br>
              <br>
              <div class="justify-content-center" style="text-align: center;">
                <button type="submit" class="btn btn-primary">Submit</button>

              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div id="AttachmentModal1" class="modal" style="border: none; padding-top:10%; background-color:rgba(56, 56, 56, 0.08)">
    <div class="row justify-content-center" id="AttachmentModal2">
      <div class="col-md-8 row justify-content-center" id="AttachmentModal3" style="border: none; text-align:center">
        <div class="card" id="editCard2" style="border: none; width: 100%">
          <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('Attachment') }}</b>
          </div>
          <input type="hidden" name="paper_id" id="attachment_paper_id" value="{{$paper->id}}">
          <input type="hidden" name="attachment_id" id="attachment_id" value="">
          <div class="card-body justify-content-center">
            <p><span id="AttachmentSpanName"></span></p>
            <div class="flex justify-center pb-3 border-2 border-gray-300 border-dashed rounded-md">
              <div class="space-y-1 text-center" style="width:100%">
                <center><span id="AttachmentSpan"></span></center>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  window.onload = function() {
    if (document.getElementsByClassName("Table_Objectives_No").length > 0) {
      var Table_Objectives_No = document.getElementsByClassName("Table_Objectives_No")[document.getElementsByClassName("Table_Objectives_No").length - 1];
      Table_Objectives_Counter = parseInt(Table_Objectives_No.innerText);
    } else {
      Table_Objectives_Counter = 0;
    }

    if (document.getElementsByClassName("Table_Recommendations_No").length > 0) {
      var Table_Recommendations_No = document.getElementsByClassName("Table_Recommendations_No")[document.getElementsByClassName("Table_Recommendations_No").length - 1];
      Table_Recommendations_Counter = parseInt(Table_Recommendations_No.innerText);
    } else {
      Table_Recommendations_Counter = 0;
    }

    // OnClickImportance(document.getElementById("input-importance").value);
  }

  //#region Recipient
  var AddRecipientModal1 = document.getElementById("AddRecipientModal1");
  var AddRecipientModal2 = document.getElementById("AddRecipientModal2");
  var AddRecipientModal3 = document.getElementById("AddRecipientModal3");

  function AddRecipientCloseFunction() {
    const body = document.body;
    body.style.overflowY = '';
    AddRecipientModal1.style.display = "none";
  };

  function AddRecipientOnClick(recipient_id, recipient) {
    AddRecipientModal1.style.display = "block";
    AddRecipientModal1.style.overflowY = "";
    const body = document.body;
    body.style.overflowY = 'hidden';

    if (recipient != null) {
      // alert("recipient : " + recipient + " || recipient_id : " + recipient_id);
      // document.getElementById("recipient").value = recipient;
      document.getElementById("recipient_id").value = recipient_id;
      document.getElementById("PaperAddRecipientSubmitButton").innerText = "Save Changes";
    } else {
      // document.getElementById("recipient").value = null;
      document.getElementById("recipient_id").value = null;
      document.getElementById("PaperAddRecipientSubmitButton").innerText = "Add to Recipient List";
    }
  };

  document.addEventListener("click", function(e) {
    if ((e.target == AddRecipientModal1 || e.target == AddRecipientModal2 || e.target == AddRecipientModal3)) {
      AddRecipientCloseFunction();
    }
  });

  function SaveRecipient() {
    // var recipient = document.getElementById("recipient");
    var recipient_email = document.getElementById("recipient_email");
    var paper_id = document.getElementById("recipient_paper_id");
    var recipient_category = document.getElementById("recipient_category");
    var Table_Recipients_tr_no_count = document.getElementById("Table_Recipients_tr_no_count");
    var Table_Recipients_Tr_Counter = document.getElementsByClassName("Table_Recipients_No").length;
    if (!(recipient_email.value == "" || recipient_email.value == null)) {
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'post',
        url: '{{url("/PaperAddRecipient")}}',
        dataType: 'JSON',
        async: false, //ensure process finish then exit the function
        data: {
          // recipient : recipient.value,
          recipient_email: recipient_email.value,
          recipient_category: recipient_category.value,
          paper_id: paper_id.value,
        },
        success: function(data) {
          if (data.mode == "create") {
            $('#Table_Recipients')
              .append($('<tr>', {
                  class: "",
                  id: "recipient-id-" + data.recipient.id,
                  onclick: ""
                })
                .append($('<td>', {
                  class: "Table_Recipients_No",
                  style: "color : black; text-align: center;",
                  text: parseInt(Table_Recipients_Tr_Counter) + 1,
                }))
                .append($('<td>', {
                  id: "recipient-id-" + data.recipient.id + "-content",
                  text: data.user.name + " (" + recipient_email.value + ")"
                }))
                .append($('<td>', {
                  id: "recipient-id-" + data.recipient.id + "-role",
                  class: "text-center",
                  text: data.recipient.recipient_category
                }))
                .append($('<td>', {
                    class: "justify-center align-center text-center"
                  })
                  .append($('<button>', {
                    type: "button",
                    onclick: "DeleteRecipientOnClick('" + data.recipient.id + "')",
                    text: " Delete ",
                    class: "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
                    style: "width:100px; margin-right: 0px"
                  })))
              );
            // recipient.value = null;
            recipient_email.value = null;
            recipient_category.value = null;
            if (Table_Recipients_tr_no_count != null) {
              Table_Recipients_tr_no_count.remove();
            }
            alert("Add recipient success!");
          }
          AddRecipientCloseFunction();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert("Fail to add recipient!");
        },
      });
    } else {
      AddRecipientCloseFunction();
    }
  }

  function DeleteRecipientOnClick(recipient_id) {
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/PaperDeleteRecipient")}}',
      dataType: 'JSON',
      async: false, //ensure process finish then exit the function
      data: {
        recipient_id: recipient_id,
      },
      success: function(data) {
        document.getElementById("recipient-id-" + recipient_id).remove();
        if (document.getElementsByClassName("Table_Recipients_Tr").length < 1) {
          $('#Table_Recipients')
            .append($('<tr>', {
                class: "Table_Recipients_tr_no_count",
                id: "Table_Recipients_tr_no_count",
              })
              .append($('<td>', {
                class: "text-center",
                colspan: "4",
                text: "No Recipients Yet",
              }))
            );
        }
        alert("Remove recipient success!");
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to remove recipient!");
      },
    });

    Table_Recipients_Tr = document.getElementsByClassName("Table_Recipients_No");
    var counter = 0;
    for (var i = 0; i < Table_Recipients_Tr.length; i++) {
      Table_Recipients_Tr[i].innerText = i + 1;
    }
  }
  //#endregion

  //#region Recommendation
  var AddRecommendationModal1 = document.getElementById("AddRecommendationModal1");
  var AddRecommendationModal2 = document.getElementById("AddRecommendationModal2");
  var AddRecommendationModal3 = document.getElementById("AddRecommendationModal3");

  function AddRecommendationCloseFunction() {
    const body = document.body;
    body.style.overflowY = '';
    AddRecommendationModal1.style.display = "none";
  };

  function AddRecommendationOnClick(recommendation_id, recommendation) {
    AddRecommendationModal1.style.display = "block";
    AddRecommendationModal1.style.overflowY = "";
    const body = document.body;
    body.style.overflowY = 'hidden';

    if (recommendation != null) {
      // alert("recommendation : " + recommendation + " || recommendation_id : " + recommendation_id);
      document.getElementById("recommendation").value = recommendation;
      document.getElementById("recommendation_id").value = recommendation_id;
      document.getElementById("PaperAddRecommendationSubmitButton").innerText = "Save Changes";
    } else {
      document.getElementById("recommendation").value = null;
      document.getElementById("recommendation_id").value = null;
      document.getElementById("PaperAddRecommendationSubmitButton").innerText = "Add to Recommendation List";
    }
  };

  document.addEventListener("click", function(e) {
    if ((e.target == AddRecommendationModal1 || e.target == AddRecommendationModal2 || e.target == AddRecommendationModal3)) {
      AddRecommendationCloseFunction();
    }
  });

  function SaveRecommendation() {
    var recommendation = document.getElementById("recommendation");
    var recommendation_id = document.getElementById("recommendation_id");
    var paper_id = document.getElementById("recommendation_paper_id");
    var Table_Recommendations_tr_no_count = document.getElementById("Table_Recommendations_tr_no_count");
    var Table_Recommendations_Tr_Counter = document.getElementsByClassName("Table_Recommendations_No").length;
    if (!(recommendation.value == "" || recommendation.value == null)) {
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'post',
        url: '{{url("/PaperAddRecommendation")}}',
        dataType: 'JSON',
        async: false, //ensure process finish then exit the function
        data: {
          recommendation: recommendation.value,
          recommendation_id: recommendation_id.value,
          paper_id: paper_id.value,
        },
        success: function(data) {
          if (data.mode == "create") {
            $('#Table_Recommendations')
              .append($('<tr>', {
                  class: "Table_Recommendations_Tr",
                  id: "recommendation-id-" + data.recommendation_id,
                })
                .append($('<td>', {
                  class: "Table_Recommendations_No",
                  style: "color : black; text-align: center;",
                  text: parseInt(Table_Recommendations_Tr_Counter) + 1,
                }))
                .append($('<td>', {
                  id: "recommendation-id-" + data.recommendation_id + "-content",
                  text: recommendation.value
                }))
                .append($('<td>', {
                    class: "justify-center align-center text-center"
                  })
                  .append($('<button>', {
                    type: "button",
                    onclick: "AddRecommendationOnClick('" + data.recommendation_id + "', '" + recommendation.value + "')",
                    text: " Edit ",
                    class: "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
                    style: "width:100px; margin-right: 0px"
                  }))
                  .append($('<button>', {
                    type: "button",
                    onclick: "DeleteRecommendationOnClick('" + data.recommendation_id + "')",
                    text: " Delete ",
                    class: "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
                    style: "width:100px; margin-right: 0px"
                  })))
              );

            recommendation.value = null;
            recommendation_id.value = null;
            if (Table_Recommendations_tr_no_count != null) {
              Table_Recommendations_tr_no_count.remove();
            }
            alert("Add recommendation success!");
          } else {
            document.getElementById("recommendation-id-" + data.recommendation_id + "-content").innerText = recommendation.value;
            // alert("Add recommendation success!");
          }
          AddRecommendationCloseFunction();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert("Fail to add recommendation!");
        },
      });
    } else {
      AddRecommendationCloseFunction();
    }
  }

  function DeleteRecommendationOnClick(recommendation_id) {
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/PaperDeleteRecommendation")}}',
      dataType: 'JSON',
      async: false, //ensure process finish then exit the function
      data: {
        recommendation_id: recommendation_id,
      },
      success: function(data) {
        document.getElementById("recommendation-id-" + recommendation_id).remove();
        if (document.getElementsByClassName("Table_Recommendations_Tr").length < 1) {
          $('#Table_Recommendations')
            .append($('<tr>', {
                class: "Table_Recommendations_tr_no_count",
                id: "Table_Recommendations_tr_no_count",
              })
              .append($('<td>', {
                class: "text-center",
                colspan: "3",
                text: "No Recommendations yet",
              }))
            );
        }
        alert("Remove recommendation success!");
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to remove recommendation!");
      },
    });

    Table_Recommendations_Tr = document.getElementsByClassName("Table_Recommendations_No");
    var counter = 0;
    for (var i = 0; i < Table_Recommendations_Tr.length; i++) {
      Table_Recommendations_Tr[i].innerText = i + 1;
    }
  }
  //#endregion

  // #region Attachment
  var AddAttachmentModal1 = document.getElementById("AddAttachmentModal1");
  var AddAttachmentModal2 = document.getElementById("AddAttachmentModal2");
  var AddAttachmentModal3 = document.getElementById("AddAttachmentModal3");

  function AddAttachmentCloseFunction() {
    const body = document.body;
    body.style.overflowY = '';
    AddAttachmentModal1.style.display = "none";
  };

  function AddAttachmentOnClick(attachment_id, attachment) {
    AddAttachmentModal1.style.display = "block";
    AddAttachmentModal1.style.overflowY = "";
    const body = document.body;
    body.style.overflowY = 'hidden';

    if (attachment != null) {
      // alert("attachment : " + attachment + " || attachment_id : " + attachment_id);
      document.getElementById("attachment").value = attachment;
      document.getElementById("attachment_id").value = attachment_id;
      // document.getElementById("PaperAddAttachmentSubmitButton").innerText = "Save Changes";
    } else {
      document.getElementById("attachment").value = null;
      document.getElementById("attachment_id").value = null;
      // document.getElementById("PaperAddAttachmentSubmitButton").innerText = "Add to Attachment List";
    }
  };

  document.addEventListener("click", function(e) {
    if ((e.target == AddAttachmentModal1 || e.target == AddAttachmentModal2 || e.target == AddAttachmentModal3)) {
      AddAttachmentCloseFunction();
    }
  });

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $('#laravel-ajax-file-upload').submit(function(e) {
    e.preventDefault();
    var attachment = document.getElementById("attachment");
    var attachment_id = document.getElementById("attachment_id");
    var paper_id = document.getElementById("attachment_paper_id");
    var Table_Attachments_tr_no_count = document.getElementById("Table_Attachments_tr_no_count");
    var Table_Attachments_Tr_Counter = document.getElementsByClassName("Table_Attachments_No").length;
    var formData = new FormData(this);
    $.ajax({
      type: 'POST',
      url: "{{url('/PaperAddAttachment')}}",
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: (data) => {
        this.reset();
        if (data.mode == "create") {
          $('#Table_Attachments')
            .append($('<tr>', {
                class: "Table_Attachments_Tr",
                id: "attachment-id-" + data.attachment_id,
              })
              .append($('<td>', {
                class: "Table_Attachments_No",
                style: "color : black; text-align: center;",
                text: parseInt(Table_Attachments_Tr_Counter) + 1,
              }))
              .append($('<td>', {
                id: "attachment-id-" + data.attachment_id + "-content",
                text: formData.get('attachment').name,
                onclick: "AttachmentModalShow('" + formData.get('attachment').name + "','" + paper_id.value + "')",
              }))
              .append($('<td>', {
                  class: "justify-center align-center text-center"
                })
                // .append($('<button>', { type    : "button",
                //                         onclick : "AddAttachmentOnClick('"+data.attachment_id+"', '"+attachment.value+"')",
                //                         text    : " Edit ",
                //                         class   : "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
                //                         style   : "margin-right: 0px"
                //                         }))
                .append($('<button>', {
                  type: "button",
                  onclick: "DeleteAttachmentOnClick('" + data.attachment_id + "')",
                  text: " Delete ",
                  class: "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
                  style: "width:100px; margin-right: 0px"
                })))
            );

          attachment.value = null;
          attachment_id.value = null;
          if (Table_Attachments_tr_no_count != null) {
            Table_Attachments_tr_no_count.remove();
          }
          alert("Add attachment success!");
        } else {
          document.getElementById("attachment-id-" + data.attachment_id + "-content").innerText = attachment.value;
          alert("Add attachment success!");
        }
        AddAttachmentCloseFunction();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to add attachment!");
      },
    });
  });

  // #region tak pakai
  // function SaveAttachment(){
  //   var attachment = document.getElementById("attachment");
  //   var attachment_id =  document.getElementById("attachment_id");
  //   var paper_id =  document.getElementById("attachment_paper_id");
  //   var Table_Attachments_tr_no_count = document.getElementById("Table_Attachments_tr_no_count");
  //   var Table_Attachments_Tr_Counter = document.getElementsByClassName("Table_Attachments_No").length;
  //   var fd = new FormData();
  //   var files = $('#attachment')[0].files;
  //   if(files.length > 0){
  //     fd.append('file', files[0]);
  //     alert(fd.get('file').type + " || " + fd.get('file').name);
  //     $.ajax({
  //         headers: {
  //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  //         },
  //         type: 'POST',
  //         url: '{{url("/PaperAddAttachment")}}',
  //         dataType: "JSON",
  //         async:false, //ensure process finish then exit the function
  //         contentType : "application/json",
  //         processData : false,
  //         data: {
  //           fd : fd.get('file'),
  //           abc : "asdasd",
  //         },
  //         success: function (data) {
  //           alert("success");
  //           if(data.mode == "create"){
  //             $('#Table_Attachments')
  //               .append($('<tr>', { class   : "Table_Attachments_Tr",
  //                                   id      : "attachment-id-"+data.attachment_id,
  //                                   })
  //                 .append($('<td>', { class   : "Table_Attachments_No",
  //                                     style   : "color : black; text-align: center;",
  //                                     text    : parseInt(Table_Attachments_Tr_Counter)+1,
  //                                     }))
  //                 .append($('<td>', { id      : "attachment-id-"+data.attachment_id+"-content",
  //                                     text    : attachment.value
  //                                     }))
  //                 .append($('<td>', { class   : "justify-center align-center text-center"
  //                                     })
  //                   .append($('<button>', { type    : "button",
  //                                           onclick : "AddAttachmentOnClick('"+data.attachment_id+"', '"+attachment.value+"')",
  //                                           text    : " Edit ",
  //                                           class   : "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
  //                                           style   : "margin-right: 0px"
  //                                           }))
  //                   .append($('<button>', { type    : "button",
  //                                           onclick : "DeleteAttachmentOnClick('"+data.attachment_id+"')",
  //                                           text    : " Delete ",
  //                                           class   : "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
  //                                           style   : "margin-right: 0px"
  //                                           })))
  //                 );

  //             attachment.value = null;
  //             attachment_id.value = null;
  //             if(Table_Attachments_tr_no_count != null){
  //               Table_Attachments_tr_no_count.remove();
  //             }
  //             alert("Add attachment success!");
  //           }else{
  //             document.getElementById("attachment-id-"+data.attachment_id+"-content").innerText = attachment.value;
  //             alert("Add attachment success!");
  //           }
  //           AddAttachmentCloseFunction();
  //         },
  //         error: function (XMLHttpRequest, textStatus, errorThrown) {
  //           alert("Fail to add attachment!");
  //         },
  //     });
  //   }else{
  //     AddAttachmentCloseFunction();
  //   }
  // }
  // #endregion

  function DeleteAttachmentOnClick(attachment_id) {
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/PaperDeleteAttachment")}}',
      dataType: 'JSON',
      async: false, //ensure process finish then exit the function
      data: {
        attachment_id: attachment_id,
      },
      success: function(data) {
        document.getElementById("attachment-id-" + attachment_id).remove();
        if (document.getElementsByClassName("Table_Attachments_Tr").length < 1) {
          $('#Table_Attachments')
            .append($('<tr>', {
                class: "Table_Attachments_tr_no_count",
                id: "Table_Attachments_tr_no_count",
              })
              .append($('<td>', {
                class: "text-center",
                colspan: "3",
                text: "No Attachments yet",
              }))
            );
        }
        alert("Remove attachment success!");
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to remove attachment!");
      },
    });

    Table_Attachments_Tr = document.getElementsByClassName("Table_Attachments_No");
    var counter = 0;
    for (var i = 0; i < Table_Attachments_Tr.length; i++) {
      Table_Attachments_Tr[i].innerText = i + 1;
    }
  }

  var AttachmentModal = document.getElementById("AttachmentModal1");
  var AttachmentModal2 = document.getElementById("AttachmentModal2");
  var AttachmentModal3 = document.getElementById("AttachmentModal3");

  function AttachmentModalShow(attachment_name, paper_id) {
    var fileExt = attachment_name.substring(attachment_name.lastIndexOf('.') + 1).toLowerCase();
    if (fileExt == "png" || fileExt == "jpg" || fileExt == "jpeg") {
      document.getElementById("AttachmentSpanName").innerHTML = attachment_name;
      document.getElementById("AttachmentSpan").innerHTML = "<img src='attachments/" + paper_id + "_" + attachment_name + "' style='height:500; align:center'>";
    } else {
      document.getElementById("AttachmentSpanName").innerHTML = attachment_name;
      document.getElementById("AttachmentSpan").innerHTML = "<iframe src='attachments/" + paper_id + "_" + attachment_name + "' style='height:500; width:100%; overflow-x:auto' frameborder='0'></iframe>";
    }
    // document.getElementById('attachment_name').value = attachment_name;
    AttachmentModal.style.display = "block";
    AttachmentModal.style.overflowY = "";
    const body = document.body;
    body.style.overflowY = 'hidden';
  };

  document.addEventListener("click", function(e) {
    if ((e.target == AttachmentModal || e.target == AttachmentModal2 || e.target == AttachmentModal3)) {
      AttachmentModalCloseFunction();
    }
  });

  function AttachmentModalCloseFunction() {
    const body = document.body;
    body.style.overflowY = '';
    AttachmentModal.style.display = "none";
  }
  // #endregion
  paper_content = document.getElementById("paper_content");

  function paper_content_oninput() {
    document.getElementById("paper_content_count").innerText = paper_content.value.length;
  }

  paper_next_step = document.getElementById("paper_next_step");

  function paper_next_step_oninput() {
    document.getElementById("paper_next_step_count").innerText = paper_next_step.value.length;
  }

  objective = document.getElementById("objective");

  function objective_oninput() {
    document.getElementById("objective_count").innerText = objective.value.length;
  }

  recommendation = document.getElementById("recommendation");

  function recommendation_oninput() {
    document.getElementById("recommendation_count").innerText = recommendation.value.length;
  }
</script>
@endsection