<style>
  table {
    border-collapse:separate;
    border:solid black 1px;
    border-radius:6px;
    -moz-border-radius:6px;
}
  </style>
@extends('layouts.app')
@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" href="static/css/jquery.emailinput.min.css">
<script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script>


<div class="flex justify" style="">
  <div class=" pt-16 m-5 p-5" style="background-color: #F5F5F5; padding-bottom: 0px;">
   <!-- This example requires Tailwind CSS v2.0+ -->
    <nav aria-label="Progress">
      <ol class="overflow-hidden">
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <!-- Current Step -->
          <a href="#" class="relative flex items-start group" aria-current="step">
            <span class="h-9 flex items-center">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-green-500 rounded-full group-hover:bg-green-700">
                <!-- Heroicon name: solid/check -->
                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
              </span>
            </span>
              <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">DRAFT APPROVAL</span>
              </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <!-- Current Step -->
          <a href="#" class="relative flex items-start group" aria-current="step">
            <span class="h-9 flex items-center">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-green-500 rounded-full group-hover:bg-green-700">
                <!-- Heroicon name: solid/check -->
                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
              </span>
            </span>
              <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">SELF ASSESSMENT</span>
              </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="{{ url('/selfassessment') }}" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
                <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
            </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
          <a href="report_submission" class="relative flex items-start group">
              <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                  <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
              </span>
              <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
              </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SUMMARY OF PAPER</span>
            </span>
          </a>
        </li>
        <li class="relative">
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">APPROVAL INITIATED</span>
            </span>
          </a>
        </li>
      </ol>
    </nav>
  </div>
  <div class="pt-6 h-screen " style="background-color: #F5F5F5; width:100%;">
    <div class="mx-14 mt-10 sm:mt-0" >
      <div class="md:grid md:grid-cols-2 md:gap-6">
        <div class="mt-5 md:mt-0 md:col-span-2">
          <div class="shadow overflow-hidden sm:rounded-md">
            <div class="px-4 py-5 bg-white sm:p-6">
              <label class="pb-4 block text-lg font-medium text-gray-700">Risk Assessment Submission Page</label>
              <div class="grid gap-y-0 grid-cols-8 grid-rows-8 gap-12">
                @if($paper->risks->count() > 0)
                  <div class="col-span-5">
                    <b>Reviewer</b>
                    <form name="RiskAddReviewer" id="RiskAddReviewer" action="RiskAddReviewer" class="form-horizontal" method="POST" enctype="multipart/form-data">
                      @csrf 
                      <input required type="email" id="user_email" name="user_email" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                      {{-- <input type="hidden" name="reviewer_role" value="Reviewer"> --}}
                    </form>
                  </div>
                  <div class="pt-7 col-span-3">
                    <button type="submit" form="RiskAddReviewer" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                      Add
                    </button>
                  </div>
                  @if ($risk_reviewers != null)
                    <div class="col-span-3">
                      <table>
                        @foreach ($risk_reviewers as $risk_reviewer)
                          <tr>
                            <td>
                              {{$risk_reviewer->user->name}}
                            </td>
                            <td>
                              <form action="RiskRemoveReviewer" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="riskrecipient_id" value="{{$risk_reviewer->id}}">
                                <button type="submit" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                  Remove
                                </button>
                              </form>
                            </td>
                          </tr>
                        @endforeach
                      </table>
                    </div>
                  @endif
                @endif
                <div class="col-span-5">
                </div>
                <div class="pt-10 col-span-8 grid gap-y-0 grid-cols-8 grid-rows-8 gap-12">
                  <div class="col-span-5">
                    <b>Endorser</b>
                    <form name="RiskAddEndorser" id="RiskAddEndorser" action="RiskAddEndorser" class="form-horizontal" method="POST" enctype="multipart/form-data">
                      @csrf 
                      <input required type="email" id="user_email" name="user_email" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                      {{-- <input type="hidden" name="endorser_role" value="Endorser"> --}}
                    </form>
                  </div>
                  <div class="pt-7 col-span-3">
                    <button type="submit" form="RiskAddEndorser" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                      Add
                    </button>
                  </div>
                  @if ($risk_endorsers != null)
                    <div class="col-span-3">
                      <table>
                        @foreach ($risk_endorsers as $risk_endorser)
                          <tr>
                            <td>
                              {{$risk_endorser->user->name}}
                            </td>
                            <td>
                              <form action="RiskRemoveEndorser" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="riskrecipient_id" value="{{$risk_endorser->id}}">
                                <button type="submit" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                  Remove
                                </button>
                              </form>
                            </td>
                          </tr>
                        @endforeach
                      </table>
                    </div>
                  @endif
                </div>
              </div>
              <div class="pt-10 col-span-8 grid gap-y-0 grid-cols-8 grid-rows-8 gap-12">
                <div class="col-span-8">
                  <b>Remarks</b>
                  <form name="RiskSubmitEndorsement" id="RiskSubmitEndorsement" action="RiskSubmitEndorsement" class="form-horizontal" method="POST" enctype="multipart/form-data">
                    @csrf 
                    <textarea id="paper_riskremark" name="paper_riskremark" rows="10" cols="100" maxlength="1000" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">{{$paper->paper_riskremark != null ? $paper->paper_riskremark : ""}}</textarea>
                  </form>
                </div>
              </div>
              <div class="pt-10 col-span-8 grid gap-y-0 grid-cols-8 grid-rows-8 gap-12">
                <div class="col-span-8">
                  <b>Attachments</b>
                </div>
                <div class="col-span-8">
                  <table class="min-w-full divide-y" id="Table_Attachments">
                    <thead style="background:; border-radius: 25px;">
                      <tr style="">
                        <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                          No
                        </th>
                        <th scope="col" class="px-8 py-3  text-xs font-medium text-gray-500 uppercase tracking-wider text-center">
                          Attachments
                        </th>
                      </tr>
                    </thead>
                    @if ($paper->attachments->count() == 0)
                      <tr id="Table_Attachments_tr_no_count">
                        <td colspan="3" class="text-center">
                          No Attachments yet
                        </td>
                      </tr>
                    @else
                      @php
                          $counter = 1;
                      @endphp
                      @foreach ($paper->attachments as $attachment)
                        <tr id="attachment-id-{{$attachment->id}}" class="Table_Attachments_Tr">
                          <td class="text-center Table_Attachments_No">
                            {{$counter}}
                          </td>
                          <td id="attachment-id-{{$attachment->id}}-content" onclick="AttachmentModalShow('{{$attachment->attachment_name}}','{{$paper->id}}')">
                            {{$attachment->attachment_name}}
                          </td>
                        </tr>
                        @php
                          $counter++;
                      @endphp
                      @endforeach
                    @endif
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="flex justify-end pt-4">
            <!--    <p>page 1 of 6</p> &nbsp;&nbsp;-->
            <!--    <button id="submitdraft" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">-->
            <!--      Save as Draft-->
            <!--    </button>-->
            <button type="button" onclick="ConfirmSubmitModalShow()" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              Submit for endorsment
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="AttachmentModal1" class="modal" style="border: none; padding-top:10%; background-color:rgba(56, 56, 56, 0.08)">
    <div class="row justify-content-center" id="AttachmentModal2" >
      <div class="col-md-8 row justify-content-center" id="AttachmentModal3" style="border: none; text-align:center" >
        <div class="card" id="editCard2" style="border: none; width: 100%">
          <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('Attachment') }}</b>
          </div>
          <input type="hidden" name="paper_id" id="attachment_paper_id" value="{{$paper->id}}">
          <input type="hidden" name="attachment_id" id="attachment_id" value="" >
          <div class="card-body justify-content-center">
            <p><span id="AttachmentSpanName"></span></p>
            <div class="flex justify-center pb-3 border-2 border-gray-300 border-dashed rounded-md">
              <div class="space-y-1 text-center" style="width:100%">
                <center><span id="AttachmentSpan"></span></center>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="ConfirmSubmitModal1" class="modal" style="border: none; padding-top: 100px; background-color:rgba(56, 56, 56, 0.08)">
    <div class="row justify-content-center" id="ConfirmSubmitModal2" >
      <div class="col-md-8 row justify-content-center" id="ConfirmSubmitModal3" style="border: none; text-align:center" >
        <div class="card" id="editCard2" style="border: none; width: 100%">
          <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('New Recommendation') }}</b>
          </div>
          <form name="RiskConfirmSubmitForm" id="RiskConfirmSubmitForm" action="RiskConfirmSubmitForm" class="form-horizontal" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="paper_id" id="attachment_paper_id" value="{{$paper->id}}">
            <input type="hidden" name="paper_risk_remarks" id="paper_risk_remarks" value="">
            <div class="card-body justify-content-center">
              Confirm your risk assessment submission?
              <br>
              <br>
              <div class="justify-content-center" style="text-align: center;">
                <button type="submit" id="PaperConfirmSubmitButton" form="RiskSubmitEndorsement" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    {{ __('Submit Risk For Endorsment') }}
                </button>
                <button type="button" onclick="PaperCancelSubmitButton()" id="PaperCancelSubmitButton" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                  {{ __('Cancel Submission') }}
                </button>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    var AttachmentModal = document.getElementById("AttachmentModal1");
    var AttachmentModal2 = document.getElementById("AttachmentModal2");
    var AttachmentModal3 = document.getElementById("AttachmentModal3");

    function AttachmentModalShow(attachment_name, paper_id) {
        var fileExt = attachment_name.substring(attachment_name.lastIndexOf('.') + 1).toLowerCase();
        if(fileExt == "png" || fileExt == "jpg" || fileExt == "jpeg"){
            document.getElementById("AttachmentSpanName").innerHTML = attachment_name;
            document.getElementById("AttachmentSpan").innerHTML = "<img src='attachments/" + paper_id + "_" + attachment_name + "' style='height:500; align:center'>";
        }else{
            document.getElementById("AttachmentSpanName").innerHTML = attachment_name;
            document.getElementById("AttachmentSpan").innerHTML = "<iframe src='attachments/" + paper_id + "_" + attachment_name + "' style='height:500; width:100%; overflow-x:auto' frameborder='0'></iframe>";
        }
        // document.getElementById('attachment_name').value = attachment_name;
        document.getElementById("paper_risk_remarks").value = document.getElementById("paper_riskremark").innerHTML;
        AttachmentModal.style.display = "block";
        AttachmentModal.style.overflowY = "";
        const body = document.body;
        body.style.overflowY = 'hidden';
    };

    document.addEventListener("click", function(e)
    {
        if ((e.target==AttachmentModal || e.target==AttachmentModal2 || e.target==AttachmentModal3))
        {
          AttachmentModalCloseFunction();
        }
    });

    function AttachmentModalCloseFunction(){
      const body = document.body;
      body.style.overflowY = '';
      AttachmentModal.style.display = "none";
    }

    var ConfirmSubmitModal = document.getElementById("ConfirmSubmitModal1");
    var ConfirmSubmitModal2 = document.getElementById("ConfirmSubmitModal2");
    var ConfirmSubmitModal3 = document.getElementById("ConfirmSubmitModal3");

    function ConfirmSubmitModalShow() {
        // var fileExt = attachment_name.substring(attachment_name.lastIndexOf('.') + 1).toLowerCase();
        // if(fileExt == "png" || fileExt == "jpg" || fileExt == "jpeg"){
        //     document.getElementById("AttachmentSpanName").innerHTML = attachment_name;
        //     document.getElementById("AttachmentSpan").innerHTML = "<img src='attachments/" + paper_id + "_" + attachment_name + "' style='height:500; align:center'>";
        // }else{
        //     document.getElementById("AttachmentSpanName").innerHTML = attachment_name;
        //     document.getElementById("AttachmentSpan").innerHTML = "<iframe src='attachments/" + paper_id + "_" + attachment_name + "' style='height:500; width:100%; overflow-x:auto' frameborder='0'></iframe>";
        // }
        // document.getElementById('attachment_name').value = attachment_name;
        ConfirmSubmitModal.style.display = "block";
        ConfirmSubmitModal.style.overflowY = "";
        const body = document.body;
        body.style.overflowY = 'hidden';
    };

    document.addEventListener("click", function(e)
    {
        if ((e.target==ConfirmSubmitModal || e.target==ConfirmSubmitModal2 || e.target==ConfirmSubmitModal3))
        {
          ConfirmSubmitModalCloseFunction();
        }
    });

    function ConfirmSubmitModalCloseFunction(){
      const body = document.body;
      body.style.overflowY = '';
      ConfirmSubmitModal.style.display = "none";
    }
    
  </script>

@endsection
