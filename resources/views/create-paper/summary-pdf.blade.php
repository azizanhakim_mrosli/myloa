<!DOCTYPE html>
<style>
    td, th {
        border: 1px none black;
        padding: 5px;
    }
    table {
        width: 100%;
    }
</style>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <table style="width: 100%" class="">
      <tr >
        <td rowspan="3">
          Logo petronas
        </td>
        <td rowspan="3">
          For Your Approval
        </td>
        <td>
          Serial No
        </td>
      </tr>
      <tr>
          <td>
              {{$paper->id}}
          </td>
      </tr>
      <tr>
        <td>
            Open
        </td>
    </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td>
                Date : 
            </td>
            <td>
                {{$paper->paper_app_date}}
            </td>
            <td>
                From : 
            </td>
            <td>
                {{$paper->user->name}}
            </td>
        </tr>
        <tr>
            <td>
                Reference : 
            </td>
            <td>
                {{$paper->paper_ref_number}}
            </td>
            <td>
                To : 
            </td>
            <td>
                {{$paper->projectthreshold->projecttreshold_approval}}
            </td>
        </tr>
        <tr>
            <td>
                Importance : 
            </td>
            <td>
                {{$paper->paper_importance}}
            </td>
            <td>
                CC : 
            </td>
            <td>
                See Distribution List
            </td>
        </tr>
        <tr>
            <td>
                Status : 
            </td>
            <td>
                Pending Approval
            </td>
        </tr>
        <tr>
            <td>
                Subject :
            </td>
            <td colspan="3">
                {{$paper->paper_subject}}
            </td>
        </tr>
        <tr>
            <td>
                Paper Type :
            </td>
            <td colspan="3">
                {{$paper->papertype->papertype_name}} - {{$paper->projectthreshold->project->project_type}} - {{$paper->projectthreshold->projecttreshold_name}}
            </td>
        </tr>
    </table>
    <br>
    <br>
    {{$paper->paper_content}}
    <br>
    <br>
    RISK & MITIGATION
    <table>
        <tr>
            <th>
                No
            </th>
            <th>
                Risk
            </th>
            <th>
                Mitigation
            </th>
        </tr>
        @php
            $counter = 1;
        @endphp
        @foreach ($paper->risks as $risk)
        <tr>
            <td rowspan="{{$risk->mitigations->count()+1}}" style="text-align: center;" width=25>
                {{$counter}}
            </td>
            <td rowspan="{{$risk->mitigations->count()+1}}">
                {{$risk->risk_title}}
            </td>
        </tr>
            @foreach ($risk->mitigations as $mitigation)
                <tr>
                    <td>
                        {{$mitigation->mitigation_detail}}
                    </td>
                </tr>
            @endforeach
            @php
                $counter = $counter + 1;
            @endphp
        @endforeach
    </table>
    <br>
    RELATED PARTY TRANSFER PRICING (TP)
    <table>
        <tr>
            <th width=25>
                No
            </th>
            <th>
                Questions
            </th>
            <th>
                Yes
            </th>
            <th>
                No
            </th>
            <th>
                N/A
            </th>
        </tr>
        @php
            $counter = 1;
        @endphp
        <tr>
            <td style="text-align: center;">
                1
            </td>
            <td>
                Is the transacting party related to PDB?
            </td>
            <td style="text-align: center;">
                @if ($paper->rpt->rpt_related_pdb == "Yes")
                    X
                @endif
            </td>
            <td style="text-align: center;">
                @if ($paper->rpt->rpt_related_pdb == "No")
                    X
                @endif
            </td>
            <td style="text-align: center;">
                @if ($paper->rpt->rpt_related_pdb == "N/A")
                    X
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                2
            </td>
            <td>
                Is the agreed price within prevailing market rate on normal commercial term?
            </td>
            <td style="text-align: center;">
                @if ($paper->rpt->rpt_market_rate == "Yes")
                    X
                @endif
            </td>
            <td style="text-align: center;">
                @if ($paper->rpt->rpt_market_rate == "No")
                    X
                @endif
            </td>
            <td style="text-align: center;">
                @if ($paper->rpt->rpt_market_rate == "N/A")
                    X
                @endif
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                3
            </td>
            <td>
                Are the material terms at par with the non-related party?
            </td>
            <td style="text-align: center;">
                @if ($paper->rpt->rpt_at_par == "Yes")
                    X
                @endif
            </td>
            <td style="text-align: center;">
                @if ($paper->rpt->rpt_at_par == "No")
                    X
                @endif
            </td>
            <td style="text-align: center;">
                @if ($paper->rpt->rpt_at_par == "N/A")
                    X
                @endif
            </td>
        </tr>
    </table>
    <br>
    AUTHORITY
    <table>
        <tr>
            <th>
                No
            </th>
            <th>
                Objectives
            </th>
        </tr>
        @php
            $counter = 1;
        @endphp
        @foreach ($paper->objectives as $objective)
            <tr>
                <td style="text-align: center;" width=25>
                    {{$counter}}
                </td>
                <td>
                    {{$objective->objective}}
                </td>
            </tr>
            @php
                $counter = $counter + 1;
            @endphp
        @endforeach
    </table>
    <br>
    RECOMMENDATIONS
    <table>
        <tr>
            <th>
                No
            </th>
            <th>
                Recommendations
            </th>
        </tr>
        @php
            $counter = 1;
        @endphp
        @foreach ($paper->recommendations as $recommendation)
            <tr>
                <td style="text-align: center;" width=25>
                    {{$counter}}
                </td>
                <td>
                    {{$recommendation->recommendation}}
                </td>
            </tr>
            @php
                $counter = $counter + 1;
            @endphp
        @endforeach
    </table>
    <br>
    NEXT STEP
    <br>
    {{$paper->paper_next_steps}}
    <br>
    <br>
    Reviewal And Approval
    <table>
        <tr>
            <th colspan=2 style="text-align: center;">
                REVIEWER
            </th>
        </tr>
        @php
            $counter = 1;
        @endphp
        @foreach ($paper->recipients->where('recipient_category', 'Reviewer') as $recipient)
            <tr>
                <td style="text-align: center;" width=25>
                    {{$counter}}
                </td>
                <td>
                    {{$recipient->user->name}}
                </td>
            </tr>
            @php
                $counter = $counter + 1;
            @endphp
        @endforeach
        <tr>
            <th colspan=2 style="text-align: center;">
                Approver
            </th>
        </tr>
        @php
            $counter = 1;
        @endphp
        @foreach ($paper->recipients->where('recipient_category', 'Approver') as $recipient)
            <tr>
                <td style="text-align: center;" width=25>
                    {{$counter}}
                </td>
                <td>
                    {{$recipient->user->name}}
                </td>
            </tr>
            @php
                $counter = $counter + 1;
            @endphp
        @endforeach
    </table>
    <br>
    Distribution List
    <table>
        <tr>
            <th colspan=2 style="text-align: center;">
                REPORTING TO
            </th>
        </tr>
        <tr>
            <td style="text-align: center;" width=25>
                1
            </td>
            <td>
                {{$paper->Projectthreshold->projecttreshold_approval}}
            </td>
        </tr>
        <tr>
            <th colspan=2 style="text-align: center;">
                CC
            </th>
        </tr>
        @php
            $counter = 1;
        @endphp
        @foreach ($paper->recipients->where('recipient_category', 'CC') as $recipient)
            <tr>
                <td style="text-align: center;" width=25>
                    {{$counter}}
                </td>
                <td>
                    {{$recipient->user->name}}
                </td>
            </tr>
            @php
                $counter = $counter + 1;
            @endphp
        @endforeach
    </table>
  </body>
</html>