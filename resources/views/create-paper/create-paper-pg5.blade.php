@extends('layouts.app')
@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" href="static/css/jquery.emailinput.min.css">
<script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script>
<script type="text/javascript" src="static/js/projectOption.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

<div class="flex justify">
  <div class=" pt-16 m-5 p-5" style="background-color: #F5F5F5; padding-bottom: 0px;">
   <!-- This example requires Tailwind CSS v2.0+ -->
<nav aria-label="Progress">
  <ol class="overflow-hidden">
      <li class="relative pb-10">
      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
      <!-- Upcoming Step -->
       <!-- Current Step -->
      <a href="#" class="relative flex items-start group" aria-current="step">
        <span class="h-9 flex items-center" aria-hidden="true">
          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
            <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
          </span>
        </span>
        <span class="ml-4 min-w-0 flex flex-col">
          <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">SELF ASSESSMENT</span>
        </span>
      </a>
    </li>
     <li class="relative pb-10">
      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
      <!-- Upcoming Step -->
      <a href="#" class="relative flex items-start group">
        <span class="h-9 flex items-center" aria-hidden="true">
          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
          </span>
        </span>
        <span class="ml-4 min-w-0 flex flex-col">
          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
        </span>
      </a>
    </li>
    <li class="relative pb-10">
      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
      <!-- Upcoming Step -->
      <a href="report_submission" class="relative flex items-start group">
        <span class="h-9 flex items-center" aria-hidden="true">
          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
          </span>
        </span>
        <span class="ml-4 min-w-0 flex flex-col">
          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
        </span>
      </a>
    </li>


    <li class="relative pb-10">
      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
      <!-- Upcoming Step -->
      <a href="#" class="relative flex items-start group">
        <span class="h-9 flex items-center" aria-hidden="true">
          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
          </span>
        </span>
        <span class="ml-4 min-w-0 flex flex-col">
          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Summary of Paper</span>
        </span>
      </a>
    </li>
    <li class="relative">
      <!-- Upcoming Step -->
      <a href="#" class="relative flex items-start group">
        <span class="h-9 flex items-center" aria-hidden="true">
          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
          </span>
        </span>
        <span class="ml-4 min-w-0 flex flex-col">
          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Approval initiated</span>
        </span>
      </a>
    </li>
  </ol>
</nav>

  </div>


  <div class="pt-6 h-screen w-8/12" style="background-color: #F5F5F5;">
   <div class="mx-14 mt-10 sm:mt-0">
  <div class="md:grid md:grid-cols-2 md:gap-6">
    <div class="mt-5 md:mt-0 md:col-span-2">

      <form class="form-horizontal" action="create-paper-pg6" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="shadow overflow-hidden sm:rounded-md">
          <div class="px-4 py-5 bg-white sm:p-6">
             <label class="pb-4 block text-lg font-medium text-gray-700">Project</label>
             <div class="col-span-6 sm:col-span-3">
              <select id="project" name="project" onchange="get(this)" class="form-control input-medium mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"required>
                {{-- <option>--- Select Project ---</option> --}}
                <option disabled {{ old('project') == "" ? 'selected' : '' }} hidden value="">--- Select Project ---</option>
                {{-- {% for project in project %} --}}
                {{-- <option value="{{ project.id }}">{{project.name}}</option> --}}
                <option value="1">test project</option>
                {{-- {% endfor %} --}}
              </select>
             {{-- <button id="submittry" type="submit" name="submit" value="aaaa" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Select Project
             </button> --}}
             </div>
            <div style="padding-top: 50px">
                {{-- {% for p_data in p_data %} --}}
                {{-- <input type="radio" name="threshold" value="{{p_data.id}}"> {{ p_data.name }}<br> --}}
                <table style="width: 100%">
                  <tr>
                    <td>
                      <input type="radio" name="threshold" value="1">Test threshold 1<br>
                    </td>
                    <td>
                      <input type="radio" name="threshold" value="2">Test threshold 2<br>
                    </td>
                  </tr>
                </table>
                {{-- {% endfor %} --}}
            </div>
                {{-- <button id="submittry" type="submit" name="submit" value="projthreshold" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Select Threshold Value
                </button> --}}
                {{-- {% for result in result %}
                    Your Recommended Approval is {{result.approval}}<br/>
                    {{result.description}}

                {% endfor %} --}}


  <div class="flex justify-end pt-4">
      <p>page 5 of 6</p> &nbsp;&nbsp;
    <button id="submit" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
      Proceed
    </button>
</div>
</fieldset>
 </form>
    </div>
  </div>
</div>
  </div>
</div>

<script>
function get(selected) {
  var value = selected.value;
  console.log(value);
}
</script>
@endsection
