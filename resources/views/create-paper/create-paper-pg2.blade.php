@extends('layouts.app')
@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" href="static/css/jquery.emailinput.min.css">
<script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script>


<div class="flex justify">
  <div class=" pt-16 m-5 p-5" style="background-color: #F5F5F5; padding-bottom: 0px;">
    <!-- This example requires Tailwind CSS v2.0+ -->
    <nav aria-label="Progress">
      <ol class="overflow-hidden">
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <!-- Current Step -->
          <a href="" class="relative flex items-start group" aria-current="step">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
                <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">SELF ASSESSMENT</span>
            </span>
          </a>
        </li>
        {{-- <li class="relative pb-10">-->
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>-->
            &lt;!&ndash; Upcoming Step &ndash;&gt;-->
            <a href="#" class="relative flex items-start group">-->
              <span class="h-9 flex items-center" aria-hidden="true">-->
                <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">-->
                  <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>-->
                </span>-->
              </span>-->
              <span class="ml-4 min-w-0 flex flex-col">-->
                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">LOA REFERENCING</span>-->
              </span>-->
            </a>-->
        </li>--> --}}

        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="report_submission" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Summary of Paper</span>
            </span>
          </a>
        </li>
        <li class="relative">
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Approval initiated</span>
            </span>
          </a>
        </li>
      </ol>
    </nav>
  </div>
  <div class="pt-6 w-8/12" style="background-color: #F5F5F5;">
   <div class="mx-14 mt-10 sm:mt-0">
    <div class="md:grid md:grid-cols-2 md:gap-6">
      <div class="mt-5 md:mt-0 md:col-span-2">
        <form class="form-horizontal" action="create-paper-pg3" method="POST" enctype="multipart/form-data">
          @csrf
          <fieldset>
            <div class="shadow overflow-hidden sm:rounded-md">
              <div class="px-4 py-5 bg-white sm:p-6">
                <label class="pb-4 block text-lg font-medium text-gray-700">Self Assessment</label>
                <div class="grid grid-cols-12 gap-9 ">
                  <div class="col-span-9 ">
                    <label  class="block text-sm font-medium text-gray-700">Justification</label>
                    <textarea id="justification" name="justification" rows="2" cols="8" placeholder="Message Body Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max" required></textarea>
                  </div>
                  <div class="col-span-9 ">
                    <label class="block text-sm font-medium text-gray-700 ">Related Party and Transfer Pricing</label>
                    <p class="block text-sm font-normal text-gray-700 ">1. Is the transacting party related to PDB?</p>&nbsp; &nbsp;&nbsp; Yes <input type="radio" id="yes1" name="r1" value="yes">&nbsp;&nbsp;&nbsp; No <input type="radio" id="no1" name="r1" value="no"><br></br>
                    <p class="block text-sm font-normal text-gray-700 ">2. Is the agreed price within prevailing  market rate and on normal commercial terms?</p>&nbsp;&nbsp; &nbsp; Yes <input type="radio" id="yes2" name="r2" value="yes">&nbsp;&nbsp;&nbsp; No <input type="radio" id="no2" name="r2" value="no"><br></br>
                    <p class="block text-sm font-normal text-gray-700 ">3. Are the material terms at par with the non-related party?</p>&nbsp; &nbsp;&nbsp; Yes <input type="radio" id="yes3" name="r3" value="yes">&nbsp;&nbsp;&nbsp; No <input type="radio" id="no3" name="r3" value="no">
                  </div>
                  <div class="col-span-9 ">
                    <label  class="block text-sm font-medium text-gray-700">Next Steps</label>
                    <textarea id="nextstep" name="nextsteps" rows="4" cols="9" placeholder="Message Body Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max"></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div class="flex justify-end pt-4 button-bottom-cluster">
                <p>page 2 of 6</p> &nbsp;&nbsp;
                <button id="submitdraft" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                  Save as Draft
                </button>
                <a href="self_ass_3">
                  <button id="submit" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Proceed
                  </button>
                </a>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
