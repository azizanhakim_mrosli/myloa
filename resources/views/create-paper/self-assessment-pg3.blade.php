<style>
  table {
    border-collapse: separate;
    border: solid black 1px;
    border-radius: 6px;
    -moz-border-radius: 6px;
  }
</style>
@extends('layouts.app')
@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
{{-- <link rel="stylesheet" href="static/css/jquery.emailinput.min.css"> --}}
{{-- <script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script> --}}
{{-- <script type="text/javascript" src="static/js/projectOption.js"></script> --}}

<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

<div class="flex justify h-full">
  <div class=" pt-16 m-5 p-5" style="background-color: #F5F5F5; padding-bottom: 0px;">
    <!-- This example requires Tailwind CSS v2.0+ -->
    <nav aria-label="Progress">
      <ol class="overflow-hidden">
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <!-- Current Step -->
          <a href="#" class="relative flex items-start group" aria-current="step">
            <span class="h-9 flex items-center">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-green-500 rounded-full group-hover:bg-green-700">
                <!-- Heroicon name: solid/check -->
                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">DRAFT APPROVAL</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="{{ url('/selfassessment') }}" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
                <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SELF ASSESSMENT</span>
            </span>
          </a>
        </li>
        <!--    <li class="relative pb-10">-->
        <!--      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>-->
        <!--      &lt;!&ndash; Upcoming Step &ndash;&gt;-->
        <!--      <a href="#" class="relative flex items-start group">-->
        <!--        <span class="h-9 flex items-center" aria-hidden="true">-->
        <!--          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">-->
        <!--            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>-->
        <!--          </span>-->
        <!--        </span>-->
        <!--        <span class="ml-4 min-w-0 flex flex-col">-->
        <!--          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">LOA REFERENCING</span>-->
        <!--        </span>-->
        <!--      </a>-->
        <!--    </li>-->

        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="risk_ass_1" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="report_submission" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SUMMARY OF PAPER</span>
            </span>
          </a>
        </li>
        <li class="relative">
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">APPROVAL INITIATED</span>
            </span>
          </a>
        </li>
      </ol>
    </nav>

  </div>


  <div class="pt-6 w-8/12" style="background-color: #F5F5F5; height:90%">
    <div class="mx-14 mt-10 sm:mt-0">
      <div class="md:grid md:grid-cols-2 md:gap-6">
        <div class="mt-5 md:mt-0 md:col-span-2">
          <form class="form-horizontal" action="PaperAddProjectthreshold" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="paper_id" value={{$paper->id}}>
            <div class="shadow overflow-hidden sm:rounded-md">
              <div class="px-4 py-5 bg-white sm:p-6">
                <label class="pb-4 block text-lg font-medium text-gray-700">Project</label>
                <div class="col-span-6 sm:col-span-3">
                  <select id="project" name="project" onchange="GetProjectThresholds()" class="input-medium mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" required>
                    <option hidden>--- Choose Type/ Threshold ---</option>
                    @foreach ($projects as $project)
                    <option value="{{$project->id}}">{{$project->project_type}}</option>
                    @endforeach
                  </select>
                </div>
                <br>
                <div class="flex justify-center w-full">
                  <table id="tableProjectThresholds" style="width: 50%">
                  </table>
                </div>
                <br>
                <div id="ProjectThresholdApproval" class="flex justify-center w-full font-bold">
                </div>
                <br>
                <div id="ProjectThresholdApprovalPoints" style="display:none; text-align: center" class="flex justify-center w-full font-bold">
                  i. Based on total costs of project inclusive of building, equipment, consultation fee, etc. 
                  <br>
                  ii. Includes approval on terms and conditions of the agreement.
                  <br>
                  iii. Prior endorsement from IRC
                </div>
              </div>
            </div>
            <div class="flex ">
              <div class="w-full pt-4 grid grid-cols-2 rows-1">
                <div class="flex justify-start">
                  <button id="BackButton" style="width: 200px" type="button" name="BackButton" onclick="window.location='self-assessment-pg1'" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Back
                  </button>
                </div>
                <div class="flex justify-end">
                  <button id="submit" style="width: 200px" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Proceed
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="BoardModal1" class="modal mt-16" style="border: none; padding-top: 100px; background-color:rgba(56, 56, 56, 0.08)">
  <div class="row justify-content-center mx-auto" id="BoardModal2" style="height: 40%; width:50%">
    <div class="col-md-8 row justify-content-center" id="BoardModal3" style="border: none; text-align:center">
      <div class="card-header" style="background-color:#00a57c; color:white; width:100%; height:10%"><b>{{ __('') }}</b>
      </div>
      <div class="card p-5 font-bold" id="editCard2" style="border: none; width: 100%; height:100%">
        <div class="h-full flex justify-center items-center">
          For papers that require approval from the Board of Directors,
          <br>
          please consult Group Secretariat & Board Governance to register your paper.
          <br><br>
          Thank you.
        </div>
        <br>
        <div class="flex justify-center">
          <button style="width: 200px" type="button" name="submit" onclick="window.location='home'" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
            Back to Landing Page
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  function GetProjectThresholds() {
    var project_id = document.getElementById('project').value;
    var check1;
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/GetProjectThresholds")}}',
      dataType: 'JSON',
      async: false, //ensure process finish then exit the function
      data: {
        project_id: project_id,
      },
      success: function(data) {
        // if(data.t){
        //     check1 = false;
        // }else{
        //     check1 = true;
        // }
        $('#tableProjectThresholds .trProjectThresholds').remove();
        for (var i in data.projectthresholds) {
          var projectthreshold = data.projectthresholds[i];
          $('#tableProjectThresholds')
            .append($('<tr>', {
                class: "trProjectThresholds",
                onclick: ""
              })
              .append($('<td>', {
                class: "tdNo",
                style: "color : black; text-align: center;",
                text: projectthreshold.projecttreshold_name
              }))
              .append($('<td>', {
                  style: "text-align:center;"
                })
                .append($('<input>', {
                  type: "radio",
                  name: "projectthreshold_id",
                  value: projectthreshold.id,
                  onclick: "RadioProjectThresholdOnClick('" + projectthreshold.projecttreshold_approval + "')"
                }))
              )
            );
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {},
    });
  }

  function RadioProjectThresholdOnClick(projecttreshold_approval) {
    var ProjectThresholdApproval = document.getElementById("ProjectThresholdApproval");
    var Submit = document.getElementById("submit");
      ProjectThresholdApproval.innerText = "The approver of this paper is the " + projecttreshold_approval;
    if (projecttreshold_approval == "Board") {
      document.getElementById("ProjectThresholdApprovalPoints").style.display = "none";
      Submit.type = "button";
      Submit.setAttribute('onclick', "BoardOnClick()");
    } else {
      document.getElementById("ProjectThresholdApprovalPoints").style.display = "block";
      Submit.type = "submit";
      Submit.setAttribute('onclick', "");
    }
  }

  var BoardModal1 = document.getElementById("BoardModal1");
  var BoardModal2 = document.getElementById("BoardModal2");
  var BoardModal3 = document.getElementById("BoardModal3");

  function BoardCloseFunction() {
    const body = document.body;
    body.style.overflowY = '';
    BoardModal1.style.display = "none";
  };

  function BoardOnClick() {
    BoardModal1.style.display = "block";
    BoardModal1.style.overflowY = "";
    const body = document.body;
    body.style.overflowY = 'hidden';
  };

  document.addEventListener("click", function(e) {
    if ((e.target == BoardModal1 || e.target == BoardModal2 || e.target == BoardModal3)) {
      BoardCloseFunction();
    }
  });
</script>
@endsection