<style>
  table {
    border-collapse: separate;
    border-spacing: 0;
    min-width: 350px;
  }

  table tr th,
  table tr td {
    border-right: 1px solid #bbb;
    border-bottom: 1px solid #bbb;
    padding: 5px;
  }

  table tr th:first-child,
  table tr td:first-child {
    border-left: 1px solid #bbb;
  }

  table tr th {
    background: #eee;
    border-top: 1px solid #bbb;
    text-align: left;
  }

  table tr td {
    background: #fff;
  }

  /* top-left border-radius */
  table tr:first-child th:first-child {
    border-top-left-radius: 6px;
  }

  /* top-right border-radius */
  table tr:first-child th:last-child {
    border-top-right-radius: 6px;
  }

  /* bottom-left border-radius */
  table tr:last-child td:first-child {
    border-bottom-left-radius: 6px;
  }

  /* bottom-right border-radius */
  table tr:last-child td:last-child {
    border-bottom-right-radius: 6px;
  }
</style>

@extends('layouts.app')
@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
{{-- <link rel="stylesheet" href="static/css/jquery.emailinput.min.css"> --}}
{{-- <script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script> --}}
{{-- {% with messages = get_flashed_messages(with_categories=true) %}
    {% if messages %}
        {% for category, message in messages %}
        <div class="alert alert-{{ category }}">
{{ message }}
</div>
{% endfor %}
{% endif %}
{% endwith %} --}}

<div class="" style="width:100%;">
  <div class="pt-20 pl-6 left" style="background-color: #F5F5F5; padding-bottom: 0px; width:20%">
    <!-- This example requires Tailwind CSS v2.0+ -->
    <nav aria-label="Progress">
      <ol class="overflow-hidden">
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="{{ url('/selfassessment') }}" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
                <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
              </span>
            </span>
            <span class="ml-2 mt-2 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">DRAFT APPROVAL</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="{{ url('/selfassessment') }}" class="relative flex items-start group">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
              <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
            </span>
            </span>
            <span class="ml-2 mt-2 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SELF ASSESSMENT</span>
            </span>
          </a>
        </li>
        <!--    <li class="relative pb-10">-->
        <!--      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>-->
        <!--      &lt;!&ndash; Upcoming Step &ndash;&gt;-->
        <!--      <a href="#" class="relative flex items-start group">-->
        <!--        <span class="h-9 flex items-center" aria-hidden="true">-->
        <!--          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">-->
        <!--            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>-->
        <!--          </span>-->
        <!--        </span>-->
        <!--        <span class="ml-4 min-w-0 flex flex-col">-->
        <!--          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">LOA REFERENCING</span>-->
        <!--        </span>-->
        <!--      </a>-->
        <!--    </li>-->

        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="risk_ass_1" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-2 mt-2 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="report_submission" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-2 mt-2 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-2 mt-2 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SUMMARY OF PAPER</span>
            </span>
          </a>
        </li>
        <li class="relative">
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-2 mt-2 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">APPROVAL INITIATED</span>
            </span>
          </a>
        </li>
      </ol>
    </nav>
  </div>
  <div class="pt-6 flex-grow" style="background-color: #F5F5F5; height:100%; width:100% ">
    <div class="mx-14 mt-10 sm:mt-0 flex-none" style=" height:100%">
      <div class="md:grid md:grid-cols-2 md:gap-6" style="height: 100%">
        <div class="md:mt-0 md:col-span-2 overflow-hidden" style=" height: 100%">
          <form class="form-horizontal" action="{{ url('PaperAddDratf') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div style="height: 80%">
              <div class="px-4 py-5 sm:p-6 shadow overflow-auto sm:rounded-md" style=" background-color:#ffffff; height: 100%">
                <label class="pb-4 block text-lg font-medium text-gray-700 text-center">Draft Approval : New Approval Paper</label>
                <div class="grid grid-cols-9 gap-9 text-center">
                  <div class="col-span-6 sm:col-span-9 gap-1 text-center ">
                    <label class="block text-sm font-medium text-gray-700 ">Send From</label>
                    <b>{{Auth::user()->name}}</b>
                    <p>{{Auth::user()->user_role}}</p>
                    {{-- <input id="company" name="company" type="text" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="" required> --}}
                  </div>
                  <p>
                  <div class="col-span-9 mx-20 px-20">
                    <label class="block text-sm font-medium text-gray-700">Reference No</label>
                    <input id="title" name="paper_ref_number" type="text" placeholder="Insert Ref. No (Only if applicable)" value="{{$paper->paper_ref_number != null ? $paper->paper_ref_number : ''}}" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                  </div>
                  </p>
                  <div class="col-span-9  mx-20 px-20">
                    <label class="block text-sm font-medium text-gray-700">Subject <span style="color: red">*</span></label>
                    <input id="paper_subject" name="paper_subject" type="text" value="{{$paper->paper_subject != null ? $paper->paper_subject : ''}}" class="emailinput ei mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" required>
                  </div>
                  {{-- <div class="col-span-9 mx-20 px-20">
                    <label  class="block text-sm font-medium text-gray-700">Body</label>
                    <textarea id="message" name="message" rows="4" placeholder="Message Body Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max" required></textarea>
                  </div> --}}
                  <div class="col-span-9 mx-20 px-20" style="">
                    <label class="block text-sm font-medium text-gray-700 text-center">Importance <span style="color: red">*</span></label>
                    <table style="width:100%; text-align:center">
                      <tr>
                        <td id="importance-default" style="width:40%" onClick="OnClickImportance('Default')" class="btn btn-success ml-3 bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 petronas-color-font-hover">
                          Default
                          {{-- <button class="width-full btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Default</button> --}}
                        </td>
                        <td id="importance-high" style="width:40%" onClick="OnClickImportance('High')" class="btn btn-success ml-3 bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 petronas-color-font-hover">
                          High
                          {{-- <button class="width-full btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">High</button> --}}
                        </td>
                      </tr>
                    </table>
                    <input id="input-importance" type="hidden" name="paper_importance" value="{{$paper->paper_importance != null ? $paper->paper_importance : ''}}" required>
                    <input id="" type="hidden" name="paper_id" value="{{$paper->id}}" required>
                    {{-- <textarea id="message" name="message" rows="4" placeholder="Message Body Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max" required></textarea> --}}
                  </div>
                  <div class="col-span-9 mx-20 px-20" style="">
                    <label class="block text-sm font-medium text-gray-700">Date <span style="color: red">*</span></label>
                    <div class="col-span-6 sm:col-span-3" style="text-align:right;">
                      <input id="date" name="paper_app_date" type="date" value="{{date('Y-m-d')}}" class="text-center input-medium mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" value="" required>
                    </div>
                    {{-- <textarea id="message" name="message" rows="4" placeholder="Message Body Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max" required></textarea> --}}
                  </div>
                  <div class="col-span-9 mx-20 px-20 " style="">
                    <label class="block text-sm font-medium text-gray-700">Objective</label>
                    <div class="col-span-6 sm:col-span-3">
                      <table class="min-w-full divide-y" id="Table_Objectives">
                        <thead style="background:; border-radius: 25px;">
                          <tr style="">
                            <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%">
                              No
                            </th>
                            <th scope="col" class="px-8 py-3  text-xs font-medium text-gray-500 uppercase tracking-wider text-center">
                              Objectives
                            </th>
                            <th scope="col" class="px-8 py-3  text-xs font-medium text-gray-500 uppercase tracking-wider text-center" style="width: 28%">
                              Action
                            </th>
                          </tr>
                        </thead>
                        @if ($paper->objectives->count() == 0)
                        <tr id="Table_Objectives_tr_no_count">
                          <td colspan="3" class="text-center">
                            No objectives yet
                          </td>
                        </tr>
                        @else
                        @php
                        $counter = 1;
                        @endphp
                        @foreach ($paper->objectives as $objective)
                        <tr id="objective-id-{{$objective->id}}" class="Table_Objectives_Tr">
                          <td class=" text-center Table_Objectives_No">
                            {{$counter}}
                          </td>
                          <td id="objective-id-{{$objective->id}}-content">
                            {{$objective->objective}}
                          </td>
                          <td class="justify-center align-center text-center">
                            <button type="button" style="width: 100px" onclick="AddObjectiveOnClick('{{$objective->id}}', '{{$objective->objective}}')" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
                              Edit
                            </button>
                            <button type="button" style="width: 100px" onclick="DeleteObjectiveOnClick('{{$objective->id}}')" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
                              Delete
                            </button>
                          </td>
                        </tr>
                        @php
                        $counter++;
                        @endphp
                        @endforeach
                        @endif
                      </table>

                      {{-- add new objective/edit objective --}}
                      <div class="card mt-5" id="editCard2" style="border: none; width: 100%">
                        <div id="objective_header" class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('Add New Objective') }}</b>
                        </div>

                        @csrf
                        <input type="hidden" name="paper_id" id="objective_paper_id" value="{{$paper->id}}">
                        <input type="hidden" name="objective_id" id="objective_id" value="">
                        <div class="card-body justify-content-center">
                          <p id="objective_instructions">Type your objective below :</p>
                          <input type='text' style="width: 100%;" id="objective" name="objective" oninput="objective_oninput()" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                          <span id="objective_count" class="pl-3">0</span>/200 characters
                          <br>
                          <br>
                          {{-- <div class="justify-content-center" style="text-align: center;">
                            <button onclick="SaveObjective()" id="PaperAddObjectiveSubmitButton" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                {{ __('Add to Objective list') }}
                          </button>
                        </div> --}}

                        <div class=" justify-content-center" style="width: 100%;">
                          <button id='objective_button' type="button" onclick="SaveObjective()" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
                            Add
                          </button>
                          <button id="objective_cancel" hidden type="button" onclick="AddObjectiveCloseFunction()" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Cancel
                          </button>
                        </div>
                      </div>

                    </div>



                    {{-- <div class="width-full flex justify-end" >
                        <button type="button" onclick="AddObjectiveOnClick(null, null)" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
                          Add Objective
                        </button>
                      </div> --}}



                  </div>
                </div>
                <div class="col-span-9 mx-20 px-20" style="">
                  <label class="block text-sm font-medium text-gray-700">Content of Approval Paper<span style="color: red">*</span></label>
                  <div class="col-span-6 sm:col-span-3">
                    <textarea id="paper_content" maxlength="10000" oninput="paper_content_oninput()" name="paper_content" rows="15" placeholder="Paper Content Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max" required>{{$paper->paper_content != null ? $paper->paper_content : ''}}</textarea>
                    <span id="paper_content_count" class="pl-3">0</span>/10000 characters
                  </div>
                </div>
                <div class="col-span-9 mx-20 px-20" style="">
                  <label class="block text-sm font-medium text-gray-700">Attach your file(s) here</label>
                  @if ($paper->rpt->rpt_related_pdb == "Yes")
                  <label class="block text-sm font-medium text-gray-700" style="color: red">Please attach the relevant RPT Assessment (email communication, report, etc)*</label>
                  @endif
                  <div class="col-span-6 sm:col-span-3">
                    <table class="min-w-full divide-y" id="Table_Attachments">
                      <thead style="background:; border-radius: 25px;">
                        <tr style="">
                          <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%">
                            No
                          </th>
                          <th scope="col" class="px-8 py-3  text-xs font-medium text-gray-500 uppercase tracking-wider text-center">
                            Attachments
                          </th>
                          <th scope="col" class="px-8 py-3  text-xs font-medium text-gray-500 uppercase tracking-wider text-center" style="width: 28%">
                            Action
                          </th>
                        </tr>
                      </thead>
                      @if ($paper->attachments->count() == 0)
                      <tr id="Table_Attachments_tr_no_count">
                        <td colspan="3" class="text-center">
                          No Attachments yet
                        </td>
                      </tr>
                      @else
                      @php
                      $counter = 1;
                      @endphp
                      @foreach ($paper->attachments as $attachment)
                        <tr id="attachment-id-{{$attachment->id}}" class="Table_Attachments_Tr">
                          <td class="text-center Table_Attachments_No">
                            {{$counter}}
                          </td>
                          <td id="attachment-id-{{$attachment->id}}-content" onclick="AttachmentModalShow('{{$attachment->attachment_name}}','{{$paper->id}}')">
                            {{$attachment->attachment_name}}
                          </td>
                          <td class="justify-center align-center text-center">
                            {{-- <button type="button" onclick="AddAttachmentOnClick('{{$attachment->id}}', '{{$attachment->attachment}}')" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
                            Edit
                            </button> --}}
                            <button type="button" style="width: 100px" onclick="DeleteAttachmentOnClick('{{$attachment->id}}')" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
                              Delete
                            </button>
                          </td>
                        </tr>
                      @php
                      $counter++;
                      @endphp
                      @endforeach
                      @endif
                    </table>


                    <div class="width-full flex justify-end">
                      <button type="button" onclick="AddAttachmentOnClick(null, null)" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
                        Add Attachment
                      </button>
                    </div>
                  </div>
                </div>

                {{-- Recommendation --}}
                <div class="col-span-9 mx-20 px-20" style="">
                  <label class="block text-sm font-medium text-gray-700">Recommendation</label>
                  <div class="col-span-6 sm:col-span-3">
                    <table class="min-w-full divide-y" id="Table_Recommendations">
                      <thead style="background:; border-radius: 25px;">
                        <tr style="">
                          <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%">
                            No
                          </th>
                          <th scope="col" class="px-8 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider text-center">
                            Recommendations
                          </th>
                          <th scope="col" class="px-8 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider text-center" style="width: 28%">
                            Action
                          </th>
                        </tr>
                      </thead>
                      @if ($paper->recommendations->count() == 0)
                      <tr id="Table_Recommendations_tr_no_count">
                        <td colspan="3" class="text-center">
                          No Recommendations yet
                        </td>
                      </tr>
                      @else
                      @php
                      $counter = 1;
                      @endphp
                      @foreach ($paper->recommendations as $recommendation)
                      <tr id="recommendation-id-{{$recommendation->id}}" class="Table_Recommendations_Tr">
                        <td class=" text-center Table_Recommendations_No">
                          {{$counter}}
                        </td>
                        <td id="recommendation-id-{{$recommendation->id}}-content">
                          {{$recommendation->recommendation}}
                        </td>
                        <td class="justify-center align-center text-center">
                          <button style="width: 100px" type="button" onclick="AddRecommendationOnClick('{{$recommendation->id}}', '{{$recommendation->recommendation}}')" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
                            Edit
                          </button>
                          <button style="width: 100px" type="button" onclick="DeleteRecommendationOnClick('{{$recommendation->id}}')" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
                            Delete
                          </button>
                        </td>
                      </tr>
                      @php
                      $counter++;
                      @endphp
                      @endforeach
                      @endif
                    </table>

                    {{-- add new recommendation/edit recommendation --}}
                    <div class="card mt-5" id="editCard2" style="border: none; width: 100%">
                      <div id="recommendation_header" class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('New Recommendation') }}</b>
                      </div>
                      @csrf
                      <input type="hidden" name="paper_id" id="recommendation_paper_id" value="{{$paper->id}}">
                      <input type="hidden" name="recommendation_id" id="recommendation_id" value="">
                      <div class="card-body justify-content-center">
                        <p id="recommendation_instructions">Type your recommendation below :</p>
                        <input type='text' style="width: 100%;" id="recommendation" name="recommendation" oninput="recommendation_oninput()" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        <span id="recommendation_count" class="pl-3">0</span>/200 characters
                        <br>
                        <br>
                        <div class="justify-content-center" style="text-align: center;">
                          <button id="recommendation_button" type="button" onclick="SaveRecommendation()" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            {{-- {{ __('Add Recommendation') }} --}}
                            Add
                          </button>
                          <button id="recommendation_cancel" hidden type="button" onclick="AddRecommendationCloseFunction()" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Cancel
                          </button>
                        </div>
                      </div>
                    </div>

                    {{-- <div class="width-full flex justify-end" >
                        <button type="button" onclick="AddRecommendationOnClick(null, null)" class="mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="margin-right: 0px">
                          Add Recommendation
                        </button>
                      </div> --}}
                  </div>
                </div>
                <div class="col-span-9 mx-20 px-20" style="">
                  <label class="block text-sm font-medium text-gray-700">Next Step<span style="color: red">*</span></label>
                  <div class="col-span-6 sm:col-span-3">
                    <textarea id="paper_next_step" name="paper_next_steps" oninput="paper_next_step_oninput()" maxlength="1000" rows="4" placeholder="Next Step Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max" required>{{$paper->paper_content != null ? $paper->paper_content : ''}}</textarea>
                    <span id="paper_next_step_count" class="pl-3">0</span>/1000 characters
                  </div>
                </div>
                {{-- <div class="col-span-6 sm:col-span-3">
                    <label  class="block text-sm font-medium text-gray-700">Attach your file(s) here</label>
                    <div class="max-w-lg flex justify-center px-9 pt-5 pb-3 border-2 border-gray-300 border-dashed rounded-md">
                      <div class="space-y-1 text-center">
                        <svg class="mx-auto h-12 w-12 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                          <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                        <div class="flex text-sm text-gray-600">
                        <input id="attachment" name="files" type="file" multiple="true">
                        </div>
                      </div>
                    </div>
                  </div> --}}
              </div>
            </div>
        </div>
        <div class="flex w-full col-span-2">
          <div class="w-full pt-4 grid grid-cols-2 rows-1">
            {{-- <p>page 1 of 6</p> &nbsp;&nbsp; --}}
            <div class="flex justify-start">
              <button id="BackButton" style="width: 200px" type="button" name="BackButton" onclick="window.location='draft-approval-pg1'" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Back
              </button>
            </div>
            <div class="flex justify-end">
              {{-- <button id="submitdraft" style="width: 200px" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                  Save as Draft
                  </button> --}}
              <button id="submit" style="width: 200px" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Proceed
              </button>
            </div>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

{{-- modals --}}
<div id="AddObjectiveModal1" class="modal" style="border: none; padding-top: 100px; background-color:rgba(56, 56, 56, 0.08)">
  <div class="row justify-content-center" id="AddObjectiveModal2">
    <div class="col-md-8 row justify-content-center" id="AddObjectiveModal3" style="border: none; text-align:center">
      <div class="card" id="editCard2" style="border: none; width: 100%">
        <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('New Objective') }}</b>
        </div>
        @csrf
        <input type="hidden" name="paper_id" id="objective_paper_id" value="{{$paper->id}}">
        <input type="hidden" name="objective_id" id="objective_id" value="">
        <div class="card-body justify-content-center">
          <p>Objective</p>
          <textarea required style="width: 100%;" id="objective" name="objective" oninput="objective_oninput()" maxlength="1000" rows="10" cols="100" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"></textarea>
          <span id="objective_count" class="pl-3">0</span>/1000 characters
          <br>
          <br>
          <div class="justify-content-center" style="text-align: center;">
            <button onclick="SaveObjective()" id="PaperAddObjectiveSubmitButton" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              {{ __('Add to Objective list') }}
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="AddRecommendationModal1" class="modal" style="border: none; padding-top: 100px; background-color:rgba(56, 56, 56, 0.08)">
  <div class="row justify-content-center" id="AddRecommendationModal2">
    <div class="col-md-8 row justify-content-center" id="AddRecommendationModal3" style="border: none; text-align:center">
      <div class="card mt-5" id="editCard2" style="border: none; width: 100%">
        <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('New Recommendation') }}</b>
        </div>
        @csrf
        <input type="hidden" name="paper_id" id="recommendation_paper_id" value="{{$paper->id}}">
        <input type="hidden" name="recommendation_id" id="recommendation_id" value="">
        <div class="card-body justify-content-center">
          <p>Recommendation</p>
          <textarea style="width: 100%;" id="recommendation" name="recommendation" oninput="recommendation_oninput()" rows="10" cols="100" maxlength="1000" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"></textarea>
          <span id="recommendation_count" class="pl-3">0</span>/1000 characters
          <br>
          <br>
          <div class="justify-content-center" style="text-align: center;">
            <button onclick="SaveRecommendation()" id="PaperAddRecommendationSubmitButton" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              {{ __('Add to Recommendation list') }}
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="AddAttachmentModal1" class="modal" style="border: none; padding-top:20%; background-color:rgba(56, 56, 56, 0.08)">
  <div class="row justify-content-center" id="AddAttachmentModal2">
    <div class="col-md-8 row justify-content-center" id="AddAttachmentModal3" style="border: none; text-align:center">
      <div class="card" id="editCard2" style="border: none; width: 50%; height : 100%">
        <div class="card-header" style="background-color:#00a57c; color:white; width:100%;"><b>{{ __('New Attachment') }}</b>
        </div>
        <form method="POST" enctype="multipart/form-data" id="laravel-ajax-file-upload" action="javascript:void(0)">
          @csrf
          <input type="hidden" name="paper_id" id="attachment_paper_id" value="{{$paper->id}}">
          <input type="hidden" name="attachment_id" id="attachment_id" value="">
          <div class="card-body justify-content-center">
            <p>Attachment</p>
            <div class="flex justify-center pb-3 border-2 border-gray-300 border-dashed rounded-md">
              <div class="space-y-1 text-center">
                <svg class="mx-auto h-5 w-12 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                  <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                </svg>
                <div class="flex text-sm text-gray-600">
                  <input required id="attachment" name="attachment" type="file" multiple="false" {{$paper != null ? ($paper->rpt->rpt_related_pdb == "Yes" ? "required" : "" ) : ""}}>
                </div>
              </div>
            </div>
            {{-- <textarea style="width: 100%;" id="attachment" name="attachment" oninput="attachment_oninput()" rows="10" cols="100" maxlength="1000" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"></textarea>
            <span id="attachment_count" class="pl-3">0</span>/1000 characters --}}
            <br>
            <br>
            <div class="justify-content-center" style="text-align: center;">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button onclick="SaveAttachment()" id="PaperAddAttachmentSubmitButton" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                {{ __('Add to Attachment list') }}
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="AttachmentModal1" class="modal" style="border: none; padding-top:10%; background-color:rgba(56, 56, 56, 0.08)">
  <div class="row justify-content-center" id="AttachmentModal2">
    <div class="col-md-8 row justify-content-center" id="AttachmentModal3" style="border: none; text-align:center">
      <div class="card" id="editCard2" style="border: none; width: 100%">
        <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('Attachment') }}</b>
        </div>
        <input type="hidden" name="paper_id" id="attachment_paper_id" value="{{$paper->id}}">
        <input type="hidden" name="attachment_id" id="attachment_id" value="">
        <div class="card-body justify-content-center">
          <p><span id="AttachmentSpanName"></span></p>
          <div class="flex justify-center pb-3 border-2 border-gray-300 border-dashed rounded-md">
            <div class="space-y-1 text-center" style="width:100%">
              <center><span id="AttachmentSpan"></span></center>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<script type="text/javascript">
  window.onload = function() {
    if (document.getElementsByClassName("Table_Objectives_No").length > 0) {
      var Table_Objectives_No = document.getElementsByClassName("Table_Objectives_No")[document.getElementsByClassName("Table_Objectives_No").length - 1];
      Table_Objectives_Counter = parseInt(Table_Objectives_No.innerText);
    } else {
      Table_Objectives_Counter = 0;
    }

    if (document.getElementsByClassName("Table_Recommendations_No").length > 0) {
      var Table_Recommendations_No = document.getElementsByClassName("Table_Recommendations_No")[document.getElementsByClassName("Table_Recommendations_No").length - 1];
      Table_Recommendations_Counter = parseInt(Table_Recommendations_No.innerText);
    } else {
      Table_Recommendations_Counter = 0;
    }

    OnClickImportance(document.getElementById("input-importance").value);
  }

  function OnClickImportance(importance) {
    document.getElementById("input-importance").value = importance;
    // alert(document.getElementById("input-importance").value);
    if (importance == "Default") {
      document.getElementById("importance-default").removeAttribute("class");
      document.getElementById("importance-default").classList.add('btn', 'btn-success', 'ml-3', 'inline-flex', 'justify-center', 'py-2', 'px-4', 'border', 'border-transparent', 'shadow-sm', 'text-sm', 'font-medium', 'rounded-md', 'text-white', 'bg-green-500', 'hover:bg-green-600', 'focus:outline-none', 'focus:ring-2', 'focus:ring-offset-2', 'focus:ring-indigo-500');
      document.getElementById("importance-high").removeAttribute("class");
      document.getElementById("importance-high").classList.add('btn', 'btn-success', 'ml-3', 'bg-white', 'py-2', 'px-4', 'border', 'border-gray-300', 'rounded-md', 'shadow-sm', 'text-sm', 'font-medium', 'text-gray-700', 'hover:bg-gray-50', 'focus:outline-none', 'focus:ring-2', 'focus:ring-offset-2', 'focus:ring-indigo-500', 'petronas-color-font-hover');
    } else if (importance == "High") {
      document.getElementById("importance-high").removeAttribute("class");
      document.getElementById("importance-high").classList.add('btn', 'btn-success', 'ml-3', 'inline-flex', 'justify-center', 'py-2', 'px-4', 'border', 'border-transparent', 'shadow-sm', 'text-sm', 'font-medium', 'rounded-md', 'text-white', 'bg-green-500', 'hover:bg-green-600', 'focus:outline-none', 'focus:ring-2', 'focus:ring-offset-2', 'focus:ring-indigo-500');
      document.getElementById("importance-default").removeAttribute("class");
      document.getElementById("importance-default").classList.add('btn', 'btn-success', 'ml-3', 'bg-white', 'py-2', 'px-4', 'border', 'border-gray-300', 'rounded-md', 'shadow-sm', 'text-sm', 'font-medium', 'text-gray-700', 'hover:bg-gray-50', 'focus:outline-none', 'focus:ring-2', 'focus:ring-offset-2', 'focus:ring-indigo-500', 'petronas-color-font-hover');
    }
  }

  //#region Objective
  var AddObjectiveModal1 = document.getElementById("AddObjectiveModal1");
  var AddObjectiveModal2 = document.getElementById("AddObjectiveModal2");
  var AddObjectiveModal3 = document.getElementById("AddObjectiveModal3");

  var ObjectiveHeader = document.getElementById('objective_header')
  var ObjectiveInstructions = document.getElementById('objective_instructions');
  var ObjectiveButton = document.getElementById('objective_button');

  function AddObjectiveCloseFunction() {
    const body = document.body;
    body.style.overflowY = '';
    AddObjectiveModal1.style.display = "none";

    document.getElementById("objective").value = null;
    document.getElementById("objective_id").value = null;

    ObjectiveHeader.innerHTML = '<b>Add New Objective</b>'
    ObjectiveHeader.className = 'card-header'

    ObjectiveInstructions.innerText = 'Type your objective below : ';

    ObjectiveButton.innerText = 'Add Objective'
    ObjectiveButton.className = 'mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'

    document.getElementById("objective_cancel").setAttribute("hidden", true)
  };

  function AddObjectiveOnClick(objective_id, objective) {
    // AddObjectiveModal1.style.display = "block";
    // AddObjectiveModal1.style.overflowY = "";
    // const body = document.body;
    // body.style.overflowY = 'hidden';

    document.getElementById("objective_cancel").removeAttribute("hidden")


    ObjectiveHeader.innerHTML = '<b>Edit Objective</b>'
    ObjectiveHeader.className = 'card-header'

    ObjectiveInstructions.innerText = 'Edit your objective below : ';

    ObjectiveButton.innerText = 'Edit Objective'
    ObjectiveButton.className = 'mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'



    if (objective != null) {
      // alert("objective : " + objective + " || objective_id : " + objective_id);
      document.getElementById("objective").value = objective;
      document.getElementById("objective_id").value = objective_id;
      document.getElementById("PaperAddObjectiveSubmitButton").innerText = "Save Changes";
    } else {
      document.getElementById("objective").value = null;
      document.getElementById("objective_id").value = null;
      document.getElementById("PaperAddObjectiveSubmitButton").innerText = "Add to Objective List";
    }
  };

  document.addEventListener("click", function(e) {
    if ((e.target == AddObjectiveModal1 || e.target == AddObjectiveModal2 || e.target == AddObjectiveModal3)) {
      AddObjectiveCloseFunction();
    }
  });

  function SaveObjective() {
    var objective = document.getElementById("objective");
    var objective_id = document.getElementById("objective_id");
    var paper_id = document.getElementById("objective_paper_id");
    var Table_Objectives_tr_no_count = document.getElementById("Table_Objectives_tr_no_count");
    var Table_Objectives_Tr_Counter = document.getElementsByClassName("Table_Objectives_No").length;
    if (!(objective.value == "" || objective.value == null)) {
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'post',
        url: '{{url("/PaperAddObjective")}}',
        dataType: 'JSON',
        async: false, //ensure process finish then exit the function
        data: {
          objective: objective.value,
          objective_id: objective_id.value,
          paper_id: paper_id.value,
        },
        success: function(data) {
          if (data.mode == "create") {
            $('#Table_Objectives')
              .append($('<tr>', {
                  class: "",
                  id: "objective-id-" + data.objective_id,
                  onclick: ""
                })
                .append($('<td>', {
                  class: "Table_Objectives_No",
                  style: "color : black; text-align: center;",
                  text: parseInt(Table_Objectives_Tr_Counter) + 1,
                }))
                .append($('<td>', {
                  id: "objective-id-" + data.objective_id + "-content",
                  text: objective.value
                }))
                .append($('<td>', {
                    class: "justify-center align-center text-center"
                  })
                  .append($('<button>', {
                    type: "button",
                    onclick: "AddObjectiveOnClick('" + data.objective_id + "', '" + objective.value + "')",
                    text: " Edit ",
                    class: "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
                    style: "width:100px; margin-right: 0px"
                  }))
                  .append($('<button>', {
                    type: "button",
                    onclick: "DeleteObjectiveOnClick('" + data.objective_id + "')",
                    text: " Delete ",
                    class: "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
                    style: "width:100px; margin-right: 0px"
                  })))
              );

            objective.value = null;
            objective_id.value = null;
            if (Table_Objectives_tr_no_count != null) {
              Table_Objectives_tr_no_count.remove();
            }
            alert("Add objective success!");
          } else {
            document.getElementById("objective-id-" + data.objective_id + "-content").innerText = objective.value;
            // alert("Add objective success!");
          }
          AddObjectiveCloseFunction();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert("Fail to add objective!");
        },
      });
    } else {
      AddObjectiveCloseFunction();
    }
  }

  function DeleteObjectiveOnClick(objective_id) {
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/PaperDeleteObjective")}}',
      dataType: 'JSON',
      async: false, //ensure process finish then exit the function
      data: {
        objective_id: objective_id,
      },
      success: function(data) {
        document.getElementById("objective-id-" + objective_id).remove();
        if (document.getElementsByClassName("Table_Objectives_Tr").length < 1) {
          $('#Table_Objectives')
            .append($('<tr>', {
                class: "Table_Objectives_tr_no_count",
                id: "Table_Objectives_tr_no_count",
              })
              .append($('<td>', {
                class: "text-center",
                colspan: "3",
                text: "No Objectives Yet",
              }))
            );
        }
        alert("Remove objective success!");
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to remove objective!");
      },
    });

    Table_Objectives_Tr = document.getElementsByClassName("Table_Objectives_No");
    var counter = 0;
    for (var i = 0; i < Table_Objectives_Tr.length; i++) {
      Table_Objectives_Tr[i].innerText = i + 1;
    }
  }
  //#endregion

  //#region Recommendation
    var AddRecommendationModal1 = document.getElementById("AddRecommendationModal1");
    var AddRecommendationModal2 = document.getElementById("AddRecommendationModal2");
    var AddRecommendationModal3 = document.getElementById("AddRecommendationModal3");

    var RecommendationHeader = document.getElementById('recommendation_header')
    var RecommendationInstructions = document.getElementById('recommendation_instructions');
    var RecommendationButton = document.getElementById('recommendation_button');

    function AddRecommendationCloseFunction() {
      const body = document.body;
      body.style.overflowY = '';
      AddRecommendationModal1.style.display = "none";

      document.getElementById("recommendation").value = null;
      document.getElementById("recommendation_id").value = null;

      RecommendationHeader.innerHTML = '<b>Add New Recommendation</b>'
      RecommendationHeader.className = 'card-header'

      RecommendationInstructions.innerText = 'Type your objective below : ';

      RecommendationButton.innerText = 'Add Recommendation'
      RecommendationButton.className = 'mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'

      document.getElementById("recommendation_cancel").setAttribute("hidden", true)
    };

    function AddRecommendationOnClick(recommendation_id, recommendation) {
      // AddRecommendationModal1.style.display = "block";
      // AddRecommendationModal1.style.overflowY = "";
      // const body = document.body;
      // body.style.overflowY = 'hidden';

      document.getElementById("recommendation_cancel").removeAttribute("hidden")

      RecommendationHeader.innerHTML = '<b>Edit Recommendation</b>'
      RecommendationHeader.className = 'card-header'

      RecommendationInstructions.innerText = 'Edit your recommendation below : ';

      RecommendationButton.innerText = 'Edit Recommendation'
      RecommendationButton.className = 'mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'

      if (recommendation != null) {
        // alert("recommendation : " + recommendation + " || recommendation_id : " + recommendation_id);
        document.getElementById("recommendation").value = recommendation;
        document.getElementById("recommendation_id").value = recommendation_id;
        document.getElementById("PaperAddRecommendationSubmitButton").innerText = "Save Changes";
      } else {
        document.getElementById("recommendation").value = null;
        document.getElementById("recommendation_id").value = null;
        document.getElementById("PaperAddRecommendationSubmitButton").innerText = "Add to Recommendation List";
      }
    };

    document.addEventListener("click", function(e) {
      if ((e.target == AddRecommendationModal1 || e.target == AddRecommendationModal2 || e.target == AddRecommendationModal3)) {
        AddRecommendationCloseFunction();
      }
    });

    function SaveRecommendation() {
      var recommendation = document.getElementById("recommendation");
      var recommendation_id = document.getElementById("recommendation_id");
      var paper_id = document.getElementById("recommendation_paper_id");
      var Table_Recommendations_tr_no_count = document.getElementById("Table_Recommendations_tr_no_count");
      var Table_Recommendations_Tr_Counter = document.getElementsByClassName("Table_Recommendations_No").length;
      if (!(recommendation.value == "" || recommendation.value == null)) {
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type: 'post',
          url: '{{url("/PaperAddRecommendation")}}',
          dataType: 'JSON',
          async: false, //ensure process finish then exit the function
          data: {
            recommendation: recommendation.value,
            recommendation_id: recommendation_id.value,
            paper_id: paper_id.value,
          },
          success: function(data) {
            if (data.mode == "create") {
              $('#Table_Recommendations')
                .append($('<tr>', {
                    class: "Table_Recommendations_Tr",
                    id: "recommendation-id-" + data.recommendation_id,
                  })
                  .append($('<td>', {
                    class: "Table_Recommendations_No",
                    style: "color : black; text-align: center;",
                    text: parseInt(Table_Recommendations_Tr_Counter) + 1,
                  }))
                  .append($('<td>', {
                    id: "recommendation-id-" + data.recommendation_id + "-content",
                    text: recommendation.value
                  }))
                  .append($('<td>', {
                      class: "justify-center align-center text-center"
                    })
                    .append($('<button>', {
                      type: "button",
                      onclick: "AddRecommendationOnClick('" + data.recommendation_id + "', '" + recommendation.value + "')",
                      text: " Edit ",
                      class: "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
                      style: "width:100px; margin-right: 0px"
                    }))
                    .append($('<button>', {
                      type: "button",
                      onclick: "DeleteRecommendationOnClick('" + data.recommendation_id + "')",
                      text: " Delete ",
                      class: "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
                      style: "width:100px; margin-right: 0px"
                    })))
                );

              recommendation.value = null;
              recommendation_id.value = null;
              if (Table_Recommendations_tr_no_count != null) {
                Table_Recommendations_tr_no_count.remove();
              }
              alert("Add recommendation success!");
            } else {
              document.getElementById("recommendation-id-" + data.recommendation_id + "-content").innerText = recommendation.value;
              // alert("Add recommendation success!");
            }
            AddRecommendationCloseFunction();
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("Fail to add recommendation!");
          },
        });
      } else {
        AddRecommendationCloseFunction();
      }
    }

    function DeleteRecommendationOnClick(recommendation_id) {
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'post',
        url: '{{url("/PaperDeleteRecommendation")}}',
        dataType: 'JSON',
        async: false, //ensure process finish then exit the function
        data: {
          recommendation_id: recommendation_id,
        },
        success: function(data) {
          document.getElementById("recommendation-id-" + recommendation_id).remove();
          if (document.getElementsByClassName("Table_Recommendations_Tr").length < 1) {
            $('#Table_Recommendations')
              .append($('<tr>', {
                  class: "Table_Recommendations_tr_no_count",
                  id: "Table_Recommendations_tr_no_count",
                })
                .append($('<td>', {
                  class: "text-center",
                  colspan: "3",
                  text: "No Recommendations yet",
                }))
              );
          }
          alert("Remove recommendation success!");
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert("Fail to remove recommendation!");
        },
      });

      Table_Recommendations_Tr = document.getElementsByClassName("Table_Recommendations_No");
      var counter = 0;
      for (var i = 0; i < Table_Recommendations_Tr.length; i++) {
        Table_Recommendations_Tr[i].innerText = i + 1;
      }
    }
  //#endregion

  // #region Attachment
    var AddAttachmentModal1 = document.getElementById("AddAttachmentModal1");
    var AddAttachmentModal2 = document.getElementById("AddAttachmentModal2");
    var AddAttachmentModal3 = document.getElementById("AddAttachmentModal3");

    function AddAttachmentCloseFunction() {
      const body = document.body;
      body.style.overflowY = '';
      AddAttachmentModal1.style.display = "none";
    };

    function AddAttachmentOnClick(attachment_id, attachment) {
      AddAttachmentModal1.style.display = "block";
      AddAttachmentModal1.style.overflowY = "";
      const body = document.body;
      body.style.overflowY = 'hidden';

      if (attachment != null) {
        // alert("attachment : " + attachment + " || attachment_id : " + attachment_id);
        document.getElementById("attachment").value = attachment;
        document.getElementById("attachment_id").value = attachment_id;
        // document.getElementById("PaperAddAttachmentSubmitButton").innerText = "Save Changes";
      } else {
        document.getElementById("attachment").value = null;
        document.getElementById("attachment_id").value = null;
        // document.getElementById("PaperAddAttachmentSubmitButton").innerText = "Add to Attachment List";
      }
    };

    document.addEventListener("click", function(e) {
      if ((e.target == AddAttachmentModal1 || e.target == AddAttachmentModal2 || e.target == AddAttachmentModal3)) {
        AddAttachmentCloseFunction();
      }
    });

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $('#laravel-ajax-file-upload').submit(function(e) {
      e.preventDefault();
      var attachment = document.getElementById("attachment");
      var attachment_id = document.getElementById("attachment_id");
      var paper_id = document.getElementById("attachment_paper_id");
      var Table_Attachments_tr_no_count = document.getElementById("Table_Attachments_tr_no_count");
      var Table_Attachments_Tr_Counter = document.getElementsByClassName("Table_Attachments_No").length;
      var formData = new FormData(this);
      $.ajax({
        type: 'POST',
        url: "{{url('/PaperAddAttachment')}}",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: (data) => {
          this.reset();
          if (data.mode == "create") {
            $('#Table_Attachments')
              .append($('<tr>', {
                  class: "Table_Attachments_Tr",
                  id: "attachment-id-" + data.attachment_id,
                })
                .append($('<td>', {
                  class: "Table_Attachments_No",
                  style: "color : black; text-align: center;",
                  text: parseInt(Table_Attachments_Tr_Counter) + 1,
                }))
                .append($('<td>', {
                  id: "attachment-id-" + data.attachment_id + "-content",
                  text: formData.get('attachment').name,
                  onclick: "AttachmentModalShow('" + formData.get('attachment').name + "','" + paper_id.value + "')",
                }))
                .append($('<td>', {
                    class: "justify-center align-center text-center"
                  })
                  // .append($('<button>', { type    : "button",
                  //                         onclick : "AddAttachmentOnClick('"+data.attachment_id+"', '"+attachment.value+"')",
                  //                         text    : " Edit ",
                  //                         class   : "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
                  //                         style   : "margin-right: 0px"
                  //                         }))
                  .append($('<button>', {
                    type: "button",
                    onclick: "DeleteAttachmentOnClick('" + data.attachment_id + "')",
                    text: " Delete ",
                    class: "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
                    style: "width:100px; margin-right: 0px"
                  })))
              );

            attachment.value = null;
            attachment_id.value = null;
            if (Table_Attachments_tr_no_count != null) {
              Table_Attachments_tr_no_count.remove();
            }
            alert("Add attachment success!");
          } else {
            document.getElementById("attachment-id-" + data.attachment_id + "-content").innerText = attachment.value;
            alert("Add attachment success!");
          }
          AddAttachmentCloseFunction();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert("Fail to add attachment!");
        },
      });
    });
  //#endregion

  // #region tak pakai
    // function SaveAttachment(){
    //   var attachment = document.getElementById("attachment");
    //   var attachment_id =  document.getElementById("attachment_id");
    //   var paper_id =  document.getElementById("attachment_paper_id");
    //   var Table_Attachments_tr_no_count = document.getElementById("Table_Attachments_tr_no_count");
    //   var Table_Attachments_Tr_Counter = document.getElementsByClassName("Table_Attachments_No").length;
    //   var fd = new FormData();
    //   var files = $('#attachment')[0].files;
    //   if(files.length > 0){
    //     fd.append('file', files[0]);
    //     alert(fd.get('file').type + " || " + fd.get('file').name);
    //     $.ajax({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         type: 'POST',
    //         url: '{{url("/PaperAddAttachment")}}',
    //         dataType: "JSON",
    //         async:false, //ensure process finish then exit the function
    //         contentType : "application/json",
    //         processData : false,
    //         data: {
    //           fd : fd.get('file'),
    //           abc : "asdasd",
    //         },
    //         success: function (data) {
    //           alert("success");
    //           if(data.mode == "create"){
    //             $('#Table_Attachments')
    //               .append($('<tr>', { class   : "Table_Attachments_Tr",
    //                                   id      : "attachment-id-"+data.attachment_id,
    //                                   })
    //                 .append($('<td>', { class   : "Table_Attachments_No",
    //                                     style   : "color : black; text-align: center;",
    //                                     text    : parseInt(Table_Attachments_Tr_Counter)+1,
    //                                     }))
    //                 .append($('<td>', { id      : "attachment-id-"+data.attachment_id+"-content",
    //                                     text    : attachment.value
    //                                     }))
    //                 .append($('<td>', { class   : "justify-center align-center text-center"
    //                                     })
    //                   .append($('<button>', { type    : "button",
    //                                           onclick : "AddAttachmentOnClick('"+data.attachment_id+"', '"+attachment.value+"')",
    //                                           text    : " Edit ",
    //                                           class   : "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
    //                                           style   : "margin-right: 0px"
    //                                           }))
    //                   .append($('<button>', { type    : "button",
    //                                           onclick : "DeleteAttachmentOnClick('"+data.attachment_id+"')",
    //                                           text    : " Delete ",
    //                                           class   : "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
    //                                           style   : "margin-right: 0px"
    //                                           })))
    //                 );

    //             attachment.value = null;
    //             attachment_id.value = null;
    //             if(Table_Attachments_tr_no_count != null){
    //               Table_Attachments_tr_no_count.remove();
    //             }
    //             alert("Add attachment success!");
    //           }else{
    //             document.getElementById("attachment-id-"+data.attachment_id+"-content").innerText = attachment.value;
    //             alert("Add attachment success!");
    //           }
    //           AddAttachmentCloseFunction();
    //         },
    //         error: function (XMLHttpRequest, textStatus, errorThrown) {
    //           alert("Fail to add attachment!");
    //         },
    //     });
    //   }else{
    //     AddAttachmentCloseFunction();
    //   }
    // }
  // #endregion

  function DeleteAttachmentOnClick(attachment_id) {
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/PaperDeleteAttachment")}}',
      dataType: 'JSON',
      async: false, //ensure process finish then exit the function
      data: {
        attachment_id: attachment_id,
      },
      success: function(data) {
        document.getElementById("attachment-id-" + attachment_id).remove();
        if (document.getElementsByClassName("Table_Attachments_Tr").length < 1) {
          $('#Table_Attachments')
            .append($('<tr>', {
                class: "Table_Attachments_tr_no_count",
                id: "Table_Attachments_tr_no_count",
              })
              .append($('<td>', {
                class: "text-center",
                colspan: "3",
                text: "No Attachments yet",
              }))
            );
        }
        alert("Remove attachment success!");
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to remove attachment!");
      },
    });

    Table_Attachments_Tr = document.getElementsByClassName("Table_Attachments_No");
    var counter = 0;
    for (var i = 0; i < Table_Attachments_Tr.length; i++) {
      Table_Attachments_Tr[i].innerText = i + 1;
    }
  }

  var AttachmentModal = document.getElementById("AttachmentModal1");
  var AttachmentModal2 = document.getElementById("AttachmentModal2");
  var AttachmentModal3 = document.getElementById("AttachmentModal3");

  function AttachmentModalShow(attachment_name, paper_id) {
    var fileExt = attachment_name.substring(attachment_name.lastIndexOf('.') + 1).toLowerCase();
    if (fileExt == "png" || fileExt == "jpg" || fileExt == "jpeg") {
      document.getElementById("AttachmentSpanName").innerHTML = attachment_name;
      document.getElementById("AttachmentSpan").innerHTML = "<img src='attachments/" + paper_id + "_" + attachment_name + "' style='height:500; align:center'>";
    } else {
      document.getElementById("AttachmentSpanName").innerHTML = attachment_name;
      document.getElementById("AttachmentSpan").innerHTML = "<iframe src='attachments/" + paper_id + "_" + attachment_name + "' style='height:500; width:100%; overflow-x:auto' frameborder='0'></iframe>";
    }
    // document.getElementById('attachment_name').value = attachment_name;
    AttachmentModal.style.display = "block";
    AttachmentModal.style.overflowY = "";
    const body = document.body;
    body.style.overflowY = 'hidden';
  };

  document.addEventListener("click", function(e) {
    if ((e.target == AttachmentModal || e.target == AttachmentModal2 || e.target == AttachmentModal3)) {
      AttachmentModalCloseFunction();
    }
  });

  function AttachmentModalCloseFunction() {
    const body = document.body;
    body.style.overflowY = '';
    AttachmentModal.style.display = "none";
  }
  // #endregion
  paper_content = document.getElementById("paper_content");

  function paper_content_oninput() {
    document.getElementById("paper_content_count").innerText = paper_content.value.length;
  }

  paper_next_step = document.getElementById("paper_next_step");

  function paper_next_step_oninput() {
    document.getElementById("paper_next_step_count").innerText = paper_next_step.value.length;
  }

  objective = document.getElementById("objective");

  function objective_oninput() {
    document.getElementById("objective_count").innerText = objective.value.length;
  }

  recommendation = document.getElementById("recommendation");

  function recommendation_oninput() {
    document.getElementById("recommendation_count").innerText = recommendation.value.length;
  }
</script>
@endsection