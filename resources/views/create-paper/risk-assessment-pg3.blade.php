<style>
  table {
    border-collapse:separate;
    border:solid black 1px;
    border-radius:6px;
    -moz-border-radius:6px;
}
  </style>
@extends('layouts.app')
@section('content')
{{-- <script src="http://code.jquery.com/jquery-1.12.4.min.js"></script> --}}
{{-- <link rel="stylesheet" href="static/css/jquery.emailinput.min.css"> --}}
{{-- <script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script> --}}


<div class="flex justify">
  <div class=" pt-16 m-5 p-5" style="background-color: #F5F5F5; padding-bottom: 0px;">
   <!-- This example requires Tailwind CSS v2.0+ -->
   <nav aria-label="Progress">
    <ol class="overflow-hidden">
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <!-- Current Step -->
        <a href="#" class="relative flex items-start group" aria-current="step">
          <span class="h-9 flex items-center">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-green-500 rounded-full group-hover:bg-green-700">
              <!-- Heroicon name: solid/check -->
              <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
            </span>
          </span>
            <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">DRAFT APPROVAL</span>
            </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <!-- Current Step -->
        <a href="#" class="relative flex items-start group" aria-current="step">
          <span class="h-9 flex items-center">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-green-500 rounded-full group-hover:bg-green-700">
              <!-- Heroicon name: solid/check -->
              <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
            </span>
          </span>
            <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">SELF ASSESSMENT</span>
            </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <a href="{{ url('/selfassessment') }}" class="relative flex items-start group">
          <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
              <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
          </span>
          </span>
          <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
          </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
      <!-- Upcoming Step -->
        <a href="report_submission" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
            </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
            </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <a href="#" class="relative flex items-start group">
          <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
              <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
            </span>
          </span>
          <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SUMMARY OF PAPER</span>
          </span>
        </a>
      </li>
      <li class="relative">
        <!-- Upcoming Step -->
        <a href="#" class="relative flex items-start group">
          <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
              <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
            </span>
          </span>
          <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">APPROVAL INITIATED</span>
          </span>
        </a>
      </li>
    </ol>
  </nav>
  </div>
  <div class="pt-6 h-screen " style="background-color: #F5F5F5; width:100%;">
    <div class="mx-14 mt-10 sm:mt-0" >
   <div class="md:grid md:grid-cols-2 md:gap-6">
     <div class="mt-5 md:mt-0 md:col-span-2">
       <form class="form-horizontal" action="PaperAddRisk" method="POST" enctype="multipart/form-data">
        @csrf
       <div class="shadow overflow-hidden sm:rounded-md">
         <div class="px-4 py-5 bg-white sm:p-6">
           <label class="pb-4 block text-lg font-medium text-gray-700">Risk Assessment : Risk Identification & Analysis</label>
           <div class="grid grid-cols-12 gap-12">
             <input type="hidden" name="risk_id" value="{{$risk != null ? $risk->id : null }}">
             <div class="col-span-8">
              Risk Title<br>
              <input required id="risk_title" name="risk_title" type="text" placeholder="" value="{{$risk != null ? $risk->risk_title : null }}" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
             </div>
             <div class="col-span-4">
              Risk Category<br>
              <select required id="riskcategory_id" name="riskcategory_id" class="input-medium mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                <option hidden value="">Select category</option>
                @foreach ($riskcategories as $riskcategory)
                <option {{$risk != null ? ($risk->riskcategory != null ? ($riskcategory->id == $risk->riskcategory->id ? "selected" : '') : '' ) : '' }} value="{{$riskcategory->id}}" title="{{$riskcategory->riskcategory_definition}}">{{$riskcategory->riskcategory_name}}</option>
                @endforeach
              </select>
              {{-- <input id="risk_category" name="risk_category" type="text" placeholder="" value="{{$risk != null ? $risk->risk_category : null }}" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"> --}}
              </div>
              <div class="col-span-6">
                Causes<br>
                <textarea required id="risk_causes" rows="5" name="risk_causes" maxlength=1000 class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">{{$risk != null ? $risk->risk_causes : null }}</textarea>
              </div>
              <div class="col-span-6">
                Potential Impact<br>
                <textarea required id="risk_potential_impact"  rows="5" name="risk_potential_impact" maxlength=1000 class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">{{$risk != null ? $risk->risk_potential_impact : null }}</textarea>
              </div>
              <div class="col-span-6">
                Likelihood Rating<br>
                <select required id="risk_likelihood_rating" name="risk_likelihood_rating" onchange="risk_rating_onchange()" class="input-medium mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                  <option hidden value="">Select one</option>
                  <option {{$risk != null ? ($risk->risk_likelihood_rating == 1 ? "selected" : '' ) : '' }} value="1">Remote</option>
                  <option {{$risk != null ? ($risk->risk_likelihood_rating == 2 ? "selected" : '' ) : '' }} value="2">Unlikely</option>
                  <option {{$risk != null ? ($risk->risk_likelihood_rating == 3 ? "selected" : '' ) : '' }} value="3">Possible</option>
                  <option {{$risk != null ? ($risk->risk_likelihood_rating == 4 ? "selected" : '' ) : '' }} value="4">Likely</option>
                  <option {{$risk != null ? ($risk->risk_likelihood_rating == 5 ? "selected" : '' ) : '' }} value="5">Almost Certain</option>
                </select>
              </div>
              <div class="col-span-6">
                Impact Rating<br>
                <select required id="risk_impact_rating" name="risk_impact_rating" onchange="risk_rating_onchange()" class="input-medium mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                  <option hidden value="" >Select one</option>
                  <option {{$risk != null ? ($risk->risk_impact_rating == 1 ? "selected" : '' ) : '' }} value="1">Insignificant</option>
                  <option {{$risk != null ? ($risk->risk_impact_rating == 2 ? "selected" : '' ) : '' }} value="2">Minor</option>
                  <option {{$risk != null ? ($risk->risk_impact_rating == 3 ? "selected" : '' ) : '' }} value="3">Moderate Impact</option>
                  <option {{$risk != null ? ($risk->risk_impact_rating == 4 ? "selected" : '' ) : '' }} value="4">Major</option>
                  <option {{$risk != null ? ($risk->risk_impact_rating == 5 ? "selected" : '' ) : '' }} value="5">Severe</option>
                </select>
              </div>
              <div class="col-span-6">
                Assumptions & justification for likehood rating<br>
                <textarea required id="risk_assumptions_likehood" rows="5" name="risk_assumptions_likehood" maxlength="1000" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">{{$risk != null ? $risk->risk_assumptions_likehood : null }}</textarea>
              </div>
              <div class="col-span-6">
                Assumptions & justification for impact rating<br>
                <textarea required id="risk_assumptions_impact" rows="5" name="risk_assumptions_impact" maxlength="1000" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">{{$risk != null ? $risk->risk_assumptions_impact : null }}</textarea>
              </div>
              <div class="col-span-6">
                Remarks<br>
                <textarea required id="risk_remark" rows="5" name="risk_remark" maxlength="1000" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">{{$risk != null ? $risk->risk_remark : null }}</textarea>
              </div>
              <div class="col-span-6">
                Current Risk Rating<br>
                @php
                  $risk_current_rating_text = null;
                  if($risk != null){
                    if($risk->risk_current_rating == 1){
                      $risk_current_rating_text = "Low";
                    }else if($risk->risk_current_rating == 2){
                      $risk_current_rating_text = "Medium";
                    }else if($risk->risk_current_rating == 3){
                      $risk_current_rating_text = "High";
                    }else if($risk->risk_current_rating == 4){
                      $risk_current_rating_text = "Very High";
                    }
                  }
                @endphp
                <input id="risk_current_rating" name="risk_current_rating" type="hidden" value="{{$risk != null ? $risk->risk_current_rating : null }}" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                <input required id="risk_current_rating_text" name="" type="text" value="{{$risk_current_rating_text != null ? $risk_current_rating_text : null }}" disabled class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
              </div>
           </div>
         </div>
       </div>
       <div class="flex ">
        <div class="w-full pt-4 grid grid-cols-2 rows-1">
          {{-- <p>page 1 of 6</p> &nbsp;&nbsp; --}}
          <div class="flex justify-start">
            <button id="BackButton" style="width: 200px" type="button" name="BackButton" onclick="window.location='{{url('/risk-assessment-pg2')}}'" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              Back
            </button>
          </div>
          <div class="flex justify-end">
            {{-- <button id="submitdraft" style="width: 200px" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
            Save as Draft
            </button> --}}
            {{-- <button id="submit" style="width: 200px" type="button" onclick="window.location='risk-assessment-pg5';" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              Proceed
            </button> --}}
            <button id="submit" style="width: 200px" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              Proceed
            </button>
          </div>
        </div>
      </div>
 
  {{-- <div class="flex justify-end pt-4"> --}}
 <!--    <p>page 1 of 6</p> &nbsp;&nbsp;-->
 <!--    <button id="submitdraft" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">-->
 <!--      Save as Draft-->
 <!--    </button>-->
    {{-- <button id="submit" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
      Proceed
    </button>
  </div> --}}
 
 
</form>
     </div>
   </div>
 </div>
   </div>
 </div>
 <script>
    function risk_rating_onchange(){
    //#region risk_likelihood againts risk_impact
      var risk_likelihood_rating_value = document.getElementById("risk_likelihood_rating").value;
      var risk_impact_rating_value = document.getElementById("risk_impact_rating").value;
      var risk_current_rating_value = document.getElementById("risk_current_rating");
      var risk_current_rating_text_value = document.getElementById("risk_current_rating_text");

      if(risk_likelihood_rating_value == 1){
        if(risk_impact_rating_value == 1){
          risk_current_rating_value.setAttribute('value', 1);
        }else if(risk_impact_rating_value == 2){
          risk_current_rating_value.setAttribute('value', 1);
        }else if(risk_impact_rating_value == 3){
          risk_current_rating_value.setAttribute('value', 1);
        }else if(risk_impact_rating_value == 4){
          risk_current_rating_value.setAttribute('value', 1);
        }else if(risk_impact_rating_value == 5){
          risk_current_rating_value.setAttribute('value', 2);
        }
      }else if(risk_likelihood_rating_value == 2){
        if(risk_impact_rating_value == 1){
          risk_current_rating_value.setAttribute('value', 1);
        }else if(risk_impact_rating_value == 2){
          risk_current_rating_value.setAttribute('value', 1);
        }else if(risk_impact_rating_value == 3){
          risk_current_rating_value.setAttribute('value', 1);
        }else if(risk_impact_rating_value == 4){
          risk_current_rating_value.setAttribute('value', 2);
        }else if(risk_impact_rating_value == 5){
          risk_current_rating_value.setAttribute('value', 3);
        }
      }else if(risk_likelihood_rating_value == 3){
        if(risk_impact_rating_value == 1){
          risk_current_rating_value.setAttribute('value', 1);
        }else if(risk_impact_rating_value == 2){
          risk_current_rating_value.setAttribute('value', 1);
        }else if(risk_impact_rating_value == 3){
          risk_current_rating_value.setAttribute('value', 2);
        }else if(risk_impact_rating_value == 4){
          risk_current_rating_value.setAttribute('value', 3);
        }else if(risk_impact_rating_value == 5){
          risk_current_rating_value.setAttribute('value', 3);
        }
      }else if(risk_likelihood_rating_value == 4){
        if(risk_impact_rating_value == 1){
          risk_current_rating_value.setAttribute('value', 1);
        }else if(risk_impact_rating_value == 2){
          risk_current_rating_value.setAttribute('value', 2);
        }else if(risk_impact_rating_value == 3){
          risk_current_rating_value.setAttribute('value', 3);
        }else if(risk_impact_rating_value == 4){
          risk_current_rating_value.setAttribute('value', 3);
        }else if(risk_impact_rating_value == 5){
          risk_current_rating_value.setAttribute('value', 4);
        }
      }else if(risk_likelihood_rating_value == 5){
        if(risk_impact_rating_value == 1){
          risk_current_rating_value.setAttribute('value', 2);
        }else if(risk_impact_rating_value == 2){
          risk_current_rating_value.setAttribute('value', 3);
        }else if(risk_impact_rating_value == 3){
          risk_current_rating_value.setAttribute('value', 3);
        }else if(risk_impact_rating_value == 4){
          risk_current_rating_value.setAttribute('value', 4);
        }else if(risk_impact_rating_value == 5){
          risk_current_rating_value.setAttribute('value', 4);
        }
      }

      if(risk_current_rating_value.value == 1){
        risk_current_rating_text_value.setAttribute('value', "Low");
      }else if(risk_current_rating_value.value == 2){
        risk_current_rating_text_value.setAttribute('value', "Medium");
      }else if(risk_current_rating_value.value == 3){
        risk_current_rating_text_value.setAttribute('value', "High");
      }else if(risk_current_rating_value.value == 4){
        risk_current_rating_text_value.setAttribute('value', "Very High");
      }
    //#endregion
   }
   </script>
@endsection
