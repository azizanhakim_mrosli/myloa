<style>
  table {
    border-collapse:separate;
    border:solid black 1px;
    border-radius:6px;
    -moz-border-radius:6px;
}
  </style>
@extends('layouts.app')
@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
{{-- <link rel="stylesheet" href="static/css/jquery.emailinput.min.css"> --}}
{{-- <script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script> --}}
{{-- {% with messages = get_flashed_messages(with_categories=true) %}
    {% if messages %}
        {% for category, message in messages %}
        <div class="alert alert-{{ category }}">
            {{ message }}
        </div>
        {% endfor %}
    {% endif %}
{% endwith %} --}}

<div class="flex justify">
    <div class=" pt-16 m-5 p-5" style="background-color: #F5F5F5; padding-bottom: 0px;">
        <!-- This example requires Tailwind CSS v2.0+ -->
        <nav aria-label="Progress">
          <ol class="overflow-hidden">
            <li class="relative pb-10">
              <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
              <!-- Upcoming Step -->
              <!-- Current Step -->
              <a href="#" class="relative flex items-start group" aria-current="step">
                  <span class="h-9 flex items-center" aria-hidden="true">
                  <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
                      <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
                  </span>
                  </span>
                  <span class="ml-4 min-w-0 flex flex-col">
                  <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">DRAFT APPROVAL</span>
                  </span>
              </a>
            </li>
            <li class="relative pb-10">
              <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
              <!-- Upcoming Step -->
              <a href="{{ url('/selfassessment') }}" class="relative flex items-start group">
                <span class="h-9 flex items-center" aria-hidden="true">
                  <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                      <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                  </span>
                </span>
                <span class="ml-4 min-w-0 flex flex-col">
                  <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SELF ASSESSMENT</span>
                </span>
              </a>
            </li>
          <!--    <li class="relative pb-10">-->
          <!--      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>-->
          <!--      &lt;!&ndash; Upcoming Step &ndash;&gt;-->
          <!--      <a href="#" class="relative flex items-start group">-->
          <!--        <span class="h-9 flex items-center" aria-hidden="true">-->
          <!--          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">-->
          <!--            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>-->
          <!--          </span>-->
          <!--        </span>-->
          <!--        <span class="ml-4 min-w-0 flex flex-col">-->
          <!--          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">LOA REFERENCING</span>-->
          <!--        </span>-->
          <!--      </a>-->
          <!--    </li>-->

            <li class="relative pb-10">
                <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
                <!-- Upcoming Step -->
                    <a href="risk_ass_1" class="relative flex items-start group">
                      <span class="h-9 flex items-center" aria-hidden="true">
                        <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                        </span>
                      </span>
                      <span class="ml-4 min-w-0 flex flex-col">
                        <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
                      </span>
                    </a>
            </li>
            <li class="relative pb-10">
              <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
            <!-- Upcoming Step -->
              <a href="report_submission" class="relative flex items-start group">
                  <span class="h-9 flex items-center" aria-hidden="true">
                  <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                      <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                  </span>
                  </span>
                  <span class="ml-4 min-w-0 flex flex-col">
                  <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
                  </span>
              </a>
            </li>
            <li class="relative pb-10">
              <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
              <!-- Upcoming Step -->
              <a href="#" class="relative flex items-start group">
                <span class="h-9 flex items-center" aria-hidden="true">
                  <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                    <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                  </span>
                </span>
                <span class="ml-4 min-w-0 flex flex-col">
                  <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SUMMARY OF PAPER</span>
                </span>
              </a>
            </li>
            <li class="relative">
              <!-- Upcoming Step -->
              <a href="#" class="relative flex items-start group">
                <span class="h-9 flex items-center" aria-hidden="true">
                  <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                    <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                  </span>
                </span>
                <span class="ml-4 min-w-0 flex flex-col">
                  <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">APPROVAL INITIATED</span>
                </span>
              </a>
            </li>
          </ol>
        </nav>
  </div>
  <div class="pt-6" style="background-color: #F5F5F5;">
    <div class="mx-14 mt-10 sm:mt-0" style="align-content: center">
      <div class="md:grid md:grid-cols-2 md:gap-6">
        <div class="mt-5 md:mt-0 md:col-span-2">
          <form name="create_paper_pg2_form" id="create_paper_pg2_form" class="form-horizontal" action="PaperAddContent" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="shadow overflow-hidden sm:rounded-md">
              <div class="px-4 py-5 bg-white sm:p-6">
                <label class="pb-4 block text-lg font-medium text-gray-700">Draft Approval : New Approval Paper</label>
                <div class="grid grid-cols-9 gap-9 ">
                  <div class="col-span-6 sm:col-span-3">
                    <label class="block text-sm font-medium text-gray-700">Send From</label>
                    <b>{{Auth::user()->name}}</b>
                    <p>{{Auth::user()->user_role}}</p>
                    {{-- <input id="company" name="company" type="text" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="" required> --}}
                  </div>
                  {{-- <p>
                    <div class="col-span-6">
                      <label class="block text-sm font-medium text-gray-700">Reference No</label>
                      <input id="title" name="title" type="text" placeholder="Insert Ref. No (Only if applicable)" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                    </div>
                  </p> --}}
                  <p>
                    <div class="col-span-6">
                      <label class="block text-sm font-medium text-gray-700">Subject</label>
                      <input id="objective" name="objective" type="text" value="{{$paper->paper_subject}}" disabled class="ei mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" required>
                    </div>
                  </p>
                  {{-- <div class="col-span-6 ">
                    <label  class="block text-sm font-medium text-gray-700">Body</label>
                    <textarea id="message" name="message" rows="4" placeholder="Message Body Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max" required></textarea>
                  </div> --}}
                  <div class="col-span-6 ">
                    {{-- <label  class="block text-sm font-medium text-gray-700">Importance <span style="color: red">*</span></label> --}}
                    <table style="width:100%; border:rgb(170, 170, 170) solid 1px" class="rounded-md">
                      <tr>
                        <td style="border-left: rgb(170, 170, 170) solid 1px; padding : 3px">
                          <b>Importance</b>
                          {{-- <button class="width-full btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Default</button> --}}
                        </td>
                        <td style="border-left: rgb(170, 170, 170) solid 1px; padding : 3px">
                          <b>Date</b>
                          {{-- <button class="width-full btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">High</button> --}}
                        </td>
                        <td style="border-left: rgb(170, 170, 170) solid 1px; padding : 3px">
                          <b>Reference</b>
                          {{-- <button class="width-full btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">High</button> --}}
                        </td>
                      </tr>
                      <tr style=" text-align:center">
                        <td  style="height:75px;border-left: rgb(170, 170, 170) solid 1px">
                          {{$paper->paper_importance}}
                        </td>
                        <td style="border-left: rgb(170, 170, 170) solid 1px">
                          {{$paper->paper_app_date}}
                        </td>
                        <td style="border-left: rgb(170, 170, 170) solid 1px">
                          {{$paper->paper_ref_number}}
                        </td>
                      </tr>
                    </table>
                    <input id="input-importance" type="hidden" name="importance" value="" required>
                    <input type="hidden" name="paper_id" value="{{$paper->id}}" required>
                    {{-- <textarea id="message" name="message" rows="4" placeholder="Message Body Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max" required></textarea> --}}
                  </div>
                  <p>
                    <div class="col-span-6">
                      <button value="Content" type="button" onClick="editModalShow2()" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Approval Content
                        </button>
                      {{-- <label class="block text-sm font-medium text-gray-700">Subject</label> --}}
                      {{-- <input id="objective" name="objective" type="text" value="{{$subject}}" disabled class="ei mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" required> --}}
                    </div>
                  </p>
                  {{-- <div class="col-span-6 sm:col-span-3">
                    <label  class="block text-sm font-medium text-gray-700">Attach your file(s) here</label>
                    <div class="max-w-lg flex justify-center px-9 pt-5 pb-3 border-2 border-gray-300 border-dashed rounded-md">
                      <div class="space-y-1 text-center">
                        <svg class="mx-auto h-12 w-12 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                          <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                        <div class="flex text-sm text-gray-600">
                        <input id="attachment" name="files" type="file" multiple="true">
                        </div>
                      </div>
                    </div>
                  </div> --}}
                </div>
              </div>
            </div>
            <div class="flex justify-end pt-4">
              <p>page 1 of 6</p> &nbsp;&nbsp;
              <button id="submitdraft" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Save as Draft
              </button>
              <button id="submit" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              Proceed
              </button>
            </div>
        </form>
      </div>
      <div id="editModal12" class="modal" style="border: none; padding-top: 100px; background-color:rgba(56, 56, 56, 0.08)">
        <div class="row justify-content-center" id="editModal22" >
          <div class="col-md-8 row justify-content-center" id="editModal32" style="border: none; text-align:center" >
            <div class="card" id="editCard2" style="border: none; width: 100%">
              <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('Approval Paper Content') }}</b>
              </div>
              <div class="card-body justify-content-center">
                <textarea form="create_paper_pg2_form" id="w3review" name="paper_content" rows="10" cols="100">{{$paper->paper_content != null ? $paper->paper_content : ""}}</textarea>
                <br>
                <br>
                <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                  {{-- <input type="hidden" name="adminReg" value="1"> --}}
                  {{-- <button id="submitButton1" type="submit" class="button1" style="width: 100" onclick="datetimePickerValidate()">
                      {{ __('Submit') }}
                  </button> --}}
                  {{-- &nbsp &nbsp --}}
                  <button type="button" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="width: 100" onclick="eventCloseEditFunction2()">
                      {{ __('Done') }}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<script>
  var editModal12 = document.getElementById("editModal12");
        var editModal22 = document.getElementById("editModal22");
        var editModal32 = document.getElementById("editModal32");
        var editCard2 = document.getElementById("editCard2");
    
        function eventCloseEditFunction2() {
            const body = document.body;
            body.style.overflowY = '';
            editModal12.style.display = "none";
        };
    
        function editModalShow2() {
            editModal12.style.display = "block";
            editModal12.style.overflowY = "";
            const body = document.body;
            body.style.overflowY = 'hidden';
        };
    
        document.addEventListener("click", function(e)
        {
            if ((e.target==editModal12 || e.target==editModal22 || e.target==editModal32)) 
            {
                eventCloseEditFunction2();
            }
        });
</script>
@endsection
