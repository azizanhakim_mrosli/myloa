<style>
  table {
    border-collapse:separate;
    border:solid black 1px;
    border-radius:6px;
    -moz-border-radius:6px;
}
  </style>
@extends('layouts.app')
@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" href="static/css/jquery.emailinput.min.css">
<script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script>


<div class="flex justify">
  <div class=" pt-16 m-5 p-5" style="background-color: #F5F5F5; padding-bottom: 0px;">
   <!-- This example requires Tailwind CSS v2.0+ -->
   <nav aria-label="Progress">
    <ol class="overflow-hidden">
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <!-- Current Step -->
        <a href="#" class="relative flex items-start group" aria-current="step">
          <span class="h-9 flex items-center">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-green-500 rounded-full group-hover:bg-green-700">
              <!-- Heroicon name: solid/check -->
              <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
            </span>
          </span>
            <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">DRAFT APPROVAL</span>
            </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <!-- Current Step -->
        <a href="#" class="relative flex items-start group" aria-current="step">
          <span class="h-9 flex items-center">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-green-500 rounded-full group-hover:bg-green-700">
              <!-- Heroicon name: solid/check -->
              <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
            </span>
          </span>
            <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">SELF ASSESSMENT</span>
            </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <a href="{{ url('/selfassessment') }}" class="relative flex items-start group">
          <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
              <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
          </span>
          </span>
          <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
          </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
      <!-- Upcoming Step -->
        <a href="report_submission" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
            </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
            </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <a href="#" class="relative flex items-start group">
          <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
              <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
            </span>
          </span>
          <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SUMMARY OF PAPER</span>
          </span>
        </a>
      </li>
      <li class="relative">
        <!-- Upcoming Step -->
        <a href="#" class="relative flex items-start group">
          <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
              <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
            </span>
          </span>
          <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">APPROVAL INITIATED</span>
          </span>
        </a>
      </li>
    </ol>
  </nav>
  </div>
  <div class="pt-6 h-screen " style="background-color: #F5F5F5;">
    <div class="mx-14 mt-10 sm:mt-0">
   <div class="md:grid md:grid-cols-2 md:gap-6">
     <div class="mt-5 md:mt-0 md:col-span-2">
       <div class="shadow overflow-hidden sm:rounded-md">
         <div class="px-4 py-5 bg-white sm:p-6">
           <label class="pb-4 block text-lg font-medium text-gray-700">Risk Assessment Summary</label>
           <div class="grid grid-cols-12 gap-12">
              <div class="col-span-12">
                <table class="min-w-full divide-y divide-gray-200" style="border-radius: 25px">
                  <thead class="bg-gray-50">
                    <tr>
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        No
                      </th>
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Risk Category
                      </th>
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Risk
                      </th>
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Current Risk Rating
                      </th>
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Targeted Risk Rating
                      </th>
                      <th scope="col" colspan="2" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Action
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                  @if ($risks->count() == 0)
                    <tr>
                      <td colspan="7" class=" py-2 whitespace-nowrap text-sm font-medium text-gray-900" style="text-align:center" >No risk yet</td>
                    </tr>
                  @else
                    @php
                        $counter = 1;
                    @endphp
                    @foreach ($risks as $risk)
                      <tr>
                        <td class=" py-2 whitespace-nowrap text-sm font-medium text-gray-900" style="text-align:center">{{$counter}}</td>
                        <td class=" py-2 whitespace-nowrap text-sm font-medium text-gray-900" style="text-align:center">{{$risk->riskcategory->riskcategory_name}}</td>
                        <td class=" py-2 whitespace-nowrap text-sm font-medium text-gray-900" style="text-align:center">{{$risk->risk_title}}</td>
                        <td class=" py-2 whitespace-nowrap text-sm font-medium text-gray-900" style="text-align:center">
                          @php
                            $risk_current_rating_text = null;
                            if($risk->risk_current_rating == 1){
                              $risk_current_rating_text = "Remote";
                            }else if($risk->risk_current_rating == 2){
                              $risk_current_rating_text = "Unlikely";
                            }else if($risk->risk_current_rating == 3){
                              $risk_current_rating_text = "Possible";
                            }else if($risk->risk_current_rating == 4){
                              $risk_current_rating_text = "Likely";
                            }else if($risk->risk_current_rating == 5){
                              $risk_current_rating_text = "Almost Certain";
                            }
                          @endphp
                          {{$risk_current_rating_text}}
                        </td>
                        <td class=" py-2 whitespace-nowrap text-sm font-medium text-gray-900" style="text-align:center">
                          @php
                            $risk_target_rating_text = null;
                            if($risk->risk_target_rating == 1){
                              $risk_target_rating_text = "Low";
                            }else if($risk->risk_target_rating == 2){
                              $risk_target_rating_text = "Medium";
                            }else if($risk->risk_target_rating == 3){
                              $risk_target_rating_text = "High";
                            }else if($risk->risk_target_rating == 4){
                              $risk_target_rating_text = "Very High";
                            }
                          @endphp
                          {{$risk_target_rating_text}}
                        </td>{{-- later change to target risk rating --}}
                        <td class=" py-2 whitespace-nowrap text-sm font-medium text-gray-900" style="text-align:center">
                          <form action="risk-assessment-pg3" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="risk_id" value="{{$risk->id}}">
                            <button type="submit" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                              Edit
                            </button>
                          </form>
                        </td>
                        <td class=" py-2 whitespace-nowrap text-sm font-medium text-gray-900" style="text-align:center">
                          <button type="button" onclick="window.location='PaperDeleteRisk/{{$risk->id}}'" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Remove
                          </button>
                        </td>
                      </tr>
                      @php
                        $counter++
                      @endphp
                    @endforeach
                  @endif
                  </tbody>
                </table>
                <div class="flex justify-end pt-4">
                  <button type="button" onclick="window.location='risk-assessment-pg3'" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Add Risk
                  </button>
                </div>
             </div>
           </div>
         </div>
       </div>
       <div class="flex ">
        <div class="w-full pt-4 grid grid-cols-2 rows-1">
          {{-- <p>page 1 of 6</p> &nbsp;&nbsp; --}}
          <div class="flex justify-start">
            <button id="BackButton" style="width: 200px" type="button" name="BackButton" onclick="window.location='risk-assessment-pg1'" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              Back
            </button>
          </div>
          <div class="flex justify-end">
            {{-- <button id="submitdraft" style="width: 200px" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
            Save as Draft
            </button> --}}
            <button id="submit" style="width: 200px" type="button" onclick="window.location='risk-assessment-pg5';" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              Proceed
            </button>
          </div>
        </div>
      </div>
      {{-- <div class="flex justify-end pt-4"> --}}
        <!--    <p>page 1 of 6</p> &nbsp;&nbsp;-->
        <!--    <button id="submitdraft" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">-->
        <!--      Save as Draft-->
        <!--    </button>-->
        {{-- <button id="submit" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
          Proceed
        </button> --}}
        {{-- <button type="button" onclick="window.location='risk-assessment-pg5';" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
          Proceed
        </button>
      </div> --}}
 <!--</form>-->
     </div>
   </div>
 </div>
   </div>
 </div>
@endsection
