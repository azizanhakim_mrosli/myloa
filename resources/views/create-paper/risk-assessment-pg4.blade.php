<style>
  table {
    border-collapse:separate;
    border:solid black 1px;
    border-radius:6px;
    -moz-border-radius:6px;
}
  </style>
@extends('layouts.app')
@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" href="static/css/jquery.emailinput.min.css">
<script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script>


<div class="flex justify">
  <div class=" pt-16 m-5 p-5" style="background-color: #F5F5F5; padding-bottom: 0px;">
   <!-- This example requires Tailwind CSS v2.0+ -->
   <nav aria-label="Progress">
    <ol class="overflow-hidden">
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <!-- Current Step -->
        <a href="#" class="relative flex items-start group" aria-current="step">
          <span class="h-9 flex items-center">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-green-500 rounded-full group-hover:bg-green-700">
              <!-- Heroicon name: solid/check -->
              <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
            </span>
          </span>
            <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">DRAFT APPROVAL</span>
            </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <!-- Current Step -->
        <a href="#" class="relative flex items-start group" aria-current="step">
          <span class="h-9 flex items-center">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-green-500 rounded-full group-hover:bg-green-700">
              <!-- Heroicon name: solid/check -->
              <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
              </svg>
            </span>
          </span>
            <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">SELF ASSESSMENT</span>
            </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <a href="{{ url('/selfassessment') }}" class="relative flex items-start group">
          <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
              <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
          </span>
          </span>
          <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
          </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
      <!-- Upcoming Step -->
        <a href="report_submission" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
            </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
            </span>
        </a>
      </li>
      <li class="relative pb-10">
        <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
        <a href="#" class="relative flex items-start group">
          <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
              <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
            </span>
          </span>
          <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SUMMARY OF PAPER</span>
          </span>
        </a>
      </li>
      <li class="relative">
        <!-- Upcoming Step -->
        <a href="#" class="relative flex items-start group">
          <span class="h-9 flex items-center" aria-hidden="true">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
              <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
            </span>
          </span>
          <span class="ml-4 min-w-0 flex flex-col">
            <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">APPROVAL INITIATED</span>
          </span>
        </a>
      </li>
    </ol>
  </nav>
  </div>
  <div class="pt-6 h-screen " style="background-color: #F5F5F5; width:100%;">
    <div class="mx-14 mt-10 sm:mt-0" >
      <div class="md:grid md:grid-cols-2 md:gap-6">
        <div class="mt-5 md:mt-0 md:col-span-2">
          <div class="shadow overflow-hidden sm:rounded-md">
            <div class="px-4 py-5 bg-white sm:p-6">
              <label class="pb-4 block text-lg font-medium text-gray-700">Risk Assessment : Risk Treatment</label>
              <div class="grid grid-cols-12 grid-rows-8 gap-12">
                <div class="col-span-8">
                  Risk Title<br>
                  <input disabled id="risk_title" name="risk_title" type="text" value="{{$risk->risk_title}}" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                </div>
                <div class="col-span-4">
                  Risk Category<br>
                  <input disabled id="risk_category" name="risk_category" type="text" value="{{$risk->riskcategory->riskcategory_name}}" title="{{$risk->riskcategory->riskcategory_definition}}" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                </div>
                <div class="col-span-6">
                  Causes<br>
                  <textarea disabled id="risk_causes" rows="5" name="risk_causes" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">{{$risk->risk_causes}}</textarea>
                </div>
                <div class="col-span-6">
                  Potential Impact<br>
                  <textarea disabled id="risk_potential_impact" rows="5" name="risk_potential_impact" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">{{$risk->risk_potential_impact}}</textarea>
                </div>
                <div class="col-span-8 row-span-6">
                  <table style="width: 100%">
                    <tr>
                      <td style="text-align: center">
                        No
                      </td>
                      <td style="text-align: center">
                        Mitigation
                      </td>
                      <td style="text-align: center">
                        Owner
                      </td>
                      <td style="text-align: center">
                        Action
                      </td>
                    </tr>
                    @if ($risk->mitigations->count() == 0)
                    <tr>
                      <td colspan="4" style="text-align: center">
                        No mitigation yet
                      </td>
                    </tr>
                    @else
                      @php
                          $counter = 1;
                      @endphp
                      @foreach ($risk->mitigations as $mitigation)
                        <tr>
                          <td style="text-align: center">
                            {{$counter}}
                          </td>
                          <td style="text-align: center">
                            {{$mitigation->mitigation_detail}}
                          </td>
                          <td style="text-align: center">
                            {{$mitigation->user->email}}
                          </td>
                          <td style="text-align: center">
                            <button type="button" onclick="AddMitigationModalShow('{{$mitigation->mitigation_detail}}', '{{$mitigation->user->email}}','{{$mitigation->id}}')" style="width: 100px" class="btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                              Edit
                            </button>
                            <button type="button" onclick="window.location='{{$risk->id}}/PaperRemoveMitigation/{{$mitigation->id}}'" style="width: 100px" class="btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                              Remove
                            </button>
                          </td>
                        </tr>
                        @php
                            $counter++;
                        @endphp
                      @endforeach
                    @endif
                  </table>
                  <div class="flex justify-end pt-4">
                    <button type="button" onclick="AddMitigationModalShow(null, null, null)" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                      Add Mitigation
                    </button>
                  </div>
                </div>
                <div class="col-span-4">
                  Current Likelihood Rating<br>
                  @php
                      $risk_likelihood_rating_text = null;
                      if($risk->risk_likelihood_rating == 1){
                        $risk_likelihood_rating_text = "Remote";
                      }else if($risk->risk_likelihood_rating == 2){
                        $risk_likelihood_rating_text = "Unlikely";
                      }else if($risk->risk_likelihood_rating == 3){
                        $risk_likelihood_rating_text = "Possible";
                      }else if($risk->risk_likelihood_rating == 4){
                        $risk_likelihood_rating_text = "Likely";
                      }else if($risk->risk_likelihood_rating == 5){
                        $risk_likelihood_rating_text = "Almost Certain";
                      }
                    @endphp
                  <input disabled id="risk_category" name="risk_category" type="text" value="{{$risk_likelihood_rating_text}}" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                </div>
                <div class="col-span-4">
                  Current Impact Rating<br>
                  @php
                      $risk_impact_rating_text = null;
                      if($risk->risk_impact_rating == 1){
                        $risk_impact_rating_text = "Insignificant";
                      }else if($risk->risk_impact_rating == 2){
                        $risk_impact_rating_text = "Minor";
                      }else if($risk->risk_impact_rating == 3){
                        $risk_impact_rating_text = "Moderate Impact";
                      }else if($risk->risk_impact_rating == 4){
                        $risk_impact_rating_text = "Major";
                      }else if($risk->risk_impact_rating == 5){
                        $risk_impact_rating_text = "Severe";
                      }
                    @endphp
                  <input disabled id="risk_category" name="risk_category" type="text" value="{{$risk_impact_rating_text}}" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                </div>
                <div class="col-span-4">
                  Current Risk Rating<br>
                  @php
                      $risk_current_rating_text = null;
                      if($risk->risk_current_rating == 1){
                        $risk_current_rating_text = "Low";
                      }else if($risk->risk_current_rating == 2){
                        $risk_current_rating_text = "Medium";
                      }else if($risk->risk_current_rating == 3){
                        $risk_current_rating_text = "High";
                      }else if($risk->risk_current_rating == 4){
                        $risk_current_rating_text = "Very High";
                      }
                    @endphp
                  <input disabled id="risk_category" name="risk_category" type="text" value="{{$risk_current_rating_text}}" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                </div>
                  <div class="col-span-4">
                    <form name="PaperAddRiskTreatment" id="PaperAddRiskTreatment" action="../PaperAddRiskTreatment" class="form-horizontal" method="POST" enctype="multipart/form-data">
                      @csrf
                      <input type="hidden" name="risk_id" value="{{$risk->id}}">
                    </form>
                    Target Likelihood Rating<br>
                    <select form="PaperAddRiskTreatment" id="risk_target_likehood_rating" name="risk_target_likehood_rating" onchange="risk_target_rating_onchange()" class="input-medium mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"required>
                      <option hidden value="">Select one</option>
                      <option {{$risk->risk_target_likehood_rating == 1 ? "selected" : ''}} value="1">Remote</option>
                      <option {{$risk->risk_target_likehood_rating == 2 ? "selected" : ''}} value="2">Unlikely</option>
                      <option {{$risk->risk_target_likehood_rating == 3 ? "selected" : ''}} value="3">Possible</option>
                      <option {{$risk->risk_target_likehood_rating == 4 ? "selected" : ''}} value="4">Likely</option>
                      <option {{$risk->risk_target_likehood_rating == 5 ? "selected" : ''}} value="5">Almost certains</option>
                    </select>
                  </div>
                  <div class="col-span-4">
                    Target Impact Rating<br>
                    <select form="PaperAddRiskTreatment" id="risk_target_impact_rating" name="risk_target_impact_rating" onchange="risk_target_rating_onchange()" class="input-medium mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"required>
                      <option hidden value="">Select one</option>
                      <option {{$risk->risk_target_impact_rating == 1 ? "selected" : ''}} value="1">Inignificant</option>
                      <option {{$risk->risk_target_impact_rating == 2 ? "selected" : ''}} value="2">Minor</option>
                      <option {{$risk->risk_target_impact_rating == 3 ? "selected" : ''}} value="3">Moderate impact</option>
                      <option {{$risk->risk_target_impact_rating == 4 ? "selected" : ''}} value="4">Major</option>
                      <option {{$risk->risk_target_impact_rating == 5 ? "selected" : ''}} value="5">Severe</option>
                    </select>
                  </div>
                  <div class="col-span-4">
                    Target Risk Rating<br>
                    @php
                      $risk_target_rating_text = null;
                      if($risk->risk_target_rating == 1){
                        $risk_target_rating_text = "Low";
                      }else if($risk->risk_target_rating == 2){
                        $risk_target_rating_text = "Medium";
                      }else if($risk->risk_target_rating == 3){
                        $risk_target_rating_text = "High";
                      }else if($risk->risk_target_rating == 4){
                        $risk_target_rating_text = "Very High";
                      }
                    @endphp
                    <input form="PaperAddRiskTreatment" id="risk_target_rating" name="risk_target_rating" type="hidden" value="{{$risk->risk_target_rating}}" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                    <input id="risk_target_rating_text" name="" type="text" value="{{$risk != null ? $risk_target_rating_text : null }}" disabled class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                  </div>
              </div>
            </div>
          </div>
          <div class="flex ">
            <div class="w-full pt-4 grid grid-cols-2 rows-1">
              {{-- <p>page 1 of 6</p> &nbsp;&nbsp; --}}
              <div class="flex justify-start">
                <button id="BackButton" style="width: 200px" type="button" name="BackButton" onclick="window.location='{{url('/risk-assessment-pg3/'.$risk->id.'')}}'" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                  Back
                </button>
              </div>
              <div class="flex justify-end">
                {{-- <button id="submitdraft" style="width: 200px" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Save as Draft
                </button> --}}
                {{-- <button id="submit" style="width: 200px" type="button" onclick="window.location='risk-assessment-pg5';" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                  Proceed
                </button> --}}
                <button type="submit" style="width: 200px" form="PaperAddRiskTreatment" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                  Submit
                </button>
                {{-- <button id="submit" style="width: 200px" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                  Proceed
                </button> --}}
              </div>
            </div>
          </div>
          {{-- <div class="flex justify-end pt-4"> --}}
            <!--    <p>page 1 of 6</p> &nbsp;&nbsp;-->
            <!--    <button id="submitdraft" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">-->
            <!--      Save as Draft-->
            <!--    </button>-->
            {{-- <button type="submit" form="PaperAddRiskTreatment" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              Submit
            </button>
          </div> --}}
        </div>
        <div id="AddMitigationModal1" class="modal" style="border: none; padding-top: 100px; background-color:rgba(56, 56, 56, 0.08)">
          <div class="row justify-content-center" id="AddMitigationModal2" >
            <div class="col-md-8 row justify-content-center" id="AddMitigationModal3" style="border: none; text-align:center" >
              <div class="card" id="editCard2" style="border: none; width: 100%">
                <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('New Mitigation') }}</b>
                </div>
                <form name="PaperAddMitigation" id="PaperAddMitigation" action="{{$risk->id}}/PaperAddMitigation" class="form-horizontal" method="POST" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="risk_id" value="{{$risk->id}}">
                  <input type="hidden" name="mitigation_id" value="" id="mitigation_id">
                  <div class="card-body justify-content-center">
                    <p>Mitigation</p>
                    <textarea style="width: 100%;" id="mitigation_detail" name="mitigation_detail" rows="10" cols="100" maxlength="1000" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"></textarea>
                    <br>
                    <br>
                    Owner<br>
                    <input id="mitigation_owner" name="mitigation_owner" type="text" placeholder="" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                    <br>
                    <br>
                    <div class="justify-content-center" style="text-align: center;">
                      {{-- <input type="hidden" name="adminReg" value="1"> --}}
                      {{-- <button id="submitButton1" type="submit" class="button1" style="width: 100" onclick="datetimePickerValidate()">
                          {{ __('Submit') }}
                      </button> --}}
                      {{-- &nbsp &nbsp --}}
                      <button form="PaperAddMitigation" type="submit" id="PaperAddMitigationSubmitButton" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                          {{ __('Add to mitigation list') }}
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  var AddMitigationModal1 = document.getElementById("AddMitigationModal1");
  var AddMitigationModal2 = document.getElementById("AddMitigationModal2");
  var AddMitigationModal3 = document.getElementById("AddMitigationModal3");
  function eventCloseEditFunction2() {
      const body = document.body;
      body.style.overflowY = '';
      AddMitigationModal1.style.display = "none";
  };
  
  function AddMitigationModalShow(mitigation_detail, mitigation_owner, mitigation_id) {
    AddMitigationModal1.style.display = "block";
    AddMitigationModal1.style.overflowY = "";
    const body = document.body;
    body.style.overflowY = 'hidden';

    if(mitigation_owner != null && mitigation_detail != null && mitigation_id != null){
      document.getElementById("mitigation_detail").innerText = mitigation_detail;
      document.getElementById("mitigation_owner").value = mitigation_owner;
      document.getElementById("mitigation_id").value = mitigation_id;
      document.getElementById("PaperAddMitigationSubmitButton").innerText = "Save Changes";
    }else{
      document.getElementById("mitigation_detail").innerText = null;
      document.getElementById("mitigation_owner").value = null;
      document.getElementById("mitigation_id").value = null;
      document.getElementById("PaperAddMitigationSubmitButton").innerText = "Add to mitigation list";
    }
  };
  document.addEventListener("click", function(e)
  {
      if ((e.target==AddMitigationModal1 || e.target==AddMitigationModal2 || e.target==AddMitigationModal3)) 
      {
          eventCloseEditFunction2();
      }
  });

  function risk_target_rating_onchange(){
    //#region risk_likelihood againts risk_impact
      var risk_target_likehood_rating_value = document.getElementById("risk_target_likehood_rating").value;
      var risk_target_impact_rating_value = document.getElementById("risk_target_impact_rating").value;
      var risk_target_rating_value = document.getElementById("risk_target_rating");
      var risk_target_rating_value_text = document.getElementById("risk_target_rating_text");

      if(risk_target_likehood_rating_value == 1){
        if(risk_target_impact_rating_value == 1){
          risk_target_rating_value.setAttribute('value', 1);
        }else if(risk_target_impact_rating_value == 2){
          risk_target_rating_value.setAttribute('value', 1);
        }else if(risk_target_impact_rating_value == 3){
          risk_target_rating_value.setAttribute('value', 1);
        }else if(risk_target_impact_rating_value == 4){
          risk_target_rating_value.setAttribute('value', 1);
        }else if(risk_target_impact_rating_value == 5){
          risk_target_rating_value.setAttribute('value', 2);
        }
      }else if(risk_target_likehood_rating_value == 2){
        if(risk_target_impact_rating_value == 1){
          risk_target_rating_value.setAttribute('value', 1);
        }else if(risk_target_impact_rating_value == 2){
          risk_target_rating_value.setAttribute('value', 1);
        }else if(risk_target_impact_rating_value == 3){
          risk_target_rating_value.setAttribute('value', 1);
        }else if(risk_target_impact_rating_value == 4){
          risk_target_rating_value.setAttribute('value', 2);
        }else if(risk_target_impact_rating_value == 5){
          risk_target_rating_value.setAttribute('value', 3);
        }
      }else if(risk_target_likehood_rating_value == 3){
        if(risk_target_impact_rating_value == 1){
          risk_target_rating_value.setAttribute('value', 1);
        }else if(risk_target_impact_rating_value == 2){
          risk_target_rating_value.setAttribute('value', 1);
        }else if(risk_target_impact_rating_value == 3){
          risk_target_rating_value.setAttribute('value', 2);
        }else if(risk_target_impact_rating_value == 4){
          risk_target_rating_value.setAttribute('value', 3);
        }else if(risk_target_impact_rating_value == 5){
          risk_target_rating_value.setAttribute('value', 3);
        }
      }else if(risk_target_likehood_rating_value == 4){
        if(risk_target_impact_rating_value == 1){
          risk_target_rating_value.setAttribute('value', 1);
        }else if(risk_target_impact_rating_value == 2){
          risk_target_rating_value.setAttribute('value', 2);
        }else if(risk_target_impact_rating_value == 3){
          risk_target_rating_value.setAttribute('value', 3);
        }else if(risk_target_impact_rating_value == 4){
          risk_target_rating_value.setAttribute('value', 3);
        }else if(risk_target_impact_rating_value == 5){
          risk_target_rating_value.setAttribute('value', 4);
        }
      }else if(risk_target_likehood_rating_value == 5){
        if(risk_target_impact_rating_value == 1){
          risk_target_rating_value.setAttribute('value', 2);
        }else if(risk_target_impact_rating_value == 2){
          risk_target_rating_value.setAttribute('value', 3);
        }else if(risk_target_impact_rating_value == 3){
          risk_target_rating_value.setAttribute('value', 3);
        }else if(risk_target_impact_rating_value == 4){
          risk_target_rating_value.setAttribute('value', 4);
        }else if(risk_target_impact_rating_value == 5){
          risk_target_rating_value.setAttribute('value', 4);
        }
      }

      if(risk_target_rating_value.value == 1){
        risk_target_rating_value_text.setAttribute('value', "Low");
      }else if(risk_target_rating_value.value == 2){
        risk_target_rating_value_text.setAttribute('value', "Medium");
      }else if(risk_target_rating_value.value == 3){
        risk_target_rating_value_text.setAttribute('value', "High");
      }else if(risk_target_rating_value.value == 4){
        risk_target_rating_value_text.setAttribute('value', "Very High");
      }
    //#endregion
  }
</script>
@endsection
