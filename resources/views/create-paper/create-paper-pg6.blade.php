@extends('layouts.app')
@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" href="static/css/jquery.emailinput.min.css">
<script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script>


<div class="flex justify">
  <div class=" pt-16 m-5 p-5" style="background-color: #F5F5F5; padding-bottom: 0px;">
   <!-- This example requires Tailwind CSS v2.0+ -->
<nav aria-label="Progress">
  <ol class="overflow-hidden">
      <li class="relative pb-10">
      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
      <!-- Upcoming Step -->
       <!-- Current Step -->
      <a href="" class="relative flex items-start group" aria-current="step">
        <span class="h-9 flex items-center" aria-hidden="true">
          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
            <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
          </span>
        </span>
        <span class="ml-4 min-w-0 flex flex-col">
          <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">SELF ASSESSMENT</span>
        </span>
      </a>
    </li>


     <li class="relative pb-10">
      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
      <!-- Upcoming Step -->
      <a href="#" class="relative flex items-start group">
        <span class="h-9 flex items-center" aria-hidden="true">
          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
          </span>
        </span>
        <span class="ml-4 min-w-0 flex flex-col">
          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
        </span>
      </a>
    </li>
    <li class="relative pb-10">
      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
      <!-- Upcoming Step -->
      <a href="report_submission'" class="relative flex items-start group">
        <span class="h-9 flex items-center" aria-hidden="true">
          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
          </span>
        </span>
        <span class="ml-4 min-w-0 flex flex-col">
          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
        </span>
      </a>
    </li>
    <li class="relative pb-10">
      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
      <!-- Upcoming Step -->
      <a href="#" class="relative flex items-start group">
        <span class="h-9 flex items-center" aria-hidden="true">
          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
          </span>
        </span>
        <span class="ml-4 min-w-0 flex flex-col">
          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Summary of Paper</span>
        </span>
      </a>
    </li>
    <li class="relative">
      <!-- Upcoming Step -->
      <a href="#" class="relative flex items-start group">
        <span class="h-9 flex items-center" aria-hidden="true">
          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
          </span>
        </span>
        <span class="ml-4 min-w-0 flex flex-col">
          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Approval initiated</span>
        </span>
      </a>
    </li>
  </ol>
</nav>
  </div>
  <div class="pt-6 h-screen " style="background-color: #F5F5F5;">
   <div class="mx-14 mt-10 sm:mt-0">
  <div class="md:grid md:grid-cols-2 md:gap-6">
    <div class="mt-5 md:mt-0 md:col-span-2">
<!--      <form class="form-horizontal" action="risk_ass_1" method="POST" enctype="multipart/form-data">-->

      <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="shadow overflow-hidden sm:rounded-md">

          <div class="px-4 py-5 bg-white sm:p-6">
            <label class="pb-4 block text-lg font-medium text-gray-700">Self Assessment Summary</label>
              <table class="min-w-full divide-y divide-gray-200" id="myTable">

          <thead class="bg-gray-50">
            <tr>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Paper AODM
              </th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Paper Threshold Value
              </th>
              <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Consultation By
              </th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Endorsement By
              </th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                Approval By
              </th>
            </tr>
          </thead>

            <body class="bg-white divide-y divide-gray-200" >
            {{-- {% for summary in summary %} --}}

              <tr>
                <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                  {{-- {{summary.project}} --}}
                </td>
                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                  {{-- {{summary.threshold}} --}}
                </td>
                  <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                  -
                </td>
                  <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                  {{-- {{summary.consultation_by}} --}}
                </td>
                <td class="px-6 py-4 whitespace-nowrap text-centre text-sm font-medium">
                  {{-- {{summary.approval_by}} --}}
                </td>

              </tr>

              {{-- {% endfor %} --}}


            </body>

          </table>


            </div>
          </div>
  <div class="flex justify-end pt-4">
    <p>page 6 of 6</p> &nbsp;&nbsp;
    <button id="submit" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
      Proceed
    </button>
</div>
</fieldset>
    </form>
    </div>
  </div>
</div>
  </div>
</div>
@endsection
