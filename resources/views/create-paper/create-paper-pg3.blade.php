@extends('layouts.app')
@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" href="static/css/jquery.emailinput.min.css">
<script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script>
<div class="flex justify">
  <div class=" pt-16 m-5 p-5" style="background-color: #F5F5F5; padding-bottom: 0px;">
    <!-- This example requires Tailwind CSS v2.0+ -->
    <nav aria-label="Progress">
      <ol class="overflow-hidden">
          <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <!-- Current Step -->
          <a href="" class="relative flex items-start group" aria-current="step">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
                <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">SELF ASSESSMENT</span>
            </span>
          </a>
        </li>
    <!--    <li class="relative pb-10">-->
    <!--      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>-->
    <!--      &lt;!&ndash; Upcoming Step &ndash;&gt;-->
    <!--      <a href="#" class="relative flex items-start group">-->
    <!--        <span class="h-9 flex items-center" aria-hidden="true">-->
    <!--          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">-->
    <!--            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>-->
    <!--          </span>-->
    <!--        </span>-->
    <!--        <span class="ml-4 min-w-0 flex flex-col">-->
    <!--          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">LOA REFERENCING</span>-->
    <!--        </span>-->
    <!--      </a>-->
    <!--    </li>-->
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="report_submission" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Summary of Paper</span>
            </span>
          </a>
        </li>
        <li class="relative">
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Approval initiated</span>
            </span>
          </a>
        </li>
      </ol>
    </nav>
  </div>
  <div class="pt-6 h-screen w-8/12" style="background-color: #F5F5F5;">
   <div class="mx-14 mt-10 sm:mt-0">
  <div class="md:grid md:grid-cols-2 md:gap-6">
    <div class="mt-5 md:mt-0 md:col-span-2">
      <form class="form-horizontal" action="create-paper-pg4" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="shadow overflow-hidden sm:rounded-md">
          <div class="px-4 py-5 bg-white sm:p-6">
             <label class="pb-4 block text-lg font-medium text-gray-700">As per PETRONAS RADM Exclusion List issued on 29th May 2020, any of the following decision papers are excluded from carrying out
                    Risk Assessment. Please tick the relevant category for your approval paper below. If your paper does not belongs to any of these
                    categories, then please click Proceed.</label>
              {{-- {% for decision in decision %}
                {{decision.id}}.{{decision.name}} &nbsp; &nbsp;<input type="radio" name="paper" value="{{decision.name}}"><br/>
              {% endfor %} --}}
<!--            <div class="grid grid-cols-9 gap-9 ">-->
<!--              <div class="col-span-6 sm:col-span-3">-->
<!--                <label class="block text-sm font-medium text-gray-700">Company</label>-->
<!--                <input id="company" name="company" type="text" placeholder="" value="" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="" required>-->
<!--               </div>-->

<!--             <div class="col-span-6 sm:col-span-3">-->
<!--                <label for="division" class="block text-sm font-medium text-gray-700">Division</label>-->
<!--                <select id="division" name="division" class="input-medium mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"required>-->
<!--                       <option>-&#45;&#45; Select Importance -&#45;&#45;</option>-->
<!--                       <option value="Default">Default</option>-->
<!--                        <option value="High">High</option>-->
<!--                </select>-->

<!--              </div>-->

<!--              <div class="col-span-6 sm:col-span-3">-->
<!--                <label for="date" class="block text-sm font-medium text-gray-700">Date</label>-->
<!--                <input id="date" name="date" type="date" class="input-medium mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" value="" required>-->
<!--              </div>-->

<!--               <div class="col-span-6 sm:col-span-3">-->
<!--                <label class="block text-sm font-medium text-gray-700">Subject</label>-->
<!--                <input id="subject" name="subject" type="text" placeholder="" value="" class="input-xlarge mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="" required>-->
<!--               </div>-->

<!--                 <div class="col-span-6 sm:col-span-3">-->
<!--                <label class="block text-sm font-medium text-gray-700">Recipients Approver(s) </label>-->
<!--                <input id="to" name="to" class="emailinput ei mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" required>-->
<!--              </div>-->

<!--              <div class="col-span-6 sm:col-span-3">-->
<!--                <label for="duedate" class="block text-sm font-medium text-gray-700">Due Date</label>-->
<!--                <input id="duedate" name="duedate" type="date" class="input-medium mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" value="" required>-->
<!--              </div>-->


<!--              <div class="col-span-6">-->
<!--                <label class="block text-sm font-medium text-gray-700">Title </label>-->
<!--                <input id="title" name="title" class="emailinput ei mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" required>-->
<!--              </div>-->

<!--              <div class="col-span-6">-->
<!--                <label class="block text-sm font-medium text-gray-700">Objective</label>-->
<!--                <input id="objective" name="objective" class="emailinput ei mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" required>-->
<!--              </div>-->

<!--              <div class="col-span-6 ">-->
<!--                <label  class="block text-sm font-medium text-gray-700">Body</label>-->

<!--                <textarea id="message" name="message" rows="4" placeholder="Message Body Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max" required></textarea>-->
<!--              </div>-->

<!--              <div class="col-span-6 sm:col-span-3">-->
<!--                <label  class="block text-sm font-medium text-gray-700">Attach your file(s) here</label>-->
<!--            <div class="max-w-lg flex justify-center px-9 pt-5 pb-3 border-2 border-gray-300 border-dashed rounded-md">-->
<!--              <div class="space-y-1 text-center">-->
<!--                <svg class="mx-auto h-12 w-12 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">-->
<!--                  <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />-->
<!--                </svg>-->
<!--                <div class="flex text-sm text-gray-600">-->
<!--               <input id="attachment" name="files" type="file" multiple="true">-->

<!--                </div>-->
<!--              </div>-->
<!--            </div>-->
<!--          </div>-->
    </div>
  </div>
  <div class="flex justify-end pt-4">
    <p>page 3 of 6</p> &nbsp;&nbsp;
    <button id="submitdraft" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
      Save as Draft
    </button>
    <button id="submit" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
      Proceed
    </button>
</div>
</fieldset>
    </form>
    </div>
  </div>
</div>
  </div>
</div>
@endsection
