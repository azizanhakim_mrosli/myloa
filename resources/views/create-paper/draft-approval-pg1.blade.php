<style>
  table {
    border-collapse:separate;
    border:solid black 1px;
    border-radius:6px;
    -moz-border-radius:6px;
}
</style>

@extends('layouts.app')
@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
{{-- <link rel="stylesheet" href="static/css/jquery.emailinput.min.css"> --}}
{{-- <script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script> --}}
{{-- {% with messages = get_flashed_messages(with_categories=true) %}
    {% if messages %}
        {% for category, message in messages %}
        <div class="alert alert-{{ category }}">
            {{ message }}
        </div>
        {% endfor %}
    {% endif %}
{% endwith %} --}}

<div class="flex justify" style="">

  <div class=" pt-16 m-5 p-5" style="background-color: #F5F5F5; padding-bottom: 0px;">
    <!-- This example requires Tailwind CSS v2.0+ -->
    <nav aria-label="Progress">
      <ol class="overflow-hidden">
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <!-- Current Step -->
          <a href="#" class="relative flex items-start group" aria-current="step">
            <span class="h-9 flex items-center">
              
                <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
                  <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
               
        
              </span>
            </span>
            
              <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">DRAFT APPROVAL</span>
              </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="{{ url('/selfassessment') }}" class="relative flex items-start group">
            <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
              <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
          </span>
            </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SELF ASSESSMENT</span>
            </span>
          </a>
        </li>
      <!--    <li class="relative pb-10">-->
      <!--      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>-->
      <!--      &lt;!&ndash; Upcoming Step &ndash;&gt;-->
      <!--      <a href="#" class="relative flex items-start group">-->
      <!--        <span class="h-9 flex items-center" aria-hidden="true">-->
      <!--          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">-->
      <!--            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>-->
      <!--          </span>-->
      <!--        </span>-->
      <!--        <span class="ml-4 min-w-0 flex flex-col">-->
      <!--          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">LOA REFERENCING</span>-->
      <!--        </span>-->
      <!--      </a>-->
      <!--    </li>-->

        <li class="relative pb-10">
            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
            <!-- Upcoming Step -->
                <a href="risk_ass_1" class="relative flex items-start group">
                  <span class="h-9 flex items-center" aria-hidden="true">
                    <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                        <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                    </span>
                  </span>
                  <span class="ml-4 min-w-0 flex flex-col">
                    <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
                  </span>
                </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
        <!-- Upcoming Step -->
          <a href="report_submission" class="relative flex items-start group">
              <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                  <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
              </span>
              <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
              </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SUMMARY OF PAPER</span>
            </span>
          </a>
        </li>
        <li class="relative">
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">APPROVAL INITIATED</span>
            </span>
          </a>
        </li>
      </ol>
    </nav>
</div>


  
<div class="pt-6 h-screen w-8/12" style="background-color: #F5F5F5;">
    <div class="mx-14 mt-10 sm:mt-0 flex-none" >
      <div class="md:grid md:grid-cols-2 md:gap-6">
        <div class="mt-5 md:mt-0 md:col-span-2">
          <form name="create_paper_pg2_form" id="create_paper_pg2_form" class="form-horizontal" action="{{ url('CreatePaper') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="shadow overflow-auto sm:rounded-md" style="">
              <div class="px-4 py-5 bg-white sm:p-9">
                <label class="pb-4 block text-lg font-medium text-gray-700">Self Assessment</label>
                <div class="grid grid-cols-9 gap-9" >
                  <div class="col-span-9 sm:col-span-9" >
                    Please refer to the <a target="_blank" href="https://myexplorer.petronas.com/UserPortal/post/related-party-transactions-policy-and-procedure"> Related Party Transactions Policy and Procedure </a> for further information
                  </div>
                  <p>
                    <div class="col-span-9" >
                      <table style="width: 100%; border:none">
                        <tr>
                          <td style="width: 75%" colspan="2"><label class="block text-sm font-medium text-gray-700">Related Party & Transfer Pricing (TP)</label></td>
                          <td><label class="block text-sm font-medium text-gray-700">Yes</label></td>
                          <td><label class="block text-sm font-medium text-gray-700">No</label></td>
                          <td><label class="block text-sm font-medium text-gray-700">N/A</label></td>
                        </tr>
                        <tr>
                          <td>1.</td>
                          <td>Is the transacting party related to PDB?</td>
                          <td><input required type="radio" id="rpt_related_pdb" name="rpt_related_pdb" value="Yes" onclick="RadioOnClicked('Yes')" {{ $paper != null ? ($paper->rpt->rpt_related_pdb == "Yes" ? "checked" : "") : "" }}></td>
                          <td><input type="radio" id="" name="rpt_related_pdb" value="No" onclick="RadioOnClicked('No')" {{ $paper != null ? ($paper->rpt->rpt_related_pdb == "No" ? "checked" : "") : "" }}></td>
                          <td><input type="radio" id="" name="rpt_related_pdb" value="N/A" onclick="RadioOnClicked('N/A')" {{ $paper != null ? ($paper->rpt->rpt_related_pdb == "N/A" ? "checked" : "") : "" }}></td>
                        </tr>
                        <tr>
                          <td>2.</td>
                          <td>Is the agreed price within prevailing market rate and on normal commersial terms?</td>
                          <td><input required type="radio" id="" name="rpt_market_rate" value="Yes" {{ $paper != null ? ($paper->rpt->rpt_market_rate == "Yes" ? "checked" : "") : "" }}></td>
                          <td><input type="radio" id="" name="rpt_market_rate" value="No" {{ $paper != null ? ($paper->rpt->rpt_market_rate == "No" ? "checked" : "") : "" }}></td>
                          <td><input type="radio" id="" name="rpt_market_rate" value="N/A" {{ $paper != null ? ($paper->rpt->rpt_market_rate == "N/A" ? "checked" : "") : "" }}></td>
                        </tr>
                        <tr>
                          <td>3.</td>
                          <td>Are the material terms at par with the non-related party?</td>
                          <td><input required type="radio" id="" name="rpt_at_par" value="Yes" {{ $paper != null ? ($paper->rpt->rpt_at_par == "Yes" ? "checked" : "") : "" }}></td>
                          <td><input type="radio" id="" name="rpt_at_par" value="No" {{ $paper != null ? ($paper->rpt->rpt_at_par == "No" ? "checked" : "") : "" }}></td>
                          <td><input type="radio" id="" name="rpt_at_par" value="N/A" {{ $paper != null ? ($paper->rpt->rpt_at_par == "N/A" ? "checked" : "") : "" }}></td>
                        </tr>
                      </table>
                    </div>
                  </p>
                  <div id="tpr-message-parent" class="col-span-9 justify-center" style="align-content: center; min-height: 150px">
                    <div id="tpr-message" style=" border:rgb(170, 170, 170) solid 1px; margin:auto; display:none; width:80% " class="shadow rounded-md p-3">
                      <table style="border:none">
                        <tr>
                          <td class="p-1"><label class="block text-sm font-medium text-gray-700">1.</label></td>
                          <td class="p-1"><label class="block text-sm font-medium text-gray-700">Please consult Group Legal and/or the Finance Division for further information</label></td>
                        </tr>
                        <tr>
                          <td class="p-1"><label class="block text-sm font-medium text-gray-700">2.</label></td>
                          <td class="p-1"><label class="block text-sm font-medium text-gray-700">Please complete the RPT assessment (if dealing with Related Party) and attach it in the next page</label></td>
                        </tr>
                      </table>
                    </div>
                    {{-- <input id="input-importance" type="hidden" name="importance" value="" required> --}}
                    <input type="hidden" name="paper_id" value="{{$paper != null ? $paper->id : null}}" required>
                  </div>
                </div>
              </div>
            </div>
            <div class="flex justify-end pt-4">
              {{-- <p>page 1 of 6</p> &nbsp;&nbsp; --}}
              {{-- <button id="submitdraft" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Save as Draft
              </button> --}}
              <button id="submit" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              Proceed
              </button>
            </div>
          </form>
        </div>
        <div id="editModal12" class="modal" style="border: none; padding-top: 100px; background-color:rgba(56, 56, 56, 0.08)">
          <div class="row justify-content-center" id="editModal22" >
            <div class="col-md-8 row justify-content-center" id="editModal32" style="border: none; text-align:center" >
              <div class="card" id="editCard2" style="border: none; width: 100%">
                <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('Approval Paper Content') }}</b>
                </div>
                <div class="card-body justify-content-center">
                    <textarea form="create_paper_pg2_form" id="w3review" name="paper_content" rows="10" cols="100">At w3schools.com you will learn how to make a website. They offer free tutorials in all web development technologies.</textarea>
                    <br>
                    <br>
                  <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                    <button type="button" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" style="width: 100" onclick="eventCloseEditFunction2()">
                        {{ __('Cancel') }}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>

  window.onload = function(){ //run these after page loaded
    if(document.getElementById('rpt_related_pdb').checked){
      document.getElementById("tpr-message").style.display = "block";
    }         
  }
  
  function RadioOnClicked(value){
    if(value == "Yes"){
      document.getElementById("tpr-message").style.display = "block";
    }else{
      document.getElementById("tpr-message").style.display = "none";
    }
  }

  var editModal12 = document.getElementById("editModal12");
  var editModal22 = document.getElementById("editModal22");
  var editModal32 = document.getElementById("editModal32");
  var editCard2 = document.getElementById("editCard2");

  function eventCloseEditFunction2() {
      const body = document.body;
      body.style.overflowY = '';
      editModal12.style.display = "none";
  };

  function editModalShow2() {
      editModal12.style.display = "block";
      editModal12.style.overflowY = "";
      const body = document.body;
      body.style.overflowY = 'hidden';
  };

  document.addEventListener("click", function(e)
  {
      if ((e.target==editModal12 || e.target==editModal22 || e.target==editModal32)) 
      {
          eventCloseEditFunction2();
      }
  });
</script>
@endsection
