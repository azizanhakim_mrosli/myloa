<style>
  table {
    border-collapse: separate;
    border: solid black 1px;
    border-radius: 6px;
    -moz-border-radius: 6px;
  }
</style>
@extends('layouts.app')
@section('content')
<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" href="static/css/jquery.emailinput.min.css">
<script type="text/javascript" src="static/js/jquery.emailinput.min.js"></script>


<div class="flex justify">
  <div class=" pt-16 m-5 p-5" style="background-color: #F5F5F5; padding-bottom: 0px;">
    <!-- This example requires Tailwind CSS v2.0+ -->
    <nav aria-label="Progress">
      <ol class="overflow-hidden">
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <!-- Current Step -->
          <a href="#" class="relative flex items-start group" aria-current="step">
            <span class="h-9 flex items-center">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-green-500 rounded-full group-hover:bg-green-700">
                <!-- Heroicon name: solid/check -->
                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-purple-600">DRAFT APPROVAL</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="{{ url('/selfassessment') }}" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-green-500 rounded-full">
                <span class="h-2.5 w-2.5 bg-green-500 rounded-full"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SELF ASSESSMENT</span>
            </span>
          </a>
        </li>
        <!--    <li class="relative pb-10">-->
        <!--      <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>-->
        <!--      &lt;!&ndash; Upcoming Step &ndash;&gt;-->
        <!--      <a href="#" class="relative flex items-start group">-->
        <!--        <span class="h-9 flex items-center" aria-hidden="true">-->
        <!--          <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">-->
        <!--            <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>-->
        <!--          </span>-->
        <!--        </span>-->
        <!--        <span class="ml-4 min-w-0 flex flex-col">-->
        <!--          <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">LOA REFERENCING</span>-->
        <!--        </span>-->
        <!--      </a>-->
        <!--    </li>-->

        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="risk_ass_1" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">RISK ASSESSMENT</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="report_submission" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">PAPER SUBMISSION</span>
            </span>
          </a>
        </li>
        <li class="relative pb-10">
          <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">SUMMARY OF PAPER</span>
            </span>
          </a>
        </li>
        <li class="relative">
          <!-- Upcoming Step -->
          <a href="#" class="relative flex items-start group">
            <span class="h-9 flex items-center" aria-hidden="true">
              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
              </span>
            </span>
            <span class="ml-4 min-w-0 flex flex-col">
              <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">APPROVAL INITIATED</span>
            </span>
          </a>
        </li>
      </ol>
    </nav>
  </div>



  <div class="pt-6 h-screen w-8/12 " style="background-color: #F5F5F5;">
    <div class="mx-14 mt-10 sm:mt-0">
      <div class="md:grid md:grid-cols-2 md:gap-6">
        <div class="mt-5 md:mt-0 md:col-span-2">
          <div class="shadow overflow-hidden sm:rounded-md">

            <div class="px-4 py-5 bg-white sm:p-6">
              <label class="pb-4 block text-lg font-medium text-gray-700">Self Assesment : Determining Approval Authority</label>
              <label class="pb-4 block text-lg font-medium text-gray-700">Choose Area of Decision Making in PDB LOA</label>
              @foreach ($papertypes as $papertype)
              <div class="flex flex-col">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8 ">

                  <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8 ">
                    <div class="shadow border-b border-gray-200 sm:rounded-lg ">
                      <table class="min-w-full divide-y divide-gray-200 ">
                        <tbody class="bg-white divide-y divide-gray-200 border">
                          <tr>
                            <td class="px-6 py-4 w-2 whitespace-nowrap text-sm font-medium text-gray-900 " title="{{$papertype->papertype_definition}}" style="cursor: pointer;">
                              &#x1F6C8;
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500 text-left" onclick="window.location='PaperAddPaperType/{{$papertype->id}}/{{ Session::get('paper')->id}}';">
                              {{$papertype->papertype_name}}
                            </td>

                          </tr>


                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>

          </div>



          {{-- <p>
        <div style="border:black solid 1px; background-color: white; width:100%;">
          <label class="pb-4 block text-lg font-medium text-gray-700">Self Assessment : Determining Approval Authority</label>
          <div class="grid grid-cols-9 gap-9 "style="overflow-y:hidden;" >
            <div class="col-span-9 sm:col-span-9" >
              <label class="pb-4 block text-lg font-medium text-gray-700">Choose Paper Type</label>
            </div>
          </div>
        <div class="mt-10 pt-6 overflow-y-scroll overflow-x-hidden" style="border:black none 1px; max-height:80vh; width:80%">
          @foreach ($papertypes as $papertype)
          <div class="flex flex-col" >
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8 ">
                <div class=" overflow-hidden border-b border-gray-200 sm:rounded-lg ">
                  <table class="min-w-full divide-y divide-gray-200 ">
                    <tbody class="bg-white divide-y divide-gray-200 border">
                      <tr>
                        <td class="px-6 py-4 w-2 whitespace-nowrap text-sm font-medium text-gray-900 " title="{{$papertype->papertype_definition}}" style="cursor: pointer;">
          &#x1F6C8;
          </td>

          <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500 text-left" onclick="window.location='PaperAddPaperType/{{$papertype->id}}/{{ Session::get('paper')->id}}';">
            {{$papertype->papertype_name}}
          </td>
          </tr>
          </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>
</div>
</p>
{{-- <div class="pt-6 h-screen w-8/12 overflow-hidden" style="background-color: #F5F5F5;">
    <div class="mx-14 mt-10 sm:mt-0 overflow-hidden">
      <div class="md:grid md:grid-cols-2 md:gap-6 overflow-hidden">
        <div class="mt-5 md:mt-0 md:col-span-2 overflow-hidden" style="height:100%">
          <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="shadow overflow-hidden sm:rounded-md p-3" style="background: white; height:50%">
              <label class="pb-4 block text-lg font-medium text-gray-700 overflow-hidden">Self Assessment : Determining Approval Authority</label>
              <label class="pb-4 block text-lg font-medium text-gray-700 overflow-hidden">Choose Paper Type</label>
              <div class="px-4 py-5 bg-white sm:p-6 overflow-y-scroll overflow-x-hidden" style="height:50%">
                @foreach ($papertypes as $papertype)
                  <div class="flex flex-col" >
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                      <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8 ">
                        <div class=" overflow-hidden border-b border-gray-200 sm:rounded-lg ">
                          <table class="min-w-full divide-y divide-gray-200 ">
                            <tbody class="bg-white divide-y divide-gray-200 border">
                              <tr>
                                <td class="px-6 py-4 w-2 whitespace-nowrap text-sm font-medium text-gray-900 " title="{{$papertype->papertype_definition}}" style="cursor: pointer;">&#x1F6C8;
</td>
<td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500 text-left ">
  {{$papertype->papertype_name}}
</td>
<td class="px-6 py-4 w-2 whitespace-nowrap text-sm text-gray-500">
  <input type="checkbox" name="papertype_id" value="{{$papertype->id}}">
</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
@endforeach
</div>
</div>
<div class="flex justify-end pt-4">
  <p>page 4 of 6</p> &nbsp;&nbsp;
  <button id="submitdraft" type="submit" name="submit" value="drafts" class="btn btn-success bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
    Save as Draft
  </button>

  <button id="submit" type="submit" name="submit" value="send" class="btn btn-success ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
    Proceed
  </button>
</div>
</form>
</div>
</div>
</div>
</div> --}}
</div>
<script>
  function ChosePaperType(paper_type_id) {
    alert("chosen paper type id : " + paper_type_id);
  };
</script>
@endsection