@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="//cdn.materialdesignicons.com/3.7.95/css/materialdesignicons.min.css">
<link rel="stylesheet" href="https://rsms.me/inter/inter.css">

{{-- for carousel --}}
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="{{url('/css/carousel.css')}}">
<link rel="stylesheet" href="/path/to/cdn/tailwind.min.css" />

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

<link href="https://fonts.googleapis.com/css2?family=Secular+One&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Orienta&display=swap" rel="stylesheet">

<!-- Content for dashboard page -->

{{-- carousel --}}
<div class="wrapper_carousel z-10" role="region" aria-label="landing carousel" style="width: 100%;">
  <div class="carousel-data">
    <div class="carousel-item z-20" style="background-color: #d0eff3;" role="group" aria-label="slide 1 of 2">
      <div class="carousel-content-grid">
        <div>
          <h2 class="title">User Guide</h2>
          <p class="desc">
          This document provides guide to LOA users on how to navigate the myLOA system and search for information within the Company's LOA.
          </p>
          <a href="{{url('/User Guide - MyLOA.pdf')}}"><button class="read-more">Read More</button></a>
        </div>

        <img src="{{url('/images/User1.png')}}" alt="User 1" width="300" height="250">

      </div>
    </div>

    <div class="carousel-item" style="background-image: url('{{asset('images/image6.png') }}'); background-size: cover;" role="group" aria-label="slide 2 of 2">
      <div class="carousel-content-grid">
        <div>
          <h2 class="title">PDB LOA</h2>
          <p class="desc">A reporting automation for LOA-based Approvals.</p>
          <a href="https://loams.petronas.com/web/home/view"><button class="read-more">Read More</button></a>
        </div>

      </div>
    </div>
  </div>

  <div class="carousel-controls" aria-label="carousel controls">
    <button type="button" class="carousel-indicator button_carousel-prev" style="background-color: white;" aria-label="previous"></button>
    <button type="button" class="carousel-indicator button_carousel-next" style="background-color: white;" aria-label="next"></button>
  </div>
</div>
<script src="{{url('/js/carousel.js')}}"></script>
{{-- end of carousel --}}

{{-- mytracker table --}}
<div class="flex" style="width: 100%; margin:auto;">

  {{-- table for mytracker on homepage --}}
  <div class="mt-2 w-full" style="padding-left: 40px;">
    <div class="mt-4 mb-4">
      <h1 class="font-sans font-semibold text-3xl">My Tracker</h1>
    </div>
    <div class="-my-2 w-full overflow-x-auto sm:-mx-6 lg:-mx-8">
      <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
        <div class="overflow-hidden border-b border-gray-200 sm:rounded-lg">
          <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
              <tr>
                <th scope="col" class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider">
                  No
                </th>
                <th scope="col" class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Paper ID
                </th>
                <th scope="col" class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Approver
                </th>
                <th scope="col" class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Submitted Date
                </th>
                <th scope="col" class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Status
                </th>
                <th scope="col" class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider">
                  Action
                </th>
              </tr>
            </thead>
            @php
            global $counter;
            $counter = 0;
            @endphp

            <tbody class="bg-white divide-y divide-gray-200">
              @if ($papers->count() == 0) 
                <tr>
                  <td class="table-auto" colspan="6">
                    <div class="text-center font-medium text-gray-900 mt-3 mb-3">
                      You have no submitted papers
                    </div>
                  </td>
                </tr>
              @elseif ($papers->count() == 5)
                @foreach ($papers as $paper)
                  @php
                  $counter++;
                  @endphp
                  <tr>
                    <td class="px-1 py-4 whitespace-nowrap">
                      <div class="flex items-center">
                        <div class="ml-4">
                          <div class="text-sm text-center font-medium text-gray-900">
                            {{ $counter }}
                          </div>
                        </div>
                      </div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                      <div class="text-sm text-center text-gray-900">{{ $paper->id }}</div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                      <div class="text-sm text-left text-gray-900">
                        @php
                          $approverid = DB::table('recipients')->where('paper_id', $paper->id)->where('recipient_category', 'Approver')->value('user_id');
                          echo(DB::table('users')->where('id', $approverid)->value('name'))
                        @endphp
                      </div>
                      <div class="text-sm text-gray-500">
                        @php
                          $approverrole = DB::table('recipients')->where('paper_id', $paper->id)->where('recipient_category', 'Approver')->value('user_id');
                          echo(DB::table('users')->where('id', $approverrole)->value('user_role'))
                        @endphp
                      </div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-center text-sm font-medium">
                      {{ $paper->paper_app_date }}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">
                      <div id="risk_status" class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                        @php
                        $reviewer_bool = false;
                        $reviewer_pending = $paper->recipients->where('recipient_category', 'Reviewer')->where('recipient_status', 'Pending');
                        $approver_pending = $paper->recipients->where('recipient_category', 'Approver')->where('recipient_status', 'Pending');
                        if($paper->paper_status == "Risks Approved"){
                        echo($paper->paper_status);
                        }elseif ($paper->paper_status == "Wait Risk") {
                        echo("Waiting for Risk Assessment Endorsement");
                        }else {
                        echo($paper->paper_status);
                        }
                        @endphp
                      </div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">
                      <a href="#" class="text-indigo-600 hover:text-indigo-900" onclick="GetSentPaper('{{$paper->id}}')">View Details</a>
                    </td>
                  </tr>
                @endforeach
              @else
                @php
                  $count = 0;
                @endphp
                @foreach ($papers as $paper)
                  @php
                  $counter++;
                  @endphp
                  <tr>
                    <td class="px-1 py-4 whitespace-nowrap">
                      <div class="flex items-center">
                        <div class="ml-4">
                          <div class="text-sm text-center font-medium text-gray-900">
                            {{ $counter }}
                          </div>
                        </div>
                      </div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                      <div class="text-sm text-center text-gray-900">{{ $paper->id }}</div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                      <div class="text-sm text-left text-gray-900">
                        @php
                          $approverid = DB::table('recipients')->where('paper_id', $paper->id)->where('recipient_category', 'Approver')->value('user_id');
                          echo(DB::table('users')->where('id', $approverid)->value('name'))
                        @endphp
                      </div>
                      <div class="text-sm text-gray-500">
                        @php
                          $approverrole = DB::table('recipients')->where('paper_id', $paper->id)->where('recipient_category', 'Approver')->value('user_id');
                          echo(DB::table('users')->where('id', $approverrole)->value('user_role'))
                        @endphp
                      </div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-center text-sm font-medium">
                      {{ $paper->paper_app_date }}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">
                      <div id="risk_status" class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                        @php
                        $reviewer_bool = false;
                        $reviewer_pending = $paper->recipients->where('recipient_category', 'Reviewer')->where('recipient_status', 'Pending');
                        $approver_pending = $paper->recipients->where('recipient_category', 'Approver')->where('recipient_status', 'Pending');
                        if($paper->paper_status == "Risks Approved"){
                        echo($paper->paper_status);
                        }elseif ($paper->paper_status == "Wait Risk") {
                        echo("Waiting for Risk Assessment Endorsement");
                        }else {
                        echo($paper->paper_status);
                        }
                        @endphp
                      </div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">
                      <a href="#" class="text-indigo-600 hover:text-indigo-900" onclick="GetSentPaper('{{$paper->id}}')">View Details</a>
                    </td>
                  </tr>
                  @php
                    $count++;
                  @endphp
                  @if ($count == 5)
                    @break
                  @endif
                @endforeach
              @endif
            </tbody>
          </table>
        </div>
        @if ($papers->count() > 5)
          <a class="btn btn-primary mt-3 float-right" style="background-color: rgb(142, 111, 167); border: none;" href="{{ url('mytracker') }}">View More</a>
        @endif
        <h2 style="margin-top: 70px;">What would you like to do today?</h2>
        <div class="my-5 pb-5 row justify-content-center">
          <div class="col-sm-5" style="max-width: 17rem;">
            <div class="card h-40" style="background-color: rgb(142, 111, 167); border: none; border-radius: 12px 12px 0px 0px;">
              <div class="card-body">
                <h5 class="card-title text-xl text-center" style="color: white;">Create Approval Paper</h5>
                <p class="card-text text-sm text-center" style="color: white;">Create, access and submit your approval papers here.</p>
              </div>
            </div>
            <a href="{{ url('draft-approval-pg1') }}" class="btn btn-primary w-full" style="background-color: rgb(215, 187, 237); border: none; border-radius: 0px 0px 12px 12px; color: black;">></a>
          </div>
          <div class="col-sm-2">

          </div>
          <div class="col-sm-5" style="max-width: 17rem;">
            <div class="card h-40" style="background-color: rgb(142, 111, 167); border: none; border-radius: 12px 12px 0px 0px;">
              <div class="card-body">
                <h5 class="card-title text-xl text-center" style="color: white;">Approval Requests</h5>
                <p class="card-text text-sm text-center" style="color: white;">View, edit and respond to your submissions and approvals in MyLOA</p>
              </div>
            </div>
            <a href="{{ url('mytracker') }}" class="btn btn-primary w-full" style="background-color: rgb(215, 187, 237); border: none; border: none; border-radius: 0px 0px 12px 12px; color: black;">></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




{{-- end of mytracker table --}}

{{-- modal for view details --}}
<div id="ViewDetailsModal1" class="modal overflow-y-auto" style="border: none; padding-top: 100px; padding-bottom: 100px; background-color:rgba(56, 56, 56, 0.08)">
  <div class="row justify-content-center" id="ViewDetailsModal2">
    <div class="col-md-8 row justify-content-center" id="ViewDetailsModal3" style="border: none;">
      <div class="card" id="editCard2" style="border: none; width: 100%">
        <div class="card-header" style="background-color:#00a57c; color:white; width:100%;">
          <b style="width: 100%;" id="Paper_Subject"></b>
          <button class="float-left"onclick="ViewDetailsCloseFunction()">x</button>
        </div>

        <div class="accordion" id="accordionExample">
          <div class="card">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Approvers, Reviewers, CC and Related Party & Transfer Pricing (TP)
                </button>
              </h5>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body">
                <h4 style="padding-bottom: 10px;">Approvers: </h4>
                <table class="min-w-full divide-y" id="Table_Approvers">
                  <thead style="background:; border-radius: 25px;">
                    <tr style="">
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                        No
                      </th>
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Approvers
                      </th>
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Status
                      </th>
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Action
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr id="Table_Approvers_Tr" class="Table_Approvers_Tr">
                      <td id="Table_Approvers_Td_No" class="text-center Table_Approvers_Td_No">
                      </td>
                      <td id="Table_Approvers_Td_Content">
                      </td>
                    </tr>
                  </tbody>
                </table>
                <br>
                <h4 style="padding-bottom: 10px;">Reviewers: </h4>
                <table class="min-w-full divide-y" id="Table_Reviewers">
                  <thead style="background:; border-radius: 25px;">
                    <tr style="">
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                        No
                      </th>
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Reviewers
                      </th>
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Status
                      </th>
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Action
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr id="Table_Reviewers_Tr" class="Table_Reviewers_Tr">
                      <td id="Table_Reviewers_Td_No" class="text-center Table_Reviewers_Td_No">
                      </td>
                      <td id="Table_Reviewers_Td_Content">
                      </td>
                    </tr>
                  </tbody>
                </table>
                <br>
                <h4 style="padding-bottom: 10px;">CC: </h4>
                <table class="min-w-full divide-y" id="Table_CCs">
                  <thead style="background:; border-radius: 25px;">
                    <tr style="">
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                        No
                      </th>
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        CCs
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr id="Table_CCs_Tr" class="Table_CCs_Tr">
                      <td id="Table_CCs_Td_No" class="text-center Table_CCs_Td_No">
                      </td>
                      <td id="Table_CCs_Td_Content">
                      </td>
                    </tr>
                  </tbody>
                </table>
                <br>
                <h4 style="padding-bottom: 10px;">Related Party & Transfer Pricing (TP) : </h4>
                <table class="min-w-full divide-y" id="Table_RPTs">
                  <tbody>
                    <tr id="Table_RPTs_Tr1" class="Table_RPTs_Tr">
                      <td id="Table_RPTs_Td_No1" class="text-center Table_RPTs_Td_No">
                        1
                      </td>
                      <td id="Table_RPTs_Td_Content11" class="px-8">
                        Is the transacting party related to PDB?	
                      </td>
                      <td id="Table_RPTs_Td_Content12">
                      </td>
                    </tr>
                    <tr id="Table_RPTs_Tr2" class="Table_RPTs_Tr">
                      <td id="Table_RPTs_Td_No2" class="text-center Table_RPTs_Td_No">
                        2
                      </td>
                      <td id="Table_RPTs_Td_Content21" class="px-8">
                        Is the agreed price within prevailing market rate and on normal commersial terms?	
                      </td>
                      <td id="Table_RPTs_Td_Content22">
                      </td>
                    </tr>
                    <tr id="Table_RPTs_Tr3" class="Table_RPTs_Tr">
                      <td id="Table_RPTs_Td_No3" class="text-center Table_RPTs_Td_No">
                        3
                      </td>
                      <td id="Table_RPTs_Td_Content31" class="px-8">
                        Are the material terms at par with the non-related party?	
                      </td>
                      <td id="Table_RPTs_Td_Content32">
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          {{-- end of first accordion --}}

          <div class="card">
            <div class="card-header" id="headingTwo">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Objectives, Body and Attachments
                </button>
              </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
              <div class="card-body">
                <h4 style="padding-bottom: 10px;">Objectives : </h4>
                <table class="min-w-full divide-y" id="Table_Objectives">
                  <thead style="background:; border-radius: 25px;">
                    <tr style="">
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                        No
                      </th>
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Objectives
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr id="Table_Objectives_Tr" class="Table_Objectives_Tr">
                      <td id="Table_Objectives_Td_No" class="text-center Table_Objectives_Td_No">
                      </td>
                      <td id="Table_Objectives_Td_Content">
                      </td>
                    </tr>
                  </tbody>
                </table>
                <br>
                <h4 style="padding-bottom: 10px;">Body : </h4>
                <textarea readonly id="Paper_Content" maxlength="10000"  name="paper_content" rows="10" placeholder="Paper Content Here" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max"></textarea>
                <br>
                <h4 style="padding-bottom: 10px;">Attachments : </h4>
                <table class="min-w-full divide-y" id="Table_Attachments">
                  <thead style="background:; border-radius: 25px;">
                    <tr style="">
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                        No
                      </th>
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Attachments
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr id="Table_Attachments_Tr" class="Table_Attachments_Tr">
                      <td id="Table_Attachments_Td_No" class="text-center Table_Attachments_Td_No">
                      </td>
                      <td id="Table_Attachments_Td_Content">
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          {{-- end of second accordion --}}

          <div class="card">
            <div class="card-header" id="headingThree">
              <h5 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  Recommendations and Next Steps
                </button>
              </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
              <div class="card-body">
                <h4 style="padding-bottom: 10px;">Recommendations : </h4>
                <table class="min-w-full divide-y" id="Table_Recommendations">
                  <thead style="background:; border-radius: 25px;">
                    <tr style="">
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" style="width: 5%" >
                        No
                      </th>
                      <th scope="col" class="px-8 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                        Recommendations
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr id="Table_Recommendations_Tr" class="Table_Recommendations_Tr">
                      <td id="Table_Recommendations_Td_No" class="text-center Table_Recommendations_Td_No">
                      </td>
                      <td id="Table_Recommendations_Td_Content">
                      </td>
                    </tr>
                  </tbody>
                </table>
                <br>
                <h4 style="padding-bottom: 10px;">Next Steps : </h4>
                <textarea readonly id="Paper_Next_Steps" maxlength="10000"  name="Paper_Next_Steps" rows="10" placeholder="Paper Next Steps" class="py-3 px-4 block w-full shadow-sm text-warm-gray-900 focus:ring-teal-500 focus:border-teal-500 border-warm-gray-300 rounded-md" aria-describedby="message-max"></textarea>
                <br>
                <div id="AttachmentModal1" class="modal" style="border: none; padding-top:10%; background-color:rgba(56, 56, 56, 0.08)">
                  <div class="row justify-content-center" id="AttachmentModal2" >
                    <div class="col-md-8 row justify-content-center" id="AttachmentModal3" style="border: none; text-align:center" >
                      <div class="card" id="editCard2" style="border: none; width: 100%">
                        <div class="card-header" style="background-color:#00a57c; color:white; width:100%"><b>{{ __('Attachment') }}</b>
                        </div>
                        <div class="card-body justify-content-center">
                          <p><span id="AttachmentSpanName"></span></p>
                          <div class="flex justify-center pb-3 border-2 border-gray-300 border-dashed rounded-md">
                            <div class="space-y-1 text-center" style="width:100%">
                              <center><span id="AttachmentSpan"></span></center>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {{-- end of third accordion --}}

        </div>
      </div>
    </div>
  </div>
</div>

@endsection
<script>

function ViewDetailsCloseFunction() {
    document.getElementById("ViewDetailsModal1").style.display = "none";
  };

  function GetSentPaper(paper_id){
    $.ajax({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/GetSentPaper")}}',
      dataType: 'JSON',
      async:true, //ensure process finish then exit the function
      data: {
        paper_id : paper_id
      },
      success: function (data) {
        document.getElementById("ViewDetailsModal1").style.display = "block";
        $('#Table_Approvers .Table_Approvers_Tr').remove();
        for(var i in data.recipients_approver){
          var recipient_approver = data.recipients_approver[i];
          $('#Table_Approvers')
            .append($('<tr>', { class   : "Table_Approvers_Tr"})
                .append($('<td>', { class   : "text-center Table_Approvers_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Approvers_Td_Content px-8", 
                                    text    : recipient_approver.email}))
                .append($('<td>', { class   : "Table_Approvers_Td_Content px-8", 
                                    text    : recipient_approver.recipient_status}))
            );
        }
        $('#Table_Reviewers .Table_Reviewers_Tr').remove();
        for(var i in data.recipients_reviewer){
          var recipient_reviewer = data.recipients_reviewer[i];
          $('#Table_Reviewers')
            .append($('<tr>', { class   : "Table_Reviewers_Tr",
                                id      : "Table_Reviewers_Tr_"+recipient_reviewer.recipient_id})
                .append($('<td>', { class   : "text-center Table_Reviewers_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Reviewers_Td_Content px-8", 
                                    text    : recipient_reviewer.email}))
                .append($('<td>', { class   : "Table_Reviewers_Td_Content px-8", 
                                    text    : recipient_reviewer.recipient_status}))
                .append($('<td>', { class   : "Table_Reviewers_Td_Content px-8"})
                  // .append($('<button>', { type    : "button",
                  //                         onclick : "ReviewerApprovePaper('"+data.objective_id+"')",
                  //                         text    : " Delete ",
                  //                         class   : "mt-1 btn btn-success inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
                  //                         style   : "width:100px; margin-right: 0px"
                  //                         }))
                                          )
            );
          // if(recipient_reviewer.)
        }
        $('#Table_CCs .Table_CCs_Tr').remove();
        for(var i in data.recipients_cc){
          var recipient_cc = data.recipients_cc[i];
          $('#Table_CCs')
            .append($('<tr>', { class   : "Table_CCs_Tr"})
                .append($('<td>', { class   : "text-center Table_CCs_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_CCs_Td_Content px-8", 
                                    text    : recipient_cc.email}))
            );
        }
        document.getElementById("Paper_Subject").innerText = data.paper.paper_subject;
        document.getElementById("Table_RPTs_Td_Content12").innerText = data.rpt.rpt_related_pdb;
        document.getElementById("Table_RPTs_Td_Content22").innerText = data.rpt.rpt_market_rate;
        document.getElementById("Table_RPTs_Td_Content32").innerText = data.rpt.rpt_at_par;
        $('#Table_Objectives .Table_Objectives_Tr').remove();
        for(var i in data.objectives){
          var objective = data.objectives[i];
          $('#Table_Objectives')
            .append($('<tr>', { class   : "Table_Objectives_Tr"})
                .append($('<td>', { class   : "text-center Table_Objectives_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Objectives_Td_Content px-8", 
                                    text    : objective.objective}))
            );
        }
        document.getElementById("Paper_Content").innerText = data.paper.paper_content;
        $('#Table_Attachments .Table_Attachments_Tr').remove();
        for(var i in data.attachments){
          var attachment = data.attachments[i];
          $('#Table_Attachments')
            .append($('<tr>', { class   : "Table_Attachments_Tr"})
                .append($('<td>', { class   : "text-center Table_Attachments_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Attachments_Td_Content px-8", 
                                    text    : attachment.attachment_name,
                                    onclick : "AttachmentModalShow('" + attachment.attachment_name + "','" + data.paper.id + "')"}))
            );
        }
        $('#Table_Recommendations .Table_Recommendations_Tr').remove();
        for(var i in data.recommendations){
          var recommendation = data.recommendations[i];
          $('#Table_Recommendations')
            .append($('<tr>', { class   : "Table_Recommendations_Tr"})
                .append($('<td>', { class   : "text-center Table_Recommendations_Td_No", 
                                    text    : ++i}))
                .append($('<td>', { class   : "Table_Recommendations_Td_Content px-8", 
                                    text    : recommendation.recommendation}))
            );
        }
        document.getElementById("Paper_Next_Steps").innerText = data.paper.paper_next_steps;
        // document.getElementById("ButtonEditDraft").setAttribute("href", "draft-approval-pg1/" + data.paper.id);
        // document.getElementById("ButtonDeleteDraft").setAttribute("href", "DeleteDraftPaper/" + data.paper.id);
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to add objective!");
      },
    });
  }

  function GetInboxPaper(paper_id, recipient_status) {
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/GetSentPaper")}}',
      dataType: 'JSON',
      async: true, //ensure process finish then exit the function
      data: {
        paper_id: paper_id
      },
      success: function(data) {
        document.getElementById("InboxDiv").style.display = "block";
        document.getElementById("InboxDivRespond").style.display = "none";
        document.getElementById("RiskDetailDiv").style.display = "none";
        document.getElementById("DivRiskRespond").style.display = "none";

        $('#Table_Approvers .Table_Approvers_Tr').remove();
        for (var i in data.recipients_approver) {
          var recipient_approver = data.recipients_approver[i];
          $('#Table_Approvers')
            .append($('<tr>', {
                class: "Table_Approvers_Tr"
              })
              .append($('<td>', {
                class: "text-center Table_Approvers_Td_No",
                text: ++i
              }))
              .append($('<td>', {
                class: "Table_Approvers_Td_Content px-8",
                text: recipient_approver.email
              }))
              .append($('<td>', {
                class: "Table_Approvers_Td_Content px-8",
                text: recipient_approver.recipient_status
              }))
            );
        }
        $('#Table_Reviewers .Table_Reviewers_Tr').remove();
        for (var i in data.recipients_reviewer) {
          var recipient_reviewer = data.recipients_reviewer[i];
          $('#Table_Reviewers')
            .append($('<tr>', {
                class: "Table_Reviewers_Tr",
                id: "Table_Reviewers_Tr_" + recipient_reviewer.recipient_id
              })
              .append($('<td>', {
                class: "text-center Table_Reviewers_Td_No",
                text: ++i
              }))
              .append($('<td>', {
                class: "Table_Reviewers_Td_Content px-8",
                text: recipient_reviewer.email
              }))
              .append($('<td>', {
                class: "Table_Reviewers_Td_Content px-8",
                text: recipient_reviewer.recipient_status
              }))
              .append($('<td>', {
                class: "Table_Reviewers_Td_Content px-8"
              }))
            );
        }
        $('#Table_CCs .Table_CCs_Tr').remove();
        for (var i in data.recipients_cc) {
          var recipient_cc = data.recipients_cc[i];
          $('#Table_CCs')
            .append($('<tr>', {
                class: "Table_CCs_Tr"
              })
              .append($('<td>', {
                class: "text-center Table_CCs_Td_No",
                text: ++i
              }))
              .append($('<td>', {
                class: "Table_CCs_Td_Content px-8",
                text: recipient_cc.email
              }))
            );
        }
        document.getElementById("Paper_Subject").innerText = data.paper.paper_subject;
        document.getElementById("Table_RPTs_Td_Content12").innerText = data.rpt.rpt_related_pdb;
        document.getElementById("Table_RPTs_Td_Content22").innerText = data.rpt.rpt_market_rate;
        document.getElementById("Table_RPTs_Td_Content32").innerText = data.rpt.rpt_at_par;
        $('#Table_Objectives .Table_Objectives_Tr').remove();
        for (var i in data.objectives) {
          var objective = data.objectives[i];
          $('#Table_Objectives')
            .append($('<tr>', {
                class: "Table_Objectives_Tr"
              })
              .append($('<td>', {
                class: "text-center Table_Objectives_Td_No",
                text: ++i
              }))
              .append($('<td>', {
                class: "Table_Objectives_Td_Content px-8",
                text: objective.objective
              }))
            );
        }
        document.getElementById("Paper_Content").innerText = data.paper.paper_content;
        $('#Table_Attachments .Table_Attachments_Tr').remove();
        for (var i in data.attachments) {
          var attachment = data.attachments[i];
          $('#Table_Attachments')
            .append($('<tr>', {
                class: "Table_Attachments_Tr"
              })
              .append($('<td>', {
                class: "text-center Table_Attachments_Td_No",
                text: ++i
              }))
              .append($('<td>', {
                class: "Table_Attachments_Td_Content px-8",
                text: attachment.attachment_name,
                onclick: "AttachmentModalShow('" + attachment.attachment_name + "','" + data.paper.id + "')"
              }))
            );
        }
        $('#Table_Recommendations .Table_Recommendations_Tr').remove();
        for (var i in data.recommendations) {
          var recommendation = data.recommendations[i];
          $('#Table_Recommendations')
            .append($('<tr>', {
                class: "Table_Recommendations_Tr"
              })
              .append($('<td>', {
                class: "text-center Table_Recommendations_Td_No",
                text: ++i
              }))
              .append($('<td>', {
                class: "Table_Recommendations_Td_Content px-8",
                text: recommendation.recommendation
              }))
            );
        }
        document.getElementById("Paper_Next_Steps").innerText = data.paper.paper_next_steps;
        // $('#DivButtonRespond').getElementByTagName('button').remove();

        if (document.getElementById('ButtonRespond') != null) {
          document.getElementById('ButtonRespond').remove();
        }
        if (recipient_status == "Pending") {
          $('#DivButtonRespond')
            .append($('<button>', {
              id: "ButtonRespond",
              class: "btn btn-primary",
              text: "Respond",
              onclick: "RespondButtonOnclick(" + data.paper.id + ")"
            }));
        } else if (recipient_status == "Risks Approved") {
          $('#DivButtonRespond')
            .append($('<button>', {
                id: "ButtonRespond",
                class: "btn btn-primary",
                text: "Proceed",
                onclick: "window.location='{{url('/submission-pg1/')}}/" + data.paper.id + "'"
              })
              // onclick : "window.location='{{url('/PaperSubmission/" + data.paper.id + "')}}'"})

              // onclick="window.location='{{url('/risk-assessment-pg2')}}'"
              // onclick : "{{url('/PaperSubmission/" + data.paper.id + "')}}"})
              // onclick : "PaperSubmissionButonOnclick(" + data.paper.id + ")"})
            );
        }
        // document.getElementById("ButtonEditDraft").setAttribute("href", "draft-approval-pg1/" + data.paper.id);
        // document.getElementById("ButtonDeleteDraft").setAttribute("href", "DeleteDraftPaper/" + data.paper.id);
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to add objective!");
      },
    });
  }

  function RespondButtonOnclick(paper_id) {
    document.getElementById("InboxDiv").style.display = "none";
    document.getElementById("InboxDivRespond").style.display = "block";
    document.getElementById("RiskDetailDiv").style.display = "none";
    document.getElementById("DivRiskRespond").style.display = "none";

    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/GetSentPaper")}}',
      dataType: 'JSON',
      async: true, //ensure process finish then exit the function
      data: {
        paper_id: paper_id
      },
      success: function(data) {
        document.getElementById("MainDiv").scrollTop = 0;
        document.getElementById("Respond_Paper_Subject").innerText = data.paper.paper_subject;
        document.getElementById("Respond_Paper_User").innerText = data.user.name;
        // document.getElementById("ButtonReviewed").onclick = ;
        document.getElementById("ButtonReviewed").setAttribute('onclick', "ButtonReviewedClicked('" + data.paper.id + "')");

        // document.getElementById("PaperRespondComment");
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to add objective!");
      },
    });
  }

  function ButtonReviewedClicked(paper_id) {
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/PaperReviewed")}}',
      dataType: 'JSON',
      async: true, //ensure process finish then exit the function
      data: {
        paper_id: paper_id
      },
      success: function(data) {
        alert("Successfuly reviewed the paper!")
        location.reload();
        // document.getElementById("MainDiv").scrollTop = 0;
        // document.getElementById("Respond_Paper_Subject").innerText = data.paper.paper_subject;
        // document.getElementById("Respond_Paper_User").innerText = data.user.name;

        // document.getElementById("PaperRespondComment");
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to add objective!");
      },
    });
  }

  function ShowPaper() {
    document.getElementById('RiskDiv').style.display = "none";
    document.getElementById('PaperDiv').style.display = "block";
    document.getElementById("DivRiskRespond").style.display = "none";
  }

  function ShowRisk() {
    document.getElementById('RiskDiv').style.display = "block";
    document.getElementById('PaperDiv').style.display = "none";
    document.getElementById("DivRiskRespond").style.display = "none";
  }

  function GetRiskDetails(paper_id, risk_recipient_status) {
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/GetRiskDetails")}}',
      dataType: 'JSON',
      async: true, //ensure process finish then exit the function
      data: {
        paper_id: paper_id
      },
      success: function(data) {
        document.getElementById("RiskDetailDiv").style.display = "block";
        document.getElementById("DivRiskRespond").style.display = "none";
        document.getElementById("InboxDiv").style.display = "none";
        document.getElementById("InboxDivRespond").style.display = "none";

        $('#Table_Risk_Endorser .Table_Risk_Endorser_Tr').remove();
        for (var i in data.risk_endorsers) {
          var risk_endorser = data.risk_endorsers[i];
          $('#Table_Risk_Endorser')
            .append($('<tr>', {
                class: "Table_Risk_Endorser_Tr"
              })
              .append($('<td>', {
                class: "text-center Table_Risk_Endorser_Tr",
                text: ++i
              }))
              .append($('<td>', {
                class: "Table_Risk_Endorser_Td_Content px-8",
                text: risk_endorser.email
              }))
              .append($('<td>', {
                class: "Table_Risk_Endorser_Td_Content px-8",
                text: risk_endorser.riskrecipient_status
              }))
            );
        }
        // Table_Risk_Endorser
        $('#Table_Risk_Reviewer .Table_Risk_Reviewer_Tr').remove();
        for (var i in data.risk_reviewers) {
          var risk_reviewer = data.risk_reviewers[i];
          $('#Table_Risk_Reviewer')
            .append($('<tr>', {
                class: "Table_Risk_Reviewer_Tr"
              })
              .append($('<td>', {
                class: "text-center Table_Risk_Reviewer_Tr",
                text: ++i
              }))
              .append($('<td>', {
                class: "Table_Risk_Reviewer_Td_Content px-8",
                text: risk_reviewer.email
              }))
              .append($('<td>', {
                class: "Table_Risk_Reviewer_Td_Content px-8",
                text: risk_reviewer.riskrecipient_status
              }))
            );
        }
        $('#Table_Risk_Mitigation .Table_Risk_Mitigation_Tr').remove();
        for (var i in data.risks) {
          var risk = data.risks[i];
          $('#Table_Risk_Mitigation')
            .append($('<tr>', {
                class: "Table_Risk_Mitigation_Tr"
              })
              .append($('<td>', {
                class: "text-center Table_Risk_Mitigation_Tr",
                rowspan: risk.mitigations.length + 1,
                text: ++i
              }))
              .append($('<td>', {
                class: "Table_Risk_Mitigation_Content px-8",
                rowspan: risk.mitigations.length + 1,
                text: risk.risk_title
              }))
            );
          for (var i in risk.mitigations) {
            var mitigation = risk.mitigations[i];
            $('#Table_Risk_Mitigation')
              .append($('<tr>', {
                  class: "Table_Risk_Mitigation_Tr"
                })
                .append($('<td>', {
                  class: "Table_Risk_Mitigation_Content px-8",
                  text: mitigation.mitigation_detail
                }))
              );
          }
        }
        if (document.getElementById('ButtonRiskRespond') != null) {
          document.getElementById('ButtonRiskRespond').remove();
        }
        if (risk_recipient_status == "Pending") {
          $('#DivRiskButtonRespond')
            .append($('<button>', {
              id: "ButtonRiskRespond",
              class: "btn btn-primary",
              text: "Respond",
              onclick: "RiskRespondButtonOnclick(" + paper_id + ")"
            }));
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to fetch risk details!");
      },
    });
  }

  function RiskRespondButtonOnclick(paper_id) {
    document.getElementById("RiskDetailDiv").style.display = "none";
    document.getElementById("DivRiskRespond").style.display = "block";
    document.getElementById("InboxDiv").style.display = "none";
    document.getElementById("InboxDivRespond").style.display = "none";

    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/GetRiskDetails")}}',
      dataType: 'JSON',
      async: true, //ensure process finish then exit the function
      data: {
        paper_id: paper_id
      },
      success: function(data) {
        document.getElementById("MainDiv").scrollTop = 0;
        // document.getElementById("Respond_Paper_Subject").innerText = data.paper.paper_subject;
        // document.getElementById("Respond_Paper_User").innerText = data.user.name;
        document.getElementById("ButtonRiskReviewed").setAttribute('onclick', "ButtonRiskReviewedClicked('" + paper_id + "')");
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to add objective!");
      },
    });
  }

  function ButtonRiskReviewedClicked(paper_id) {
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type: 'post',
      url: '{{url("/RiskReviewed")}}',
      dataType: 'JSON',
      async: true, //ensure process finish then exit the function
      data: {
        paper_id: paper_id
      },
      success: function(data) {
        alert("Successfuly reviewed the paper!")
        location.reload();
        // document.getElementById("MainDiv").scrollTop = 0;
        // document.getElementById("Respond_Paper_Subject").innerText = data.paper.paper_subject;
        // document.getElementById("Respond_Paper_User").innerText = data.user.name;

        // document.getElementById("PaperRespondComment");
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("Fail to add objective!");
      },
    });
  }
</script>